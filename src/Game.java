package ivanhoe;
//import org.apache.log4j.Logger;
import java.util.*;

public final class Game {

	public static int JOIN = 1;
	public static int INTOURNAMENT = 2;
	public static int WAITING = 3;
	public static int GAMEOVER = 4;
	public int state;

	public ArrayList<Player> players;
	public ArrayList<Player> savedPlayers;
	
	private int totalPlayers; 
	public ArrayList<Card> drawPile;
	public ArrayList<Card> discardPile;
	private String colour;
	private String savedColour;

	private Player currPlayer;

	public Game(int totalPlayers) {
		this.totalPlayers = totalPlayers;
		this.players = new ArrayList<Player>(totalPlayers);
		this.state = JOIN;
		this.colour = "DEFAULT";
	}

	//The server will only call this if all players have joined so no need to check this again
	public void beginGame(){
		if (currPlayer == null) { currPlayer = players.get(0); }
		this.drawPile = new ArrayList<Card>(110);
		createDrawPile();
		this.discardPile = new ArrayList<Card>();
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < players.size(); j++) {
				players.get(j).addToHand(drawCard());
				//players.get(j).playing = true; //not ideal because we do this 8 times per player...
			}
		}
		//if currPlayer can't start the tournament cause they only have action cards go to next person
		boolean canPlay = false;
		while (!canPlay) {
			for (Card c : currPlayer.getHand()) {
				if (c.getValue() != 0) {
					canPlay = true;
				}
			}
			if (!canPlay) {
				for (int j = 0; j < players.size(); j++) {
					if (players.get(j) == currPlayer) {
						currPlayer = players.get((j + 1) % players.size());
					}
				}
			}
		}
		beginTournament();
	}
	public void beginTournament() {
		for (int j = 0; j < players.size(); j++) {
			players.get(j).playing = true; //not ideal because we do this 8 times per player...
		}
		boolean canPlay = false;
		while (!canPlay) {
			for (Card c : currPlayer.getHand()) {
				if (c.getValue() != 0) {
					canPlay = true;
				}
			}
			if (!canPlay) {
				for (int j = 0; j < players.size(); j++) {
					if (players.get(j).getPortId() == currPlayer.getPortId()) {
						currPlayer = players.get((j + 1) % players.size());
						break;
					}
				}
			}
		}

	}

	public boolean setColour(String col) {
		this.state = INTOURNAMENT;
		if (col.equalsIgnoreCase("P") && !colour.equalsIgnoreCase("Purple")) {
			colour = "Purple";
		} else if (col.equalsIgnoreCase("R")) {
			colour = "Red";
		} else if (col.equalsIgnoreCase("Y")) {
			colour = "Yellow";
		} else if (col.equalsIgnoreCase("B")) {
			colour = "Blue";
		} else if (col.equalsIgnoreCase("G")) {
			colour = "Green";
		} else {
			return false;
		}
		return true;
	}

	private void createDrawPile() {
		int i;
		for (i = 0; i < 14; i++) {
			drawPile.add(new Card("Green", 1));
		}
		for (i = 0; i < 8; i++) {
			drawPile.add(new Card("Yellow", 3));
			drawPile.add(new Card("Squire", 2));
			drawPile.add(new Card("Squire", 3));
		}
		for (i = 0; i < 6; i++) {
			drawPile.add(new Card("Red", 3));
			drawPile.add(new Card("Red", 4));
		}
		for (i = 0; i < 4; i++) {
			drawPile.add(new Card("Purple", 3));
			drawPile.add(new Card("Purple", 4));
			drawPile.add(new Card("Purple", 5));
			drawPile.add(new Card("Blue", 2));
			drawPile.add(new Card("Blue", 3));
			drawPile.add(new Card("Blue", 4));
			drawPile.add(new Card("Yellow", 2));
			drawPile.add(new Card("Maiden", 6));
		}
		for (i = 0; i < 2; i++) {
			drawPile.add(new Card("Purple", 7));
			drawPile.add(new Card("Red", 5));
			drawPile.add(new Card("Blue", 5));
			drawPile.add(new Card("Yellow", 4));
		}
		drawPile.add(new Card("Unhorse", 0));
		drawPile.add(new Card("ChangeWeapon", 0));
		drawPile.add(new Card("DropWeapon", 0));
		drawPile.add(new Card("Shield", 0));
		drawPile.add(new Card("Stunned", 0));
		drawPile.add(new Card("Ivanhoe", 0));
		drawPile.add(new Card("BreakLance", 0));
		drawPile.add(new Card("Riposte", 0));
		drawPile.add(new Card("Riposte", 0));
		drawPile.add(new Card("Riposte", 0));
		drawPile.add(new Card("Dodge", 0));
		drawPile.add(new Card("Retreat", 0));
		drawPile.add(new Card("KnockDown", 0));
		drawPile.add(new Card("KnockDown", 0));
		drawPile.add(new Card("Outmaneuver", 0));
		drawPile.add(new Card("Charge", 0));
		drawPile.add(new Card("Countercharge", 0));
		drawPile.add(new Card("Disgrace", 0));
		drawPile.add(new Card("Adapt", 0));
		drawPile.add(new Card("Outwit", 0));
		Collections.shuffle(drawPile);
	}

	public boolean addPlayer(int portid, String name) {
		if(players.size() < totalPlayers){
			players.add(new Player(portid, name));
			return true;
		}
		return false;
	}

	public Player getPlayer(int portid) {
		for (int i = 0; i < players.size(); i++) {
			if (players.get(i).getPortId() == portid) {
				return players.get(i);
			}
		}
		return null;
	}

	public boolean checkCurrPlayer(int portid) {
		return currPlayer.getPortId() == portid;
	}

	public Card drawCard() {
		if (drawPile.isEmpty()) {
			drawPile = discardPile;
			discardPile = new ArrayList<Card>();
			Collections.shuffle(drawPile);
		}
		if (drawPile.get(0).getName().equalsIgnoreCase("Ivanhoe")) { currPlayer.setHasIvanhoe(true); }
		return drawPile.remove(0);//changed to actually take card off the top;
	}
	
	//For the server so it returns the portid
	public Integer getIvanhoePlayer() {
		for (int i = 0; i < players.size(); i++) {
			if (players.get(i).getHasIvanhoe() && players.get(i).playing) { 
				return players.get(i).getPortId(); 
			}
		}
		return -1;
	}
	
	//Save the state to be returned to in case the Ivanhoe card is played
	public void saveState() {
		savedPlayers = new ArrayList<Player>();
		for (Player p : players) {
			if (p != null) {
				savedPlayers.add(new Player(p));
			}
		}
		savedColour = colour;
	}
	
	//Undo the changes done by an action card because Ivanhoe was played or continue
	public void updateState(String action, int portidIvanhoe, String actionCard) {
		if (action.equalsIgnoreCase("undo")) {
			players = savedPlayers;
			colour = savedColour;
			//change currPlayer to not point to the old player!
			for (Player p : players) {
				if (p != null) {
					if (p.getPortId() == currPlayer.getPortId()) {
						currPlayer = p;
						break;
					}
				}
			}
			String[] card = actionCard.split(" ");
			currPlayer.removeFromHand(card[0], Integer.parseInt(card[1]));
			getPlayer(portidIvanhoe).removeFromHand("Ivanhoe", 0);
			getPlayer(portidIvanhoe).setHasIvanhoe(false);
		} 
		savedPlayers = null;
		savedColour = "";
	}

 	public String playCard(String input) {
 		String[] parts = input.split(" ");
 		String result = "Good";
 		int value = Integer.parseInt(parts[1]);
 		
 		//if the player is stunned and has already played the 1 card they're allowed to then
 		if (currPlayer.getStunned() && currPlayer.numPlayedDisplay > 0
 				&& (parts[0].equalsIgnoreCase(colour) || parts[0].equalsIgnoreCase("Squire")
 		 		|| parts[0].equalsIgnoreCase("Maiden") || parts[0].equalsIgnoreCase("Riposte"))) {
 			//check if the card they're trying to play will add another card and if so then return bad
 			return "Bad";
 		}
 		
 		if (parts[0].equalsIgnoreCase(colour) || parts[0].equalsIgnoreCase("Squire")
 				|| parts[0].equalsIgnoreCase("Maiden")) {
 			if (parts[0].equalsIgnoreCase("Maiden")) {
 				for (Card temp : currPlayer.getDisplay()) {
 					if (temp != null) {
 						if (temp.getName().equalsIgnoreCase("Maiden")) {
 							return "Bad"; //only one maiden per player per tournament
 						}
 					}
 				}
 			}
 			if(colour.equals("Green")) {
 				currPlayer.setScore(currPlayer.getScore() + 1);
 			} else {
 				currPlayer.setScore(currPlayer.getScore() + value);
 			}
 			Card c = currPlayer.removeFromHand(parts[0], value);
 			currPlayer.addToDisplay(c);
 			currPlayer.hasPlayedACard = true;
 			currPlayer.numPlayedDisplay++;

 		} else if (parts[0].equalsIgnoreCase("Unhorse") && colour.equalsIgnoreCase("Purple")) {
 			saveState();
 			result = "colour";
 		} else if (parts[0].equalsIgnoreCase("ChangeWeapon") && (colour.equalsIgnoreCase("Red")
 				|| colour.equalsIgnoreCase("Blue") || colour.equalsIgnoreCase("Yellow"))) {
 			saveState();
 			result = "colour";
 		} else if (parts[0].equalsIgnoreCase("DropWeapon") && (colour.equalsIgnoreCase("Red")
 				|| colour.equalsIgnoreCase("Blue") || colour.equalsIgnoreCase("Yellow"))) {
 			saveState();
 			colour = "Green";
 			//if other -> green, display cards count for 1
 			for (int i = 0; i < players.size(); i++) {
 				players.get(i).setScore(players.get(i).getDisplay().size());
			}
			//currPlayer.removeFromHand(parts[0], value);
 			discardPile.add(currPlayer.removeFromHand(parts[0], value));
			currPlayer.hasPlayedACard = true;

		} else if (parts[0].equalsIgnoreCase("BreakLance") || parts[0].equalsIgnoreCase("Riposte")) {
			result = "target";
		} else if (parts[0].equalsIgnoreCase("Dodge")) {
			result = "targetDisplayCard";
		} else if (parts[0].equalsIgnoreCase("Retreat")) {
			result = "ownDisplayCard";
		} else if (parts[0].equalsIgnoreCase("KnockDown") || parts[0].equalsIgnoreCase("Stunned")) {
			result = "targetHandCard";
		} else if (parts[0].equalsIgnoreCase("Shield")) {
			saveState();
			currPlayer.setShield(true);
			//currPlayer.removeFromHand(parts[0], value);
			discardPile.add(currPlayer.removeFromHand(parts[0], value));
			
		} else if (parts[0].equalsIgnoreCase("Outmaneuver")) {
			for (Player p : players) {
				if (p.playing && p != currPlayer) {
					if (p.getDisplay().size() < 2) {
						return "Bad";
					}
				}
			}
			saveState();
			//I'm assuming that remove means take back into hand
			for (Player p : players) {
				if (p.playing && p != currPlayer && !p.getShield()) {
					Card c = p.getDisplay().remove(p.getDisplay().size() - 1);
					p.setScore(p.getScore() - c.getValue());
					p.addToHand(c);
				}
			} 
			//currPlayer.removeFromHand(parts[0], value);
			discardPile.add(currPlayer.removeFromHand(parts[0], value));
			currPlayer.hasPlayedACard = true;
			
		} else if (parts[0].equalsIgnoreCase("Charge")) {
			int smallValue = 10; //larger then any card's value
			//check that all players that are still playing can loose a card safely 
			for (Player p : players) {
				if (p.playing) {
					if (p.getDisplay().size() < 2) {
						return "Bad";
					}
					for (Card c : p.getDisplay()) {
						smallValue = Math.min(smallValue, c.getValue());
					}
				}
			}
			
			saveState();
			//discard all cards with smallValue from the displays
			for (Player p : players) {
				if (!p.getShield()) {
					for (int i = p.getDisplay().size() -1; i > -1; i--) { //loop backwards to keep first card
						if (p.getDisplay().get(i).getValue() == smallValue && p.getDisplay().size() != 1) {
							Card c = p.getDisplay().remove(i);
							discardPile.add(c);
							p.setScore(p.getScore() - c.getValue());
						}
					}
				}
			}
			//currPlayer.removeFromHand(parts[0], value);
			discardPile.add(currPlayer.removeFromHand(parts[0], value));
			currPlayer.hasPlayedACard = true;
			
		} else if (parts[0].equalsIgnoreCase("Countercharge")) {
			int largeValue = 0; //larger then any card's value
			//check that all players that are still playing can loose a card safely 
			for (Player p : players) {
				if (p.playing) {
					if (p.getDisplay().size() < 2) {
						return "Bad";
					}
					for (Card c : p.getDisplay()) {
						largeValue = Math.max(largeValue, c.getValue());
					}
				}
			}
			
			saveState();
			//discard all cards with largeValue from the displays
			for (Player p : players) {
				if (!p.getShield()) {
					for (int i = p.getDisplay().size() -1; i > -1; i--) { //loop backwards to keep first card
						if (p.getDisplay().get(i).getValue() == largeValue && p.getDisplay().size() != 1) {
							Card c = p.getDisplay().remove(i);
							discardPile.add(c);
							p.setScore(p.getScore() - c.getValue());
						}
					}
				}
			}
			//currPlayer.removeFromHand(parts[0], value);
			discardPile.add(currPlayer.removeFromHand(parts[0], value));
			currPlayer.hasPlayedACard = true;
			
		} else if (parts[0].equalsIgnoreCase("Disgrace")) {
			//check that all players that are still playing can loose a card safely 
			for (Player p : players) {
				if (p.playing) {
					if (p.getDisplay().size() < 2) {
						return "Bad";
					}
				}
			}
			
			saveState();
			//REMOVE all supported cards from the displays
			for (Player p : players) {
				if (!p.getShield()) {
					for (int i = p.getDisplay().size() -1; i > -1; i--) { //loop backwards to keep first card
						if ((p.getDisplay().get(i).getName().equalsIgnoreCase("Squire") 
								|| p.getDisplay().get(i).getName().equalsIgnoreCase("Maiden")) 
								&& p.getDisplay().size() != 1) {
							Card c = p.getDisplay().remove(i);
							p.setScore(p.getScore() - c.getValue());
							p.addToHand(c);
						}
					}
				}
			}
			//currPlayer.removeFromHand(parts[0], value);
			discardPile.add(currPlayer.removeFromHand(parts[0], value));
			currPlayer.hasPlayedACard = true;
 			
		} else if (parts[0].equalsIgnoreCase("Adapt")) {
			//check that adapt can be played
			result = "Bad";
			for (Player p : players) {
				if (!findMultipleValueCards(p.getPortId()).isEmpty()) { 
					result = "Adapt"; //The server will handle the rest
					saveState(); 
					break;
				} 
			}
			
		} else if (parts[0].equalsIgnoreCase("Outwit")) {
			result = "targetDisplayOwnDisplay"; //target & card from own display & card from target display
 		} else {
 			result = "Bad";
 		}
 	return result;
 	}

	//the server can use this function to check that the colour the player chose when they
	//played their card is a valid choice before setting the new colour
	public boolean validColour(String s) {
		return s.equalsIgnoreCase("B") || s.equalsIgnoreCase("Y") || s.equalsIgnoreCase("R");
	}

	public boolean playAgainstTarget(String card, String targetPortid, String cardNum) {

		Player target = getPlayer(Integer.parseInt(targetPortid)); //blank for retreat??
		int count = 0;
		card = card.split(" ")[0];

		if (!target.playing || (target.getShield() && !card.equalsIgnoreCase("Retreat") 
				&& !card.equalsIgnoreCase("KnockDown") && !card.equalsIgnoreCase("Stunned"))) { 
			return false; 
			} //TARGET MUST BE PLAYING!

		if (card.equalsIgnoreCase("BreakLance")) {
			for (Card c : target.getDisplay()) {
				if (c.getName().equalsIgnoreCase("Purple")) { count++; }
			}

			//check that the target has at least 1 purple card and that's not the only card in display
			if (count > 0 && target.getDisplay().size() > 1) {
				saveState();
				//if the only cards they have are purple then keep the first one played
				int i = 0;
				if (count == target.getDisplay().size()) { i = 1; }

				for (; i < target.getDisplay().size(); i++) {
					Card c = target.getDisplay().get(i);
					if (c.getName().equalsIgnoreCase("Purple")) {
						target.setScore(target.getScore() - c.getValue());
						//target.getDisplay().remove(i);
						discardPile.add(target.getDisplay().remove(i));
					}
				}
			} else {
				return false;
			}

		} else if (card.equalsIgnoreCase("Riposte")) {
			//if the target has more then 1 card in their display
			if (target.getDisplay().size() > 1) {
				saveState();
				Card c = target.getDisplay().remove(target.getDisplay().size() - 1);
				target.setScore(target.getScore() - c.getValue());
				currPlayer.addToDisplay(c);
				currPlayer.setScore(currPlayer.getScore() + c.getValue());
				currPlayer.numPlayedDisplay++;
			} else {
				return false;
			}

		} else if (card.equalsIgnoreCase("Dodge") && !cardNum.equals("") && target.getDisplay().size() > 1) {
			//discard any card from the opponent's display
			saveState();
			Card c = target.getDisplay().remove(Integer.parseInt(cardNum));
			target.setScore(target.getScore() - c.getValue());
			discardPile.add(c);


		} else if (card.equalsIgnoreCase("Retreat") && !cardNum.equals("") && currPlayer.getDisplay().size() > 1) {
			//take any card from currPlayer's display back into it's hand
			saveState();
			Card c = currPlayer.getDisplay().remove(Integer.parseInt(cardNum));
			currPlayer.setScore(currPlayer.getScore() - c.getValue());
			currPlayer.addToHand(c);

		} else if (card.equalsIgnoreCase("KnockDown") && !cardNum.equals("") && !target.getHand().isEmpty()) {
			//remove the card from the target's hand and add it to your own
			saveState();
			Card c = target.getHand().remove(Integer.parseInt(cardNum));
			currPlayer.addToHand(c);

		} else if (card.equalsIgnoreCase("Stunned")) {
			//set the target to stunned
			saveState();
			target.setStunned(true);
			
		} else {
			return false;
		}
		//currPlayer.removeFromHand(card, 0);
		discardPile.add(currPlayer.removeFromHand(card, 0));
		currPlayer.hasPlayedACard = true;
		return true;
	}
	
	//Returns true is everything went fine, false if there was a problem
	public boolean playAgainstTarget(String card, String targetPortid, String ownCard, String targetCard) {
		Player target = getPlayer(Integer.parseInt(targetPortid)); //blank for retreat??
		card = card.split(" ")[0]; //the name of the action card played, in this case Outwit

		// -1 for shield and -2 for stunned
		int ownCardNum = Integer.parseInt(ownCard);
		int targetCardNum = Integer.parseInt(targetCard);
		
		if (!target.playing || (target.getShield() && targetCardNum >= 0)) { return false; } 
				
		//shield for stunned
		if (ownCardNum == -1 && targetCardNum == -2) {
			if (currPlayer.getShield() && target.getStunned()) {
				saveState();
				currPlayer.setShield(false);
				currPlayer.setStunned(true);
				target.setShield(true);
				target.setStunned(false);;
			} else {
				return false;
			}
			
		//stunned for shield
		} else if (ownCardNum == -2 && targetCardNum == -1) {
			if (currPlayer.getStunned() && target.getShield()) {
				saveState();
				currPlayer.setShield(true);
				currPlayer.setStunned(false);
				target.setShield(false);
				target.setStunned(true);;
			} else {
				return false;
			}
			
		//shield or stunned for display
		} else if ((ownCardNum == -1 || ownCardNum == -2) && targetCardNum < target.getDisplay().size()
				&& targetCardNum > -1 && target.getDisplay().size() > 1) {
			if (ownCardNum == -1 && currPlayer.getShield()) {
				saveState();
				currPlayer.setShield(false);
				target.setShield(true);
			} else if (ownCardNum == -2 && currPlayer.getStunned()) {
				saveState();
				currPlayer.setStunned(false);
				target.setStunned(true);
			} else {
				return false;
			}
			Card c = target.getDisplay().remove(targetCardNum);
			target.setScore(target.getScore() - c.getValue());
			
			currPlayer.addToDisplay(c);
			currPlayer.setScore(currPlayer.getScore() + c.getValue());
			
		//display for shield or stunned
		} else if ((targetCardNum == -1 || targetCardNum == -2) && ownCardNum < currPlayer.getDisplay().size()
				&& ownCardNum > -1 && currPlayer.getDisplay().size() > 1) {
			if (targetCardNum == -1 && target.getShield()) {
				saveState();
				target.setShield(false);
				currPlayer.setShield(true);
			} else if (targetCardNum == -2 && target.getStunned()) {
				saveState();
				target.setStunned(false);
				currPlayer.setStunned(true);
			} else {
				return false;
			}
			Card c = currPlayer.getDisplay().remove(ownCardNum);
			currPlayer.setScore(currPlayer.getScore() - c.getValue());
			
			target.addToDisplay(c);
			target.setScore(target.getScore() + c.getValue());
			
		//display for display
		} else if (ownCardNum < currPlayer.getDisplay().size() && ownCardNum > -1
				&& targetCardNum < target.getDisplay().size() && targetCardNum > -1) {
			
			saveState();
			Card c1 = currPlayer.getDisplay().remove(ownCardNum);
			currPlayer.setScore(currPlayer.getScore() - c1.getValue());
			
			Card c2 = target.getDisplay().remove(targetCardNum);
			target.setScore(target.getScore() - c2.getValue());
			
			target.addToDisplay(c1);
			target.setScore(target.getScore() + c1.getValue());
			
			currPlayer.addToDisplay(c2);
			currPlayer.setScore(currPlayer.getScore() + c2.getValue());
			
		} else {
			return false;
		}
		
		//currPlayer.removeFromHand(card, 0);
		discardPile.add(currPlayer.removeFromHand(card, 0));
		currPlayer.hasPlayedACard = true;
		return true;
	}

	//if the player hasn't player a card, they are withdrawn.
	//return true if the player must be withdrawn, else return false.
	public boolean endTurn() {
		if (currPlayer.hasPlayedACard) {
			
			currPlayer.hasPlayedACard = false;
			currPlayer.numPlayedDisplay = 0;
			
			//they just have to top the prev player's score...cause each player must top the previous
			for (int i = 0; i < players.size(); i++) {
				if (players.get(i).getScore() >= currPlayer.getScore() && players.get(i) != currPlayer) {
					return true;
				}
			}
		} else {
			return true;
		}

		nextPlayer();
		//do nothing the server will ask for the portid of the next player
	return false;
	}

	/*
	  Player's have a boolean called playing. If it's false then they have withdrawn from the
	  tournament.
	*/
	public boolean withdraw() {
		boolean result = false; //if we need to remove a token or not
		currPlayer.playing = false;
		currPlayer.hasPlayedACard = false;
		currPlayer.numPlayedDisplay = 0;
		currPlayer.setScore(0);

		for (int i = currPlayer.getDisplay().size()-1; i > -1; i--) {

			if (currPlayer.getDisplay().get(i).getName().equalsIgnoreCase("Maiden") && !currPlayer.getTokens().isEmpty()) {
				result = true;
				break;
			}
		}
		currPlayer.getDisplay().clear();


		int numPlaying = 0;
		Player p = null; //???
		for (int i = 0; i < players.size(); i++) {
			//check if at least 2 players are still playing
			if (players.get(i).playing) { //no need to check it's not the current player, we set to false above
				numPlaying++;
				p = players.get(i);
			}
		}
		if (numPlaying == 1) {
			state = WAITING;
			currPlayer = p;
			//endTournament(p);
		} else {
			nextPlayer();
		}
		return result;
	}

	//change currPlayer to the next player still playing
	public void nextPlayer() {
		int j = players.indexOf(currPlayer);
		do {
			j = (j + 1) % (players.size());
		} while (!players.get(j).playing);
		currPlayer = players.get(j);
		currPlayer.addToHand(drawCard());
	}

	public boolean removeToken(int ID, char token) {
		boolean r = false;
		for(Player p: players){
			if(p.getPortId() == ID) r=p.removeToken(token);
		}
		return r;
	}

	//withdraw sets the currPlayer to the winner if the tournament has over
	//the server will check if the tournament is in the WAITING state after every withdraw
	//return true if the player can choose a colour, false if they can't???
	public boolean endTournament() {
		boolean result = false;
		/*
		 * for some reason this loop was only clearing cards of player who won tournament
		for (int i = 0; i < players.size()-1; i++) {
			Player p = players.get(i);
			p.getHand().clear();
			p.getDisplay().clear();
			p.setScore(0);
			//p.getTokens().clear();
			p.setShield(false);
			p.setStunned(false);
		}*/
		for(Player p: players){
			//p.getHand().clear();
			p.getDisplay().clear();
			p.setScore(0);
			//p.getTokens().clear();
			p.setShield(false);
			p.setStunned(false);
		}
		if (colour.equalsIgnoreCase("Purple")) {
			result = true;
			//how to stop them from choosing a colour they already have??
		} else if (colour.equalsIgnoreCase("Red")) {
			currPlayer.addToken('R');
		} else if (colour.equalsIgnoreCase("Blue")) {
			currPlayer.addToken('B');
		} else if (colour.equalsIgnoreCase("Yellow")) {
			currPlayer.addToken('Y');
		} else if (colour.equalsIgnoreCase("Green")) {
			currPlayer.addToken('G');
		}
		//drawPile.clear();
		//discardPile.clear();
		//we can ignore colour here I think, it could be useful to keep for now
		return result;
	}
	
	/*
	 * Used by the server to handle the Adapt action card.
	 * In: the portid of a player
	 * Out: an arraylist of the card values that appear more then once in the player's display
	 */
	public ArrayList<Integer> findMultipleValueCards(int portid) {
		//loop over the players cards and if they have a multiple of some value then put it in the arrayList
		HashMap<Integer, Integer> temp = new HashMap<Integer, Integer>();
		ArrayList<Integer> result = new ArrayList<Integer>();
		
		if (getPlayer(portid).getShield()) { return result; }
		
		for (Card c : getPlayer(portid).getDisplay()) {
			if (c != null){
				//if the value is already in there
						
				if (temp.containsKey(c.getValue())) {
					temp.put(c.getValue(), temp.get(c.getValue()) + 1);
				} else {
					temp.put(c.getValue(), 1);
				}
			}
		}
				
		//if the value is > 1 the put the key in the arraylist
		for (int i : temp.keySet()) {
			if (temp.get(i) > 1) {
				result.add(i);
			}
		}
		return result;
	}
	
	public void removeExcept(int portid, String theCard) {
		//remove all cards in the display with c's value except c
		String cardName = theCard.split(" ")[0];
		int cardValue = Integer.parseInt(theCard.split(" ")[1]);
		boolean kept = false;
		Player p = getPlayer(portid);
		
		for (int i = 0; i < p.getDisplay().size(); i++) {
			//if the card matches and we haven't saved a card of that kind yet
			if (!kept && p.getDisplay().get(i).getName().equalsIgnoreCase(cardName) 
					&& p.getDisplay().get(i).getValue() == cardValue) {
				kept = true;
			} else if (p.getDisplay().get(i).getValue() == cardValue) {
				Card c = p.getDisplay().remove(i);
				p.setScore(p.getScore() - c.getValue());
				i = i - 1;
			} else {
			}
		}
	}

	public boolean awardToken(char token) {
		if (currPlayer.getTokens().contains(token)) {
			return false;
		}
		currPlayer.addToken(token);
		return true;
	}
	
	public boolean hasWon() {
		if (totalPlayers == 4 || totalPlayers == 5) {
			return currPlayer.numTokens() == 4;
		} else {
			return currPlayer.numTokens() == 5;
		}
		
	}
	
	public int getTotalPlayers(){
		return totalPlayers;
	}
	public String getColour(){
		return colour;
	}
	
	public void removePlayer(int id){
		
		if(this.getPlayer(id) == currPlayer){
			nextPlayer();
		}
		
		players.remove(this.getPlayer(id));
		totalPlayers--;
	}
	//server get's hands, own hand, #cards in opponent's hands, whose turn it is?
}