package ivanhoe;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import org.junit.Before;

import java.util.ArrayList;

public class TestGame3 {
	Game game;

	/** This will be processed before the Test Class is instantiated */
    @BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass: TestGame");
    }

    /** This is processed/initialized before each test is conducted */
    @Before
    public void setUp() {
		System.out.println("@Before(): TestGame");
		game = new Game(3);
	}
	
    /** This is processed/initialized after each test is conducted */
    @After
    public void tearDown () {
		System.out.println("@After(): TestGame");
		game = null;
	}
	
    /** This will be processed after the Test Class has been destroyed */
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass: TestGame");
    }
    
    /** Each unit test is annotated with the @Test Annotation */
    
    //test playing the outmaneuver card when one of the players has less then 2 cards (can't play)
    @Test
	public void playOutmaneuver1() {
    	System.out.println("@Test: playOutmaneuver1");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).addToHand(new Card("Purple", 3));
    	game.playCard("Purple 3");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4002).addToHand(new Card("Squire", 3));
    	game.getPlayer(4002).addToHand(new Card("Purple", 4));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4003).addToHand(new Card("Outmaneuver", 0));
    	assertTrue(game.playCard("Outmaneuver 0").equalsIgnoreCase("Bad"));
    	assertTrue(game.getPlayer(4001).getScore() == 3);
    	assertTrue(game.getPlayer(4002).getScore() == 7);
    	assertFalse(game.getPlayer(4003).hasPlayedACard);
    	assertEquals(0, game.getPlayer(4003).numPlayedDisplay);
    }
    
    //test playing the outmaneuver card when both other players have more then 1 card (can play)
    @Test
	public void playOutmaneuver2() {
    	System.out.println("@Test: playOutmaneuver2");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).addToHand(new Card("Purple", 3));
    	game.getPlayer(4001).addToHand(new Card("Purple", 3));
    	game.playCard("Purple 3");
    	game.playCard("Purple 3");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4002).addToHand(new Card("Squire", 3));
    	game.getPlayer(4002).addToHand(new Card("Purple", 4));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4003).addToHand(new Card("Outmaneuver", 0));
    	assertTrue(game.playCard("Outmaneuver 0").equalsIgnoreCase("Good"));
    	assertTrue(game.getPlayer(4001).getScore() == 3);
    	assertTrue(game.getPlayer(4002).getScore() == 3);
    	assertTrue(game.getPlayer(4003).hasPlayedACard);
    }
    
    //test playing the charge card when even 1 players has less then 2 cards (can't play)
    @Test
	public void playCharge1() {
    	System.out.println("@Test: playCharge1");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	//Should be unchanged
    	game.getPlayer(4001).addToHand(new Card("Purple", 3));
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.playCard("Purple 3");
    	game.playCard("Squire 3");
    	assertFalse(game.endTurn());
    	
    	//Should be unchanged
    	game.getPlayer(4002).addToHand(new Card("Squire", 3));
    	game.getPlayer(4002).addToHand(new Card("Purple", 4));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	assertFalse(game.endTurn());
    	
    	//Should be unchanged
    	game.getPlayer(4003).addToHand(new Card("Maiden", 6));
    	game.playCard("Maiden 6");
    	
    	game.getPlayer(4003).addToHand(new Card("Charge", 0));
    	assertTrue(game.playCard("Charge 0").equalsIgnoreCase("Bad"));
    	
    	//Check player 4001
    	assertTrue(game.getPlayer(4001).getScore() == 6);
    	assertEquals(2, game.getPlayer(4001).getDisplay().size());
    	
    	//Check player 4002
    	assertTrue(game.getPlayer(4002).getScore() == 7);
    	assertEquals(2, game.getPlayer(4002).getDisplay().size());
    	
    	//Check player 4003
    	assertTrue(game.getPlayer(4003).getScore() == 6);
    	assertEquals(1, game.getPlayer(4003).getDisplay().size());
    }
    
    //test playing the charge card when all players have more then 1 card (can play)
    //includes case when all player's display cards are the smallest value (4001)
    //includes case where player doesn't have any cards of the smallest value in their display (4003)
    @Test
	public void playCharge2() {
    	System.out.println("@Test: playCharge2");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	//should keep purple 3
    	game.getPlayer(4001).addToHand(new Card("Purple", 3));
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.playCard("Purple 3");
    	game.playCard("Squire 3");
    	assertFalse(game.endTurn());
    	
    	//should keep purple 4
    	game.getPlayer(4002).addToHand(new Card("Squire", 3));
    	game.getPlayer(4002).addToHand(new Card("Purple", 4));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	assertFalse(game.endTurn());
    	
    	//should keep both cards
    	game.getPlayer(4003).addToHand(new Card("Maiden", 6));
    	game.getPlayer(4003).addToHand(new Card("Purple", 4));
    	game.playCard("Maiden 6");
    	game.playCard("Purple 4");
    	
    	game.getPlayer(4003).addToHand(new Card("Charge", 0));
    	assertTrue(game.playCard("Charge 0").equalsIgnoreCase("Good"));
    	
    	//Check player 4001
    	assertTrue(game.getPlayer(4001).getScore() == 3);
    	assertEquals(1, game.getPlayer(4001).getDisplay().size());
    	assertEquals("Purple", game.getPlayer(4001).getDisplay().get(0).getName());
    	
    	//Check player 4002
    	assertTrue(game.getPlayer(4002).getScore() == 4);
    	assertEquals(1, game.getPlayer(4002).getDisplay().size());
    	assertEquals("Purple", game.getPlayer(4001).getDisplay().get(0).getName());
    	
    	//Check player 4003
    	assertTrue(game.getPlayer(4003).hasPlayedACard);
    	assertTrue(game.getPlayer(4003).getScore() == 10);
    	assertEquals(2, game.getPlayer(4003).getDisplay().size());
    }
    
    //test playing the charge card works properly when all PLAYING players have more then 1 card (can play)
    @Test
	public void playCharge3() {
    	System.out.println("@Test: playCharge3");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	//should keep purple 3
    	game.getPlayer(4001).addToHand(new Card("Purple", 3));
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.playCard("Purple 3");
    	game.playCard("Squire 3");
    	assertFalse(game.endTurn());
    	
    	//should keep purple 4
    	game.getPlayer(4002).addToHand(new Card("Squire", 3));
    	game.getPlayer(4002).addToHand(new Card("Purple", 4));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	assertFalse(game.withdraw());
    	
    	//should keep both cards
    	game.getPlayer(4003).addToHand(new Card("Maiden", 6));
    	game.getPlayer(4003).addToHand(new Card("Purple", 4));
    	game.playCard("Maiden 6");
    	game.playCard("Purple 4");
    	
    	game.getPlayer(4003).addToHand(new Card("Charge", 0));
    	assertTrue(game.playCard("Charge 0").equalsIgnoreCase("Good"));
    	
    	//Check player 4001
    	assertTrue(game.getPlayer(4001).getScore() == 3);
    	assertEquals(1, game.getPlayer(4001).getDisplay().size());
    	assertEquals("Purple", game.getPlayer(4001).getDisplay().get(0).getName());
    	
    	//Check player 4002
    	assertTrue(game.getPlayer(4002).getScore() == 0);
    	assertEquals(0, game.getPlayer(4002).getDisplay().size());
    	
    	//Check player 4003
    	assertTrue(game.getPlayer(4003).hasPlayedACard);
    	assertTrue(game.getPlayer(4003).getScore() == 10);
    	assertEquals(2, game.getPlayer(4003).getDisplay().size());
    }
    
    //test playing the countercharge card when even 1 players has less then 2 cards (can't play)
    @Test
	public void playCountercharge1() {
    	System.out.println("@Test: playCountercharge1");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	//Should be unchanged
    	game.getPlayer(4001).addToHand(new Card("Purple", 3));
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.playCard("Purple 3");
    	game.playCard("Squire 3");
    	assertFalse(game.endTurn());
    	
    	//Should be unchanged
    	game.getPlayer(4002).addToHand(new Card("Squire", 3));
    	game.getPlayer(4002).addToHand(new Card("Purple", 4));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	assertFalse(game.endTurn());
    	
    	//Should be unchanged
    	game.getPlayer(4003).addToHand(new Card("Maiden", 6));
    	game.playCard("Maiden 6");
    	
    	game.getPlayer(4003).addToHand(new Card("CounterCharge", 0));
    	assertTrue(game.playCard("CounterCharge 0").equalsIgnoreCase("Bad"));
    	
    	//Check player 4001
    	assertTrue(game.getPlayer(4001).getScore() == 6);
    	assertEquals(2, game.getPlayer(4001).getDisplay().size());
    	
    	//Check player 4002
    	assertTrue(game.getPlayer(4002).getScore() == 7);
    	assertEquals(2, game.getPlayer(4002).getDisplay().size());
    	
    	//Check player 4003
    	assertTrue(game.getPlayer(4003).getScore() == 6);
    	assertEquals(1, game.getPlayer(4003).getDisplay().size());
    }
    
    //test playing the countercharge card when all players have more then 1 card (can play)
    //includes case when all player's display cards are the largest value (4002)
    //includes case where player doesn't have any cards of the largest value in their display (4003)
    @Test
	public void playCounterCharge2() {
    	System.out.println("@Test: playCounterCharge2");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	//should keep squire 2
    	game.getPlayer(4001).addToHand(new Card("Purple", 3)); //3 is the highest value
    	game.getPlayer(4001).addToHand(new Card("Squire", 2));
    	game.playCard("Purple 3");
    	game.playCard("Squire 2");
    	assertFalse(game.endTurn());
    	
    	//should keep squire 3
    	game.getPlayer(4002).addToHand(new Card("Squire", 3));
    	game.getPlayer(4002).addToHand(new Card("Purple", 3));
    	game.playCard("Squire 3");
    	game.playCard("Purple 3");
    	assertFalse(game.endTurn());
    	
    	//should keep both cards
    	game.getPlayer(4003).addToHand(new Card("Squire", 2));
    	game.getPlayer(4003).addToHand(new Card("Squire", 2));
    	game.playCard("Squire 2");
    	game.playCard("Squire 2");
    	
    	game.getPlayer(4003).addToHand(new Card("CounterCharge", 0));
    	assertTrue(game.playCard("CounterCharge 0").equalsIgnoreCase("Good"));
    	
    	//Check player 4001
    	assertTrue(game.getPlayer(4001).getScore() == 2);
    	assertEquals(1, game.getPlayer(4001).getDisplay().size());
    	assertEquals("Squire", game.getPlayer(4001).getDisplay().get(0).getName());
    	
    	//Check player 4002
    	assertTrue(game.getPlayer(4002).getScore() == 3);
    	assertEquals(1, game.getPlayer(4002).getDisplay().size());
    	assertEquals("Squire", game.getPlayer(4001).getDisplay().get(0).getName());
    	
    	//Check player 4003
    	assertTrue(game.getPlayer(4003).getScore() == 4);
    	assertEquals(2, game.getPlayer(4003).getDisplay().size());
    }
    
  	//test playing the countercharge card works when all PLAYING players have more then 1 card (can play)
    @Test
	public void playCounterCharge3() {
    	System.out.println("@Test: playCounterCharge3");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	//should keep squire 2
    	game.getPlayer(4001).addToHand(new Card("Purple", 3)); //3 is the highest value
    	game.getPlayer(4001).addToHand(new Card("Squire", 2));
    	game.playCard("Purple 3");
    	game.playCard("Squire 2");
    	assertFalse(game.endTurn());
    	
    	//should keep squire 3
    	game.getPlayer(4002).addToHand(new Card("Squire", 3));
    	game.getPlayer(4002).addToHand(new Card("Purple", 3));
    	game.playCard("Squire 3");
    	game.playCard("Purple 3");
    	assertFalse(game.withdraw());
    	
    	//should keep both cards
    	game.getPlayer(4003).addToHand(new Card("Squire", 2));
    	game.getPlayer(4003).addToHand(new Card("Squire", 2));
    	game.playCard("Squire 2");
    	game.playCard("Squire 2");
    	
    	game.getPlayer(4003).addToHand(new Card("CounterCharge", 0));
    	assertTrue(game.playCard("CounterCharge 0").equalsIgnoreCase("Good"));
    	
    	//Check player 4001
    	assertTrue(game.getPlayer(4001).getScore() == 2);
    	assertEquals(1, game.getPlayer(4001).getDisplay().size());
    	assertEquals("Squire", game.getPlayer(4001).getDisplay().get(0).getName());
    	
    	//Check player 4002
    	assertTrue(game.getPlayer(4002).getScore() == 0);
    	assertEquals(0, game.getPlayer(4002).getDisplay().size());
    	
    	//Check player 4003
    	assertTrue(game.getPlayer(4003).getScore() == 4);
    	assertEquals(2, game.getPlayer(4003).getDisplay().size());
    }
    
  //test playing the disgrace card when even 1 player has less then 2 cards (can't play)
    @Test
	public void playDisgrace1() {
    	System.out.println("@Test: playDisgrace1");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	//We empty all the hands so we can check that disgrace is removing the cards properly
    	
    	//Should be unchanged
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3)); 
    	game.getPlayer(4001).addToHand(new Card("Purple", 4));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	assertFalse(game.endTurn());
    	
    	//Should be unchanged
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Purple", 4));
    	game.getPlayer(4002).addToHand(new Card("Purple", 5));
    	game.playCard("Purple 4");
    	game.playCard("Purple 5");
    	assertFalse(game.endTurn());
    	
    	//Should be unchanged
    	game.getPlayer(4003).getHand().clear();
    	game.getPlayer(4003).addToHand(new Card("Squire", 2));
    	game.playCard("Squire 2");
    	
    	game.getPlayer(4003).addToHand(new Card("Disgrace", 0));
    	assertTrue(game.playCard("Disgrace 0").equalsIgnoreCase("Bad"));
    	
    	//Check player 4001
    	assertTrue(game.getPlayer(4001).getScore() == 7);
    	assertEquals(2, game.getPlayer(4001).getDisplay().size());
    	assertEquals(0, game.getPlayer(4001).getHandSize());
    	
    	//Check player 4002
    	assertTrue(game.getPlayer(4002).getScore() == 9);
    	assertEquals(2, game.getPlayer(4002).getDisplay().size());
    	assertEquals(0, game.getPlayer(4002).getHandSize());
    	
    	//Check player 4003
    	assertTrue(game.getPlayer(4003).getScore() == 2);
    	assertEquals(1, game.getPlayer(4003).getDisplay().size());
    	assertEquals(1, game.getPlayer(4003).getHandSize()); //cause disgrace wasn't played
    }
    
    //test playing the disgrace card when all players have more then 1 card (can play)
    //includes case when all player's display cards are supporters (4003)
    //includes case where player doesn't have any supporter cards in their display (4002)
    @Test
	public void playDisgrace2() {
    	System.out.println("@Test: playDisgrace2");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	//We empty all the hands so we can check that disgrace is removing the cards properly
    	
    	//Should keep purple 4
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3)); 
    	game.getPlayer(4001).addToHand(new Card("Purple", 4));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	assertFalse(game.endTurn());
    	
    	//Should keep both cards
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Purple", 4));
    	game.getPlayer(4002).addToHand(new Card("Purple", 5));
    	game.playCard("Purple 4");
    	game.playCard("Purple 5");
    	assertFalse(game.endTurn());
    	
    	//Should keep squire 2
    	game.getPlayer(4003).getHand().clear();
    	game.getPlayer(4003).addToHand(new Card("Squire", 2));
    	game.getPlayer(4003).addToHand(new Card("Maiden", 6));
    	game.playCard("Squire 2");
    	game.playCard("Maiden 6");
    	
    	game.getPlayer(4003).addToHand(new Card("Disgrace", 0));
    	assertTrue(game.playCard("Disgrace 0").equalsIgnoreCase("Good"));
    	
    	//Check player 4001
    	assertTrue(game.getPlayer(4001).getScore() == 4);
    	assertEquals(1, game.getPlayer(4001).getDisplay().size());
    	assertEquals("Purple", game.getPlayer(4001).getDisplay().get(0).getName());
    	assertEquals(1, game.getPlayer(4001).getHandSize());
    	
    	//Check player 4002
    	assertTrue(game.getPlayer(4002).getScore() == 9);
    	assertEquals(2, game.getPlayer(4002).getDisplay().size());
    	assertEquals(0, game.getPlayer(4002).getHandSize());
    	
    	//Check player 4003
    	assertTrue(game.getPlayer(4003).getScore() == 2);
    	assertEquals(1, game.getPlayer(4003).getDisplay().size());
    	assertEquals("Squire", game.getPlayer(4003).getDisplay().get(0).getName());
    	assertEquals(1, game.getPlayer(4003).getHandSize());
    }
    
    //test playing the disgrace card works when all PLAYING players have more then 1 card (can play)
    @Test
	public void playDisgrace3() {
    	System.out.println("@Test: playDisgrace3");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	//We empty all the hands so we can check that disgrace is removing the cards properly
    	
    	//Should keep purple 4
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3)); 
    	game.getPlayer(4001).addToHand(new Card("Purple", 4));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	assertFalse(game.endTurn());
    	
    	//Should keep both cards
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Purple", 4));
    	game.getPlayer(4002).addToHand(new Card("Purple", 5));
    	game.playCard("Purple 4");
    	game.playCard("Purple 5");
    	assertFalse(game.withdraw());
    	
    	//Should keep squire 2
    	game.getPlayer(4003).getHand().clear();
    	game.getPlayer(4003).addToHand(new Card("Squire", 2));
    	game.getPlayer(4003).addToHand(new Card("Maiden", 6));
    	game.playCard("Squire 2");
    	game.playCard("Maiden 6");
    	
    	game.getPlayer(4003).addToHand(new Card("Disgrace", 0));
    	assertTrue(game.playCard("Disgrace 0").equalsIgnoreCase("Good"));
    	
    	//Check player 4001
    	assertTrue(game.getPlayer(4001).getScore() == 4);
    	assertEquals(1, game.getPlayer(4001).getDisplay().size());
    	assertEquals("Purple", game.getPlayer(4001).getDisplay().get(0).getName());
    	assertEquals(1, game.getPlayer(4001).getHandSize());
    	
    	//Check player 4002
    	assertTrue(game.getPlayer(4002).getScore() == 0);
    	assertEquals(0, game.getPlayer(4002).getDisplay().size());
    	assertEquals(0, game.getPlayer(4002).getHandSize());
    	
    	//Check player 4003
    	assertTrue(game.getPlayer(4003).getScore() == 2);
    	assertEquals(1, game.getPlayer(4003).getDisplay().size());
    	assertEquals("Squire", game.getPlayer(4003).getDisplay().get(0).getName());
    	assertEquals(1, game.getPlayer(4003).getHandSize());
    }
    
    //test playing the adapt card (can play)
    //4001 has all cards of the same value
    //4002 has multiple cards of one value
    //4003 has multiple cards or 2 values and unique value card
    @Test
	public void playAdapt1() {
    	System.out.println("@Test: playAdapt1");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("B");
    	
    	//Should return [3]
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Blue", 3)); 
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.playCard("Blue 3");
    	game.playCard("Squire 3");
    	assertFalse(game.endTurn());
    	
    	//Should return [4]
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Blue", 4)); 
    	game.getPlayer(4002).addToHand(new Card("Squire", 2));
    	game.getPlayer(4002).addToHand(new Card("Blue", 4));
    	game.playCard("Blue 4");
    	game.playCard("Squire 2");
    	game.playCard("Blue 4");
    	assertFalse(game.endTurn());
    	
    	//Should return [3, 2]
    	game.getPlayer(4003).getHand().clear();
    	game.getPlayer(4003).addToHand(new Card("Blue", 3)); 
    	game.getPlayer(4003).addToHand(new Card("Squire", 2));
    	game.getPlayer(4003).addToHand(new Card("Blue", 2));
    	game.getPlayer(4003).addToHand(new Card("Squire", 3));
    	game.getPlayer(4003).addToHand(new Card("Blue", 2));
    	game.getPlayer(4003).addToHand(new Card("Blue", 4));
    	game.playCard("Blue 3");
    	game.playCard("Squire 2");
    	game.playCard("Blue 2");
    	game.playCard("Squire 3");
    	game.playCard("Blue 2");
    	game.playCard("Blue 4");
    	
    	game.getPlayer(4003).addToHand(new Card("Adapt", 0));
    	assertTrue(game.playCard("Adapt 0").equalsIgnoreCase("Adapt"));
    	
    	//Check player 4001
    	ArrayList<Integer> list = game.findMultipleValueCards(4001);
    	assertEquals(1, list.size());
    	assertTrue(list.contains(3));
    	
    	game.removeExcept(4001, "Squire 3");
    	assertTrue(game.getPlayer(4001).getScore() == 3);
    	assertEquals(1, game.getPlayer(4001).getDisplay().size());
    	assertEquals("Squire", game.getPlayer(4001).getDisplay().get(0).getName());
    	
    	//Check player 4002
    	list = game.findMultipleValueCards(4002);
    	assertEquals(1, list.size());
    	assertTrue(list.contains(4));
    	
    	game.removeExcept(4002, "Blue 4");
    	
    	assertTrue(game.getPlayer(4002).getScore() == 6);
    	assertEquals(2, game.getPlayer(4002).getDisplay().size());
    	assertEquals("Blue", game.getPlayer(4002).getDisplay().get(0).getName());
    	assertEquals("Squire", game.getPlayer(4002).getDisplay().get(1).getName());
    	    	
    	//Check player 4003
    	list = game.findMultipleValueCards(4003);
    	assertEquals(2, list.size());
    	assertTrue(list.contains(3));
    	assertTrue(list.contains(2));
    	
    	game.removeExcept(4003, "Squire 3");
    	game.removeExcept(4003, "Blue 2");
    	
    	assertEquals(9, game.getPlayer(4003).getScore());
    	assertEquals(3, game.getPlayer(4003).getDisplay().size());
    }
    
    //test playing the adapt card (can play)
    //4001 has no cards of the same value
    //4002 has all cards of the same value
    //4003 has no cards in their display
    @Test
	public void playAdapt2() {
    	System.out.println("@Test: playAdapt2");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("B");
    	
    	//Should return [3]
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Blue", 3)); 
    	game.getPlayer(4001).addToHand(new Card("Squire", 2));
    	game.getPlayer(4001).addToHand(new Card("Blue", 4));
    	game.playCard("Blue 3");
    	game.playCard("Squire 2");
    	game.playCard("Blue 4");
    	assertFalse(game.endTurn());
    	
    	//Should return [4]
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Blue", 3)); 
    	game.getPlayer(4002).addToHand(new Card("Squire", 3));
    	game.getPlayer(4002).addToHand(new Card("Blue", 3));
    	game.getPlayer(4002).addToHand(new Card("Blue", 3));
    	game.playCard("Blue 3");
    	game.playCard("Squire 3");
    	game.playCard("Blue 3");
    	game.playCard("Blue 3");
    	assertFalse(game.endTurn());
    	
    	//Should return []
    	
    	game.getPlayer(4003).addToHand(new Card("Adapt", 0));
    	assertTrue(game.playCard("Adapt 0").equalsIgnoreCase("Adapt"));
    	
    	//Check player 4001
    	ArrayList<Integer> list = game.findMultipleValueCards(4001);
    	assertEquals(0, list.size());
    	
    	//Check player 4002
    	list = game.findMultipleValueCards(4002);
    	assertEquals(1, list.size());
    	assertTrue(list.contains(3));
    	
    	game.removeExcept(4002, "Blue 3");
    	
    	assertTrue(game.getPlayer(4002).getScore() == 3);
    	assertEquals(1, game.getPlayer(4002).getDisplay().size());
    	assertEquals("Blue", game.getPlayer(4002).getDisplay().get(0).getName());
    	    	
    	//Check player 4003
    	list = game.findMultipleValueCards(4003);
    	assertEquals(0, list.size());
    }
    
    //test playing the adapt card (can't play)
    //4001 has one card in their display
    //4002 has no cards of the same value
    //4003 has no cards in their display
    @Test
	public void playAdapt3() {
    	System.out.println("@Test: playAdapt3");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("B");
    	
    	//Should return []
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Blue", 3)); 
    	game.playCard("Blue 3");

    	assertFalse(game.endTurn());
    	
    	//Should return []
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Maiden", 6)); 
    	game.getPlayer(4002).addToHand(new Card("Blue", 4));
    	game.playCard("Maiden 6");
    	game.playCard("Blue 4");
    	assertFalse(game.endTurn());
    	
    	//Should return []
    	
    	game.getPlayer(4003).addToHand(new Card("Adapt", 0));
    	assertTrue(game.playCard("Adapt 0").equalsIgnoreCase("Bad"));
    }
    
    // test playing the shield card and playing a multiple target action card (outmaneuver)
    @Test
	public void playAgainstShield2() {
    	System.out.println("@Test: playAgainstShield2");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).addToHand(new Card("Purple", 3));
    	game.getPlayer(4001).addToHand(new Card("Purple", 3));
    	game.getPlayer(4001).addToHand(new Card("Shield", 0));
    	game.playCard("Purple 3");
    	game.playCard("Purple 3");
    	game.playCard("Shield 0");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4002).addToHand(new Card("Squire", 3));
    	game.getPlayer(4002).addToHand(new Card("Purple", 4));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4003).addToHand(new Card("Outmaneuver", 0));
    	assertTrue(game.playCard("Outmaneuver 0").equalsIgnoreCase("Good"));
    	assertTrue(game.getPlayer(4001).getScore() == 6);
    	assertTrue(game.getPlayer(4002).getScore() == 3);
    }
}