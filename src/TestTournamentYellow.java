package ivanhoe;

import static org.junit.Assert.*;
import java.util.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Before;

public class TestTournamentYellow {
	Game game;

	/** This will be processed before the Test Class is instantiated */
    @BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass: TestGame");
    }

    /** This is processed/initialized before each test is conducted */
    @Before
    public void setUp() {
		System.out.println("@Before(): TestGame");
		game = new Game(4);
	}
	
    /** This is processed/initialized after each test is conducted */
    @After
    public void tearDown () {
		System.out.println("@After(): TestGame");
		game = null;
	}
	
    /** This will be processed after the Test Class has been destroyed */
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass: TestGame");
    }
    
    /** Each unit test is annotated with the @Test Annotation */
    
    //one player draws/starts, others draw but do not participate (ie withdraw)
    @Test
	public void playTournamentA() {
    	System.out.println("@Test: playTournamentA");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.addPlayer(4004, "Mary");
    	game.beginGame();
    	game.setColour("Y");
    	
    	//The tournament does the draw for the first player then the nextPlayer function handles it.    	
    	game.getPlayer(4001).addToHand(new Card("Yellow", 3));
    	game.playCard("Yellow 3");
    	assertFalse(game.endTurn()); 
    	
    	//Draw is done automatically in the game, so withdraw 4002
    	assertFalse(game.withdraw());
    	
    	//Draw is done automatically in the game, so withdraw 4003
    	assertFalse(game.withdraw());
    	
    	//Check the tournament is still going
    	assertEquals(Game.INTOURNAMENT, game.state);
    	
    	//Draw is done automatically in the game, so withdraw 4004
    	assertFalse(game.withdraw());
    	
    	//Check the tournament has been won and assign the token
    	assertEquals(Game.WAITING, game.state);
    	assertFalse(game.endTournament()); //no choosing token
    	assertEquals(1, game.getPlayer(4001).getTokens().size());
    	assertTrue(game.getPlayer(4001).getTokens().contains('Y'));
    }
    
    //one player draws/starts, others draw but only one participates by playing i) a card
    @Test
	public void playTournamentB1() {
    	System.out.println("@Test: playTournamentB1");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.addPlayer(4004, "Mary");
    	game.beginGame();
    	game.setColour("Y");
    	
    	//The tournament does the draw for the first player then the nextPlayer function handles it.    	
    	game.getPlayer(4001).addToHand(new Card("Yellow", 3));
    	game.playCard("Yellow 3");
    	assertFalse(game.endTurn()); 
    	
    	//Draw is done automatically in the game, 4002 is still playing
    	game.getPlayer(4002).addToHand(new Card("Yellow", 4));
    	game.playCard("Yellow 4");
    	assertFalse(game.endTurn());
    	
    	//Draw is done automatically in the game, so withdraw 4003
    	assertFalse(game.withdraw());
    	
    	//Draw is done automatically in the game, so withdraw 4004
    	assertFalse(game.withdraw());
    	
    	//Check the tournament is still going
    	assertEquals(Game.INTOURNAMENT, game.state);
    	
    	//Draw is done automatically in the game, so withdraw 4001
    	assertFalse(game.withdraw());
    	
    	//Check the tournament has been won and assign the token
    	assertEquals(Game.WAITING, game.state);
    	assertFalse(game.endTournament()); //no choosing token
    	assertEquals(1, game.getPlayer(4002).getTokens().size());
    	assertTrue(game.getPlayer(4002).getTokens().contains('Y'));
    }
    
    //one player draws/starts, others draw but only one participates by playing ii) several cards
    @Test
	public void playTournamentB2() {
    	System.out.println("@Test: playTournamentB2");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.addPlayer(4004, "Mary");
    	game.beginGame();
    	game.setColour("Y");
    	
    	//The tournament does the draw for the first player then the nextPlayer function handles it.    	
    	game.getPlayer(4001).addToHand(new Card("Yellow", 3));
    	game.playCard("Yellow 3");
    	assertFalse(game.endTurn()); 
    	
    	//Draw is done automatically in the game, 4002 is still playing
    	game.getPlayer(4002).addToHand(new Card("Yellow", 4));
    	game.getPlayer(4002).addToHand(new Card("Squire", 3));
    	game.playCard("Yellow 4");
    	game.playCard("Squire 3");
    	assertFalse(game.endTurn());
    	
    	//Draw is done automatically in the game, so withdraw 4003
    	assertFalse(game.withdraw());
    	
    	//Draw is done automatically in the game, so withdraw 4004
    	assertFalse(game.withdraw());
    	
    	//Check the tournament is still going
    	assertEquals(Game.INTOURNAMENT, game.state);
    	
    	//Draw is done automatically in the game, so withdraw 4001
    	assertFalse(game.withdraw());
    	
    	//Check the tournament has been won and assign the token
    	assertEquals(Game.WAITING, game.state);
    	assertFalse(game.endTournament()); //no choosing token
    	assertEquals(1, game.getPlayer(4002).getTokens().size());
    	assertTrue(game.getPlayer(4002).getTokens().contains('Y'));
    }
    
    //one player draws/starts, others draw and some participate by playing i) a card
    @Test
	public void playTournamentC1() {
    	System.out.println("@Test: playTournamentC1");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.addPlayer(4004, "Mary");
    	game.beginGame();
    	game.setColour("Y");
    	
    	//The tournament does the draw for the first player then the nextPlayer function handles it.    	
    	game.getPlayer(4001).addToHand(new Card("Yellow", 2));
    	game.playCard("Yellow 2");
    	assertFalse(game.endTurn()); 
    	
    	//Draw is done automatically in the game, 4002 is still playing
    	game.getPlayer(4002).addToHand(new Card("Yellow", 3));
    	game.playCard("Yellow 3");
    	assertFalse(game.endTurn());
    	
    	//Draw is done automatically in the game, 4003 is still playing
    	game.getPlayer(4003).addToHand(new Card("Yellow", 4));
    	game.playCard("Yellow 4");
    	assertFalse(game.endTurn());
    	
    	//Draw is done automatically in the game, so withdraw 4004
    	assertFalse(game.withdraw());
    	
    	//Draw is done automatically in the game, so withdraw 4001
    	assertFalse(game.withdraw());
    	
    	//Check the tournament is still going
    	assertEquals(Game.INTOURNAMENT, game.state);
    	
    	//Draw is done automatically in the game, so withdraw 4002
    	assertFalse(game.withdraw());
    	
    	//Check the tournament has been won and assign the token
    	assertEquals(Game.WAITING, game.state);
    	assertFalse(game.endTournament()); //no choosing token
    	assertEquals(1, game.getPlayer(4003).getTokens().size());
    	assertTrue(game.getPlayer(4003).getTokens().contains('Y'));
    }
    
    //one player draws/starts, others draw and some participate by playing ii) several cards
    @Test
	public void playTournamentC2() {
    	System.out.println("@Test: playTournamentC2");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.addPlayer(4004, "Mary");
    	game.beginGame();
    	game.setColour("Y");
    	
    	//The tournament does the draw for the first player then the nextPlayer function handles it.    	
    	game.getPlayer(4001).addToHand(new Card("Yellow", 2));
    	game.playCard("Yellow 2");
    	assertFalse(game.endTurn()); 
    	
    	//Draw is done automatically in the game, 4002 is still playing
    	game.getPlayer(4002).addToHand(new Card("Yellow", 3));
    	game.getPlayer(4002).addToHand(new Card("Squire", 2));
    	game.playCard("Yellow 3");
    	game.playCard("Squire 2");
    	assertFalse(game.endTurn());
    	
    	//Draw is done automatically in the game, 4003 is still playing
    	game.getPlayer(4003).addToHand(new Card("Squire", 3));
    	game.getPlayer(4003).addToHand(new Card("Yellow", 4));
    	game.playCard("Squire 3");
    	game.playCard("Yellow 4");
    	assertFalse(game.endTurn());
    	
    	//Draw is done automatically in the game, so withdraw 4004
    	assertFalse(game.withdraw());
    	
    	//Draw is done automatically in the game, so withdraw 4001
    	assertFalse(game.withdraw());
    	
    	//Check the tournament is still going
    	assertEquals(Game.INTOURNAMENT, game.state);
    	
    	//Draw is done automatically in the game, so withdraw 4002
    	assertFalse(game.withdraw());
    	
    	//Check the tournament has been won and assign the token
    	assertEquals(Game.WAITING, game.state);
    	assertFalse(game.endTournament()); //no choosing token
    	assertEquals(1, game.getPlayer(4003).getTokens().size());
    	assertTrue(game.getPlayer(4003).getTokens().contains('Y'));
    }
    
    //one player draws/starts, others draw and all participate by playing i) a card
    @Test
	public void playTournamentD1() {
    	System.out.println("@Test: playTournamentD1");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.addPlayer(4004, "Mary");
    	game.beginGame();
    	game.setColour("Y");
    	
    	//The tournament does the draw for the first player then the nextPlayer function handles it.    	
    	game.getPlayer(4001).addToHand(new Card("Yellow", 2));
    	game.playCard("Yellow 2");
    	assertFalse(game.endTurn()); 
    	
    	//Draw is done automatically in the game, 4002 is still playing
    	game.getPlayer(4002).addToHand(new Card("Yellow", 3));
    	game.playCard("Yellow 3");
    	assertFalse(game.endTurn());
    	
    	//Draw is done automatically in the game, 4003 is still playing
    	game.getPlayer(4003).addToHand(new Card("Yellow", 4));
    	game.playCard("Yellow 4");
    	assertFalse(game.endTurn());
    	
    	//Draw is done automatically in the game, 4004 is still playing
    	game.getPlayer(4003).addToHand(new Card("Maiden", 6));
    	game.playCard("Maiden 6");
    	assertFalse(game.endTurn());
    	
    	//Draw is done automatically in the game, so withdraw 4001
    	assertFalse(game.withdraw());
    	
    	//Draw is done automatically in the game, so withdraw 4002
    	assertFalse(game.withdraw());
    	
    	//Check the tournament is still going
    	assertEquals(Game.INTOURNAMENT, game.state);
    	
    	//Draw is done automatically in the game, so withdraw 4003
    	assertFalse(game.withdraw());
    	
    	//Check the tournament has been won and assign the token
    	assertEquals(Game.WAITING, game.state);
    	assertFalse(game.endTournament()); //no choosing token
    	assertEquals(1, game.getPlayer(4004).getTokens().size());
    	assertTrue(game.getPlayer(4004).getTokens().contains('Y'));
    }
    
    //one player draws/starts, others draw and all participate by playing ii) several cards
    @Test
	public void playTournamentD2() {
    	System.out.println("@Test: playTournamentD2");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.addPlayer(4004, "Mary");
    	game.beginGame();
    	game.setColour("Y");
    	
    	//The tournament does the draw for the first player then the nextPlayer function handles it.    	
    	game.getPlayer(4001).addToHand(new Card("Yellow", 2));
    	game.playCard("Yellow 2");
    	assertFalse(game.endTurn()); 
    	
    	//Draw is done automatically in the game, 4002 is still playing
    	game.getPlayer(4002).addToHand(new Card("Yellow", 2));
    	game.getPlayer(4002).addToHand(new Card("Squire", 2));
    	game.playCard("Yellow 2");
    	game.playCard("Squire 2");
    	assertFalse(game.endTurn());
    	
    	//Draw is done automatically in the game, 4003 is still playing
    	game.getPlayer(4003).addToHand(new Card("Yellow", 3));
    	game.getPlayer(4003).addToHand(new Card("Squire", 2));
    	game.playCard("Yellow 3");
    	game.playCard("Squire 2");
    	assertFalse(game.endTurn());
    	
    	//Draw is done automatically in the game, 4004 is still playing
    	game.getPlayer(4004).addToHand(new Card("Yellow", 4));
    	game.getPlayer(4004).addToHand(new Card("Yellow", 3));
    	game.playCard("Yellow 4");
    	game.playCard("Yellow 3");
    	assertFalse(game.endTurn());
    	
    	//Draw is done automatically in the game, so withdraw 4001
    	assertFalse(game.withdraw());
    	
    	//Draw is done automatically in the game, so withdraw 4002
    	assertFalse(game.withdraw());
    	
    	//Check the tournament is still going
    	assertEquals(Game.INTOURNAMENT, game.state);
    	
    	//Draw is done automatically in the game, so withdraw 4003
    	assertFalse(game.withdraw());
    	
    	//Check the tournament has been won and assign the token
    	assertEquals(Game.WAITING, game.state);
    	assertFalse(game.endTournament()); //no choosing token
    	assertEquals(1, game.getPlayer(4004).getTokens().size());
    	assertTrue(game.getPlayer(4004).getTokens().contains('Y'));
    }
    
    //starting with i) a supporter
    @Test
	public void playTournamentE1() {
    	System.out.println("@Test: playTournamentE1");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.addPlayer(4004, "Mary");
    	game.beginGame();
    	game.setColour("Y");
    	
    	//The tournament does the draw for the first player then the nextPlayer function handles it.    	
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.playCard("Squire 3");
    	assertFalse(game.endTurn()); 
    	
    	//Draw is done automatically in the game, 4002 is still playing
    	game.getPlayer(4002).addToHand(new Card("Yellow", 3));
    	game.getPlayer(4002).addToHand(new Card("Squire", 2));
    	game.playCard("Yellow 3");
    	game.playCard("Squire 2");
    	assertFalse(game.endTurn());
    	
    	//Draw is done automatically in the game, 4003 is still playing
    	game.getPlayer(4003).addToHand(new Card("Yellow", 4));
    	game.getPlayer(4003).addToHand(new Card("Squire", 2));
    	game.playCard("Yellow 4");
    	game.playCard("Squire 2");
    	assertFalse(game.endTurn());
    	
    	//Draw is done automatically in the game, 4004 is still playing
    	game.getPlayer(4004).addToHand(new Card("Yellow", 4));
    	game.getPlayer(4004).addToHand(new Card("Yellow", 3));
    	game.playCard("Yellow 4");
    	game.playCard("Yellow 3");
    	assertFalse(game.endTurn());
    	
    	//Draw is done automatically in the game, so withdraw 4001
    	assertFalse(game.withdraw());
    	
    	//Draw is done automatically in the game, so withdraw 4002
    	assertFalse(game.withdraw());
    	
    	//Check the tournament is still going
    	assertEquals(Game.INTOURNAMENT, game.state);
    	
    	//Draw is done automatically in the game, so withdraw 4003
    	assertFalse(game.withdraw());
    	
    	//Check the tournament has been won and assign the token
    	assertEquals(Game.WAITING, game.state);
    	assertFalse(game.endTournament()); //no choosing token
    	assertEquals(1, game.getPlayer(4004).getTokens().size());
    	assertTrue(game.getPlayer(4004).getTokens().contains('Y'));
    }
    
    //starting with ii) several supporters
    @Test
	public void playTournamentE2() {
    	System.out.println("@Test: playTournamentE2");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.addPlayer(4004, "Mary");
    	game.beginGame();
    	game.setColour("Y");
    	
    	//The tournament does the draw for the first player then the nextPlayer function handles it.    	
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.getPlayer(4001).addToHand(new Card("Squire", 2));
    	game.playCard("Squire 3");
    	game.playCard("Squire 2");
    	assertFalse(game.endTurn()); 
    	
    	//Draw is done automatically in the game, 4002 is still playing
    	game.getPlayer(4002).addToHand(new Card("Yellow", 3));
    	game.getPlayer(4002).addToHand(new Card("Squire", 3));
    	game.playCard("Yellow 3");
    	game.playCard("Squire 3");
    	assertFalse(game.endTurn());
    	
    	//Draw is done automatically in the game, 4003 is still playing
    	game.getPlayer(4003).addToHand(new Card("Yellow", 4));
    	game.getPlayer(4003).addToHand(new Card("Squire", 3));
    	game.playCard("Yellow 4");
    	game.playCard("Squire 3");
    	assertFalse(game.endTurn());
    	
    	//Draw is done automatically in the game, 4004 is still playing
    	game.getPlayer(4004).addToHand(new Card("Yellow", 4));
    	game.getPlayer(4004).addToHand(new Card("Maiden", 6));
    	game.playCard("Yellow 4");
    	game.playCard("Maiden 6");
    	assertFalse(game.endTurn());
    	
    	//Draw is done automatically in the game, so withdraw 4001
    	assertFalse(game.withdraw());
    	
    	//Draw is done automatically in the game, so withdraw 4002
    	assertFalse(game.withdraw());
    	
    	//Check the tournament is still going
    	assertEquals(Game.INTOURNAMENT, game.state);
    	
    	//Draw is done automatically in the game, so withdraw 4003
    	assertFalse(game.withdraw());
    	
    	//Check the tournament has been won and assign the token
    	assertEquals(Game.WAITING, game.state);
    	assertFalse(game.endTournament()); //no choosing token
    	assertEquals(1, game.getPlayer(4004).getTokens().size());
    	assertTrue(game.getPlayer(4004).getTokens().contains('Y'));
    }
    
    //players use several maidens and supporters in one round
    @Test
	public void playTournamentE3() {
    	System.out.println("@Test: playTournamentE3");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.addPlayer(4004, "Mary");
    	game.beginGame();
    	game.setColour("Y");
    	
    	//The tournament does the draw for the first player then the nextPlayer function handles it.    	
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.getPlayer(4001).addToHand(new Card("Squire", 2));
    	game.playCard("Squire 3");
    	game.playCard("Squire 2");
    	assertFalse(game.endTurn()); 
    	
    	//Draw is done automatically in the game, 4002 is still playing
    	game.getPlayer(4002).addToHand(new Card("Yellow", 3));
    	game.getPlayer(4002).addToHand(new Card("Squire", 3));
    	game.playCard("Yellow 3");
    	game.playCard("Squire 3");
    	assertFalse(game.endTurn());
    	
    	//Draw is done automatically in the game, 4003 is still playing
    	game.getPlayer(4003).addToHand(new Card("Yellow", 4));
    	game.getPlayer(4003).addToHand(new Card("Squire", 3));
    	game.playCard("Yellow 4");
    	game.playCard("Squire 3");
    	assertFalse(game.endTurn());
    	
    	//Draw is done automatically in the game, 4004 is still playing
    	game.getPlayer(4004).addToHand(new Card("Squire", 3));
    	game.getPlayer(4004).addToHand(new Card("Maiden", 6));
    	game.getPlayer(4004).addToHand(new Card("Squire", 2));
    	game.playCard("Squire 3");
    	game.playCard("Maiden 6");
    	game.playCard("Squire 2");
    	assertFalse(game.endTurn());
    	
    	//Draw is done automatically in the game, so withdraw 4001
    	assertFalse(game.withdraw());
    	
    	//Draw is done automatically in the game, so withdraw 4002
    	assertFalse(game.withdraw());
    	
    	//Check the tournament is still going
    	assertEquals(Game.INTOURNAMENT, game.state);
    	
    	//Draw is done automatically in the game, so withdraw 4003
    	assertFalse(game.withdraw());
    	
    	//Check the tournament has been won and assign the token
    	assertEquals(Game.WAITING, game.state);
    	assertFalse(game.endTournament()); //no choosing token
    	assertEquals(1, game.getPlayer(4004).getTokens().size());
    	assertTrue(game.getPlayer(4004).getTokens().contains('Y'));
    }
    
    //a multiplayer tournament has several rounds where each player plays one and then several 
    //supporters in different rounds
    @Test
	public void playTournamentF() {
    	System.out.println("@Test: playTournamentF");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.addPlayer(4004, "Mary");
    	game.beginGame();
    	game.setColour("Y");
    	
    	//The tournament does the draw for the first player then the nextPlayer function handles it.    	
    	game.getPlayer(4001).addToHand(new Card("Squire", 2));
    	game.playCard("Squire 2");
    	assertFalse(game.endTurn()); 
    	
    	//Draw is done automatically in the game, 4002 is still playing
    	game.getPlayer(4002).addToHand(new Card("Squire", 3));
    	game.playCard("Squire 3");
    	assertFalse(game.endTurn());
    	
    	//Draw is done automatically in the game, 4003 is still playing
    	game.getPlayer(4003).addToHand(new Card("Maiden", 6));
    	game.playCard("Maiden 6");
    	assertFalse(game.endTurn());
    	
    	//Draw is done automatically in the game, 4004 is still playing
    	game.getPlayer(4004).addToHand(new Card("Squire", 2));
    	game.getPlayer(4004).addToHand(new Card("Yellow", 4));
    	game.getPlayer(4004).addToHand(new Card("Yellow", 2));
    	game.playCard("Squire 2");
    	game.playCard("Yellow 4");
    	game.playCard("Yellow 2");
    	assertFalse(game.endTurn());
    	
    	//Draw is done automatically in the game, play and withdraw 4001
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.getPlayer(4001).addToHand(new Card("Squire", 2));
    	game.playCard("Squire 3");
    	game.playCard("Squire 2");
    	assertTrue(game.endTurn()); //must withdraw
    	assertFalse(game.withdraw());
    	
    	//Draw is done automatically in the game, so withdraw 4002
    	game.getPlayer(4002).addToHand(new Card("Squire", 2));
    	game.getPlayer(4002).addToHand(new Card("Squire", 2));
    	game.playCard("Squire 2");
    	game.playCard("Squire 2");
    	assertTrue(game.endTurn()); //must withdraw
    	assertFalse(game.withdraw());
    	
    	//Draw is done automatically in the game, 4003 is still playing
    	game.getPlayer(4003).addToHand(new Card("Squire", 3));
    	game.playCard("Squire 3");
    	assertFalse(game.endTurn());
    	
    	//Check the tournament is still going
    	assertEquals(Game.INTOURNAMENT, game.state);
    	
    	//Draw is done automatically in the game, so withdraw 4004
    	assertFalse(game.withdraw());
    	
    	//Check the tournament has been won and assign the token
    	assertEquals(Game.WAITING, game.state);
    	assertFalse(game.endTournament()); //no choosing token
    	assertEquals(1, game.getPlayer(4003).getTokens().size());
    	assertTrue(game.getPlayer(4003).getTokens().contains('Y'));
    }
    
    //trying to play cards that do not get the current player to beat the tournament originator
    @Test
	public void playTournamentG() {
    	System.out.println("@Test: playTournamentG");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.addPlayer(4004, "Mary");
    	game.beginGame();
    	game.setColour("Y");
    	
    	//The tournament does the draw for the first player then the nextPlayer function handles it.    	
    	game.getPlayer(4001).addToHand(new Card("Yellow", 4));
    	game.getPlayer(4001).addToHand(new Card("Yellow", 4));
    	game.playCard("Yellow 4");
    	game.playCard("Yellow 4");
    	assertFalse(game.endTurn()); 
    	
    	//Draw is done automatically in the game, 4002 is still playing
    	game.getPlayer(4002).addToHand(new Card("Yellow", 3));
    	game.getPlayer(4002).addToHand(new Card("Squire", 3));
    	game.playCard("Yellow 3");
    	game.playCard("Squire 3");
    	assertTrue(game.endTurn()); //must withdraw
    	assertFalse(game.withdraw());
    	
    	//Draw is done automatically in the game, 4003 is still playing
    	game.getPlayer(4003).addToHand(new Card("Yellow", 4));
    	game.getPlayer(4003).addToHand(new Card("Squire", 3));
    	game.playCard("Yellow 4");
    	game.playCard("Squire 3");
    	assertTrue(game.endTurn()); //must withdraw
    	assertFalse(game.withdraw());
    	
    	//Check the tournament is still going
    	assertEquals(Game.INTOURNAMENT, game.state);
    	
    	//Draw is done automatically in the game, 4004 is still playing
    	game.getPlayer(4004).addToHand(new Card("Yellow", 3));
    	game.getPlayer(4004).addToHand(new Card("Yellow", 4));
    	game.playCard("Yellow 3");
    	game.playCard("Yellow 4");
    	assertTrue(game.endTurn()); //must withdraw
    	assertFalse(game.withdraw());
    	
    	//Check the tournament has been won and assign the token
    	assertEquals(Game.WAITING, game.state);
    	assertFalse(game.endTournament()); //no choosing token
    	assertEquals(1, game.getPlayer(4001).getTokens().size());
    	assertTrue(game.getPlayer(4001).getTokens().contains('Y'));
    }
    
    //restriction to 1 maiden per player per tournament
    //Note: ever player has been given 2 maidens for testing purposes and this would never happen in a real game.
    @Test
	public void playTournamentH() {
    	System.out.println("@Test: playTournamentH");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.addPlayer(4004, "Mary");
    	game.beginGame();
    	game.setColour("Y");
    	
    	//The tournament does the draw for the first player then the nextPlayer function handles it.    	
    	game.getPlayer(4001).addToHand(new Card("Maiden", 6));
    	game.getPlayer(4001).addToHand(new Card("Maiden", 6));
    	assertEquals("Good", game.playCard("Maiden 6")); //can play
    	assertEquals("Bad", game.playCard("Maiden 6"));  //can't play
    	assertFalse(game.endTurn()); 
    	
    	//Draw is done automatically in the game, withdraw 4002
    	game.getPlayer(4002).addToHand(new Card("Maiden", 6));
    	game.getPlayer(4002).addToHand(new Card("Maiden", 6));
    	assertEquals("Good", game.playCard("Maiden 6")); //can play
    	assertEquals("Bad", game.playCard("Maiden 6"));  //can't play
    	game.getPlayer(4002).addToHand(new Card("Yellow", 3));
    	game.playCard("Yellow 3");
    	assertFalse(game.withdraw()); //player decides to withdraw
    	
    	//Draw is done automatically in the game, withdraw 4003
    	game.getPlayer(4003).addToHand(new Card("Maiden", 6));
    	game.getPlayer(4003).addToHand(new Card("Maiden", 6));
    	assertEquals("Good", game.playCard("Maiden 6")); //can play
    	assertEquals("Bad", game.playCard("Maiden 6"));  //can't play
    	assertTrue(game.endTurn()); //must withdraw
    	assertFalse(game.withdraw());
    	
    	//Check the tournament is still going
    	assertEquals(Game.INTOURNAMENT, game.state);
    	
    	//Draw is done automatically in the game, withdraw 4004
    	game.getPlayer(4004).addToHand(new Card("Maiden", 6));
    	game.getPlayer(4004).addToHand(new Card("Maiden", 6));
    	assertEquals("Good", game.playCard("Maiden 6")); //can play
    	assertEquals("Bad", game.playCard("Maiden 6"));  //can't play
    	assertTrue(game.endTurn()); //must withdraw
    	assertFalse(game.withdraw());
    	
    	//Check the tournament has been won and assign the token
    	assertEquals(Game.WAITING, game.state);
    	assertFalse(game.endTournament()); //no choosing token
    	assertEquals(1, game.getPlayer(4001).getTokens().size());
    	assertTrue(game.getPlayer(4001).getTokens().contains('Y'));
    }
    
    //test I is covered by any and all of the other tests here
    //test J is N/A because it's a red tournament
    
    //losing with a maiden and losing a token
    @Test
	public void playTournamentK() {
    	System.out.println("@Test: playTournamentK");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.addPlayer(4004, "Mary");
    	game.beginGame();
    	game.setColour("Y");
    	
    	//The tournament does the draw for the first player then the nextPlayer function handles it.    	
    	game.getPlayer(4001).addToHand(new Card("Yellow", 3));
    	game.playCard("Yellow 3");
    	assertFalse(game.endTurn()); 
    	
    	//Draw is done automatically in the game, withdraw 4002
    	assertFalse(game.withdraw());
    	
    	//Draw is done automatically in the game, withdraw 4003
    	assertFalse(game.withdraw());
    	
    	//Check the tournament is still going
    	assertEquals(Game.INTOURNAMENT, game.state);
    	
    	//Draw is done automatically in the game, withdraw 4004
    	assertFalse(game.withdraw());
    	
    	//Check the tournament has been won and assign the token
    	assertEquals(Game.WAITING, game.state);
    	assertFalse(game.endTournament()); //no choosing token
    	assertEquals(1, game.getPlayer(4001).getTokens().size());
    	assertTrue(game.getPlayer(4001).getTokens().contains('Y'));
    	
    	//Start a new tournament
    	game.beginTournament();
    	
    	//Withdraw 4001
    	game.getPlayer(4001).addToHand(new Card("Maiden", 6));
    	game.playCard("Maiden 6");
    	assertTrue(game.withdraw()); //loose a token
    	assertFalse(game.removeToken(4001, 'G'));
    	assertTrue(game.removeToken(4001, 'Y'));
    }
}