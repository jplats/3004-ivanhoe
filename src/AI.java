package ivanhoe;

abstract class AI {
	protected Player player;
	protected Game game;
	protected ServerThread thread;
	
	AI(Player p, Game g){
		player = p;
		game = g;
		
	}
	
	public boolean playTurn(){return true;}
	public void handle(String s){};
	public void setThread(ServerThread t){thread = t;}
	public void pickToken(){};
	public void pickTournamentColour(){};
	public boolean playNextCard(){return true;}
}


class MinAi extends AI{
	
	MinAi(Player p, Game g) {
		super(p, g);
	}
	
	public void handle(String message){
		System.out.println("AI: " + message);
		String parts[] = message.split(" ");
		if(message == "play"){
			playTurn();
		}else if(parts[0].equals("set")){
			pickTournamentColour();
		}else if(parts[0].equals("choose")){
			pickToken();
		}
	}
	
	public void pickTournamentColour(){
			//code to determine what tournament to choose here
			for(Card c: player.getHand()){
				if(!player.getTokens().contains(c.getName().charAt(0))){
					if(c.getName().substring(0, 1).equals("R") || c.getName().substring(0, 1).equals("B") ||
							c.getName().substring(0, 1).equals("G") || c.getName().substring(0, 1).equals("Y") || c.getName().substring(0, 1).equals("P") ){
								System.out.println("attempting to set: " + c.getName().substring(0, 1));
								//game.setColour(c.getName().substring(0, 1));
								thread.server.handle(thread.getID(),"set " +  c.getName().substring(0, 1));
								break;
					}
				}
			}
		
	}
	
	public boolean playTurn(){
		System.out.println("AI hand");
		for(Card c: player.getHand()){
			System.out.println(c.getName()+" "+c.getValue());
		}
		int max = 0;
		int min = 0;
		for(Player p: game.players){
			if(p.getScore() > max){
				max = p.getScore();
			}
		}
		for(Card c: player.getHand()){
			if(c.getName().equals(game.getColour()) || c.getName().equalsIgnoreCase("Maiden") || c.getName().equalsIgnoreCase("Squire")){
				min += c.getValue();
			}
		}
		
		if(game.checkCurrPlayer(player.getPortId())){
			
			if(min > max){
				do{
					if(!playNextCard()){
						System.out.println("no cards left to play and score not maxed");
						break;
					}
				}while(player.getScore() <= max);
			}
			
			try{
				Thread.sleep(1000);
			}catch(InterruptedException ie){
			}
			thread.server.handle(thread.getID(),("end"));
		}
		
		return true;
	}
	public boolean playNextCard(){

		Card cardToPlay = null;
		for(Card c: player.getHand()){
			if(c.getName().equals(game.getColour()) || c.getName().equalsIgnoreCase("Maiden") || c.getName().equalsIgnoreCase("Squire")){
				if(cardToPlay != null){
					if(c.getValue() < cardToPlay.getValue()) cardToPlay = c;
				}else{
					cardToPlay = c;
				}
			}
		}
		if(cardToPlay != null){
			try{
				Thread.sleep(1000);
			}catch(InterruptedException ie){
			}
			thread.server.handle(thread.getID(),("play " +cardToPlay.getName() + " " + cardToPlay.getValue()));
		}else{
			return false;
		}
		return true;
	}
	
	
}
class MaxAi extends AI{
	
	MaxAi(Player p, Game g) {
		super(p, g);
	}
	
	public void handle(String message){
		String parts[] = message.split(" ");
		if(message == "play"){
			playTurn();
		}else if(parts[0].equals("set")){
			pickTournamentColour();
		}else if(parts[0].equals("choose")){
			pickToken();
		}
	}
	
	public void pickTournamentColour(){
			//code to determine what tournament to choose here
			System.out.println("got here");
			int colours[] = new int[5];
			for(int c: colours){
				c = 0;
			}
			for(Card c: player.getHand()){
				if(c.getName().substring(0, 1).equals("R")){
					colours[0]++;
				}else if(c.getName().substring(0, 1).equals("B")){
					colours[1]++;
				}else if(c.getName().substring(0, 1).equals("P")){
					colours[2]++;
				}else if(c.getName().substring(0, 1).equals("G")){
					colours[3]++;
				}else if(c.getName().substring(0, 1).equals("Y")){
					colours[4]++;
				}
			}
			int i = 0;
			int j = 0;
			for(int c: colours){
				if(c > colours[j]){
					j = i;
				}
				i++;
			}
			if(j == 0){
				thread.server.handle(thread.getID(),"set R");
			}else if(j==1){
				thread.server.handle(thread.getID(),"set B");
			}else if(j==2){
				thread.server.handle(thread.getID(),"set p");
			}else if(j==3){
				thread.server.handle(thread.getID(),"set G");
			}else if(j==4){
				thread.server.handle(thread.getID(),"set Y");
			}
					
	}
	
	public boolean playTurn(){
		System.out.println("AI hand");
		for(Card c: player.getHand()){
			System.out.println(c.getName()+" "+c.getValue());
		}
		int max = 0;
		int min = 0;
		for(Player p: game.players){
			if(p.getScore() > max){
				max = p.getScore();
			}
		}
		for(Card c: player.getHand()){
			if(c.getName().equals(game.getColour()) || c.getName().equalsIgnoreCase("Maiden") || c.getName().equalsIgnoreCase("Squire")){
				min += c.getValue();
			}
		}
		
		if(game.checkCurrPlayer(player.getPortId())){
			
			if(min > max+2){
				
				while(playNextCard()){
					
				}
			}
			
			try{
				Thread.sleep(1000);
			}catch(InterruptedException ie){
			}
			thread.server.handle(thread.getID(),("end"));
		}
		
		return true;
	}
	public boolean playNextCard(){

		Card cardToPlay = null;
		for(Card c: player.getHand()){
			if(c.getName().equals(game.getColour()) || c.getName().equalsIgnoreCase("Maiden") || c.getName().equalsIgnoreCase("Squire")){
				if(cardToPlay != null){
					if(c.getValue() < cardToPlay.getValue()) cardToPlay = c;
				}else{
					cardToPlay = c;
				}
			}
		}
		if(cardToPlay != null){
			try{
				Thread.sleep(1000);
			}catch(InterruptedException ie){
			}
			thread.server.handle(thread.getID(),("play " +cardToPlay.getName() + " " + cardToPlay.getValue()));
		}else{
			return false;
		}
		return true;
	}
	
	
}