package ivanhoe;

import static org.junit.Assert.*;
import java.util.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Before;

public class TestShield {
	Game game;

	/** This will be processed before the Test Class is instantiated */
    @BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass: TestGame");
    }

    /** This is processed/initialized before each test is conducted */
    @Before
    public void setUp() {
		System.out.println("@Before(): TestGame");
		game = new Game(3);
	}
	
    /** This is processed/initialized after each test is conducted */
    @After
    public void tearDown () {
		System.out.println("@After(): TestGame");
		game = null;
	}
	
    /** This will be processed after the Test Class has been destroyed */
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass: TestGame");
    }
    
    /** Each unit test is annotated with the @Test Annotation */
    
    // Test the shield stopping the Unhorse action card
    @Test
	public void playShieldUnhorse() {
    	System.out.println("@Test: playShieldUnhorse");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.getPlayer(4001).addToHand(new Card("Purple", 4));
    	game.getPlayer(4001).addToHand(new Card("Shield", 0));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	game.playCard("Shield 0");
    	assertFalse(game.endTurn());
    	
    	//Check that 4001 is shielded
    	assertTrue(game.getPlayer(4001).getShield());
    	
    	//Play Unhorse against 4001
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Unhorse", 0));
    	assertEquals("colour", game.playCard("Unhorse 0"));
    	
    	//Logic handled outside the game (ie. by the server)
    	assertTrue(game.validColour("B"));
    	game.setColour("B");
    	game.getPlayer(4002).removeFromHand("Unhorse", 0);
    	
    	//Check that the colour has been changed
    	assertEquals("Blue", game.getColour());
    	
    	//Check that 4001 is affected
    	assertEquals(2, game.getPlayer(4001).getDisplaySize());
    	assertEquals(0, game.getPlayer(4001).getHandSize());
    	assertEquals(7, game.getPlayer(4001).getScore());
    	
    	//Check that 4002 is affected
    	assertEquals(0, game.getPlayer(4002).getDisplaySize());
    	assertEquals(0, game.getPlayer(4002).getHandSize());
    	assertEquals(0, game.getPlayer(4002).getScore());
    	assertFalse(game.getPlayer(4002).hasPlayedACard);
    }
    
    // Test the shield stopping the ChangeWeapon action card
    @Test
	public void playShieldChangeWeapon() {
    	System.out.println("@Test: playShieldChangeWeapon");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("R");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.getPlayer(4001).addToHand(new Card("Red", 4));
    	game.getPlayer(4001).addToHand(new Card("Shield", 0));
    	game.playCard("Squire 3");
    	game.playCard("Red 4");
    	game.playCard("Shield 0");
    	assertFalse(game.endTurn());
    	
    	//Check that 4001 is shielded
    	assertTrue(game.getPlayer(4001).getShield());
    	
    	//Play riposte against 4001
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("ChangeWeapon", 0));
    	assertEquals("colour", game.playCard("ChangeWeapon 0"));
    	
    	//Logic handled outside the game (ie. by the server)
    	assertTrue(game.validColour("B"));
    	game.setColour("B");
    	game.getPlayer(4002).removeFromHand("ChangeWeapon", 0);
    	
    	//Check that the colour has been changed
    	assertEquals("Blue", game.getColour());
    	
    	//Check that 4001 is affected
    	assertEquals(2, game.getPlayer(4001).getDisplaySize());
    	assertEquals(0, game.getPlayer(4001).getHandSize());
    	assertEquals(7, game.getPlayer(4001).getScore());
    	
    	//Check that 4002 is affected
    	assertEquals(0, game.getPlayer(4002).getDisplaySize());
    	assertEquals(0, game.getPlayer(4002).getHandSize());
    	assertEquals(0, game.getPlayer(4002).getScore());
    	assertFalse(game.getPlayer(4002).hasPlayedACard);
    }
    
    // Test the shield stopping the DropWeapon action card
    @Test
	public void playShieldDropWeapon() {
    	System.out.println("@Test: playShieldDropWeapon");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("R");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.getPlayer(4001).addToHand(new Card("Red", 4));
    	game.getPlayer(4001).addToHand(new Card("Shield", 0));
    	game.playCard("Squire 3");
    	game.playCard("Red 4");
    	game.playCard("Shield 0");
    	assertFalse(game.endTurn());
    	
    	//Check that 4001 is shielded
    	assertTrue(game.getPlayer(4001).getShield());
    	
    	//Play riposte against 4001
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("DropWeapon", 0));
    	assertEquals("Good", game.playCard("DropWeapon 0"));
    	//assertFalse(game.playAgainstTarget("DropWeapon 0", "4001", ""));
    	
    	//Check that the colour has been changed
    	assertEquals("Green", game.getColour());
    	
    	//Check that 4001 is affected
    	assertEquals(2, game.getPlayer(4001).getDisplaySize());
    	assertEquals(0, game.getPlayer(4001).getHandSize());
    	assertEquals(2, game.getPlayer(4001).getScore());
    	
    	//Check that 4002 is affected
    	assertEquals(0, game.getPlayer(4002).getDisplaySize());
    	assertEquals(0, game.getPlayer(4002).getHandSize());
    	assertEquals(0, game.getPlayer(4002).getScore());
    	assertTrue(game.getPlayer(4002).hasPlayedACard);
    }
    
    // Test the shield stopping the BreakLance action card
    @Test
	public void playShieldBreakLance() {
    	System.out.println("@Test: playShieldBreakLance");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.getPlayer(4001).addToHand(new Card("Purple", 4));
    	game.getPlayer(4001).addToHand(new Card("Shield", 0));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	game.playCard("Shield 0");
    	assertFalse(game.endTurn());
    	
    	//Check that 4001 is shielded
    	assertTrue(game.getPlayer(4001).getShield());
    	
    	//Play riposte against 4001
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("BreakLance", 0));
    	assertEquals("target", game.playCard("BreakLance 0"));
    	assertFalse(game.playAgainstTarget("BreakLance 0", "4001", ""));
    	
    	//Check that 4001 is unaffected
    	assertEquals(2, game.getPlayer(4001).getDisplaySize());
    	assertEquals(0, game.getPlayer(4001).getHandSize());
    	assertEquals(7, game.getPlayer(4001).getScore());
    	
    	//Check that 4002 is unaffected
    	assertEquals(0, game.getPlayer(4002).getDisplaySize());
    	assertEquals(1, game.getPlayer(4002).getHandSize());
    	assertEquals(0, game.getPlayer(4002).getScore());
    	assertFalse(game.getPlayer(4002).hasPlayedACard);
    }
    
    // Test the shield stopping the Riposte action card
    @Test
	public void playShieldRiposte() {
    	System.out.println("@Test: playShieldRiposte");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.getPlayer(4001).addToHand(new Card("Purple", 4));
    	game.getPlayer(4001).addToHand(new Card("Shield", 0));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	game.playCard("Shield 0");
    	assertFalse(game.endTurn());
    	
    	//Check that 4001 is shielded
    	assertTrue(game.getPlayer(4001).getShield());
    	
    	//Play riposte against 4001
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Riposte", 0));
    	assertEquals("target", game.playCard("Riposte 0"));
    	assertFalse(game.playAgainstTarget("Riposte 0", "4001", ""));
    	
    	//Check that 4001 is unaffected
    	assertEquals(2, game.getPlayer(4001).getDisplaySize());
    	assertEquals(0, game.getPlayer(4001).getHandSize());
    	assertEquals(7, game.getPlayer(4001).getScore());
    	
    	//Check that 4002 is unaffected
    	assertEquals(0, game.getPlayer(4002).getDisplaySize());
    	assertEquals(1, game.getPlayer(4002).getHandSize());
    	assertEquals(0, game.getPlayer(4002).getScore());
    	assertFalse(game.getPlayer(4002).hasPlayedACard);
    }
    
    // Test the shield stopping the Dodge action card
    @Test
	public void playShieldDodge() {
    	System.out.println("@Test: playShieldDodge");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.getPlayer(4001).addToHand(new Card("Purple", 4));
    	game.getPlayer(4001).addToHand(new Card("Shield", 0));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	game.playCard("Shield 0");
    	assertFalse(game.endTurn());
    	
    	//Check that 4001 is shielded
    	assertTrue(game.getPlayer(4001).getShield());
    	
    	//Play riposte against 4001
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Dodge", 0));
    	assertEquals("targetDisplayCard", game.playCard("Dodge 0"));
    	assertFalse(game.playAgainstTarget("Dodge 0", "4001", "0"));
    	
    	//Check that 4001 is unaffected
    	assertEquals(2, game.getPlayer(4001).getDisplaySize());
    	assertEquals(0, game.getPlayer(4001).getHandSize());
    	assertEquals(7, game.getPlayer(4001).getScore());
    	
    	//Check that 4002 is unaffected
    	assertEquals(0, game.getPlayer(4002).getDisplaySize());
    	assertEquals(1, game.getPlayer(4002).getHandSize());
    	assertEquals(0, game.getPlayer(4002).getScore());
    	assertFalse(game.getPlayer(4002).hasPlayedACard);
    }
    
    // Test the shield stopping the Retreat action card (can be played!)
    @Test
	public void playShieldRetreat() {
    	System.out.println("@Test: playShieldRetreat");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.getPlayer(4001).addToHand(new Card("Purple", 4));
    	game.getPlayer(4001).addToHand(new Card("Shield", 0));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	game.playCard("Shield 0");
    	
    	//Check that 4001 is shielded
    	assertTrue(game.getPlayer(4001).getShield());
    	
    	//Play Retreat against 4001
    	game.getPlayer(4001).addToHand(new Card("Retreat", 0));
    	assertEquals("ownDisplayCard", game.playCard("Retreat 0"));
    	assertTrue(game.playAgainstTarget("Retreat 0", "4001", "0"));
    	
    	//Check that 4001 is affected
    	assertEquals(1, game.getPlayer(4001).getDisplaySize());
    	assertEquals(1, game.getPlayer(4001).getHandSize());
    	assertEquals(4, game.getPlayer(4001).getScore());
    }
    
    // Test the shield stopping the KnockDown action card
    @Test
	public void playShieldKnockDown() {
    	System.out.println("@Test: playShieldKnockDown");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.getPlayer(4001).addToHand(new Card("Purple", 4));
    	game.getPlayer(4001).addToHand(new Card("Shield", 0));
    	game.playCard("Squire 3");
    	game.playCard("Shield 0");
    	assertFalse(game.endTurn());
    	
    	//Check that 4001 is shielded
    	assertTrue(game.getPlayer(4001).getShield());
    	
    	//Play riposte against 4001
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("KnockDown", 0));
    	assertEquals("targetHandCard", game.playCard("KnockDown 0"));
    	assertTrue(game.playAgainstTarget("KnockDown 0", "4001", "0"));
    	
    	//Check that 4001 is unaffected
    	assertEquals(1, game.getPlayer(4001).getDisplaySize());
    	assertEquals(0, game.getPlayer(4001).getHandSize());
    	assertEquals(3, game.getPlayer(4001).getScore());
    	
    	//Check that 4002 is unaffected
    	assertEquals(0, game.getPlayer(4002).getDisplaySize());
    	assertEquals(1, game.getPlayer(4002).getHandSize());
    	assertEquals(0, game.getPlayer(4002).getScore());
    	assertTrue(game.getPlayer(4002).hasPlayedACard);
    }
    
    // Test the shield stopping the Outmaneuver action card
    @Test
	public void playShieldOutmaneuver() {
    	System.out.println("@Test: playShieldOutmaneuver");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Purple", 3));
    	game.getPlayer(4001).addToHand(new Card("Purple", 3));
    	game.getPlayer(4001).addToHand(new Card("Shield", 0));
    	game.playCard("Purple 3");
    	game.playCard("Purple 3");
    	game.playCard("Shield 0");
    	assertFalse(game.endTurn());
    	
    	//Check that 4001 is shielded
    	assertTrue(game.getPlayer(4001).getShield());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Squire", 3));
    	game.getPlayer(4002).addToHand(new Card("Purple", 4));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	assertFalse(game.endTurn());
    	
    	//Play Outmaneuver 
    	game.getPlayer(4003).getHand().clear();
    	game.getPlayer(4003).addToHand(new Card("Outmaneuver", 0));
    	assertEquals("Good", game.playCard("Outmaneuver 0"));
    	
    	//Check that 4001 is unaffected
    	assertEquals(2, game.getPlayer(4001).getDisplaySize());
    	assertEquals(0, game.getPlayer(4001).getHandSize());
    	assertEquals(6, game.getPlayer(4001).getScore());
    	
    	//Check that 4002 is unaffected
    	assertEquals(1, game.getPlayer(4002).getDisplaySize());
    	assertEquals(1, game.getPlayer(4002).getHandSize());
    	assertEquals(3, game.getPlayer(4002).getScore());
    	
    	//Check that 4003 is unaffected
    	assertEquals(0, game.getPlayer(4003).getDisplaySize());
    	assertEquals(0, game.getPlayer(4003).getHandSize());
    	assertEquals(0, game.getPlayer(4003).getScore());
    	assertTrue(game.getPlayer(4003).hasPlayedACard);
    }
    
    // Test the shield stopping the Charge action card
    @Test
	public void playShieldCharge() {
    	System.out.println("@Test: playShieldCharge");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Purple", 3));
    	game.getPlayer(4001).addToHand(new Card("Purple", 3));
    	game.getPlayer(4001).addToHand(new Card("Shield", 0));
    	game.playCard("Purple 3");
    	game.playCard("Purple 3");
    	game.playCard("Shield 0");
    	assertFalse(game.endTurn());
    	
    	//Check that 4001 is shielded
    	assertTrue(game.getPlayer(4001).getShield());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Squire", 3));
    	game.getPlayer(4002).addToHand(new Card("Purple", 4));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4003).getHand().clear();
    	game.getPlayer(4003).addToHand(new Card("Purple", 5));
    	game.getPlayer(4003).addToHand(new Card("Purple", 4));
    	game.playCard("Purple 5");
    	game.playCard("Purple 4");
    	
    	//Play Charge
    	game.getPlayer(4003).addToHand(new Card("Charge", 0));
    	assertEquals("Good", game.playCard("Charge 0"));
    	
    	//Check that 4001 is unaffected
    	assertEquals(2, game.getPlayer(4001).getDisplaySize());
    	assertEquals(0, game.getPlayer(4001).getHandSize());
    	assertEquals(6, game.getPlayer(4001).getScore());
    	
    	//Check that 4002 is unaffected
    	assertEquals(1, game.getPlayer(4002).getDisplaySize());
    	assertEquals(0, game.getPlayer(4002).getHandSize());
    	assertEquals(4, game.getPlayer(4002).getScore());
    	
    	//Check that 4003 is unaffected
    	assertEquals(2, game.getPlayer(4003).getDisplaySize());
    	assertEquals(0, game.getPlayer(4003).getHandSize());
    	assertEquals(9, game.getPlayer(4003).getScore());
    	assertTrue(game.getPlayer(4003).hasPlayedACard);
    }
    
    
    // Test the shield stopping the Countercharge action card
    @Test
   	public void playShieldCountercharge() {
    	System.out.println("@Test: playShieldCountercharge");
       	game.addPlayer(4001, "Jane");
       	game.addPlayer(4002, "Peter");
       	game.addPlayer(4003, "Pat");
       	game.beginGame();
       	game.setColour("P");
       	
       	game.getPlayer(4001).getHand().clear();
       	game.getPlayer(4001).addToHand(new Card("Squire", 3));
       	game.getPlayer(4001).addToHand(new Card("Purple", 5));
       	game.getPlayer(4001).addToHand(new Card("Shield", 0));
       	game.playCard("Squire 3");
       	game.playCard("Purple 5");
       	game.playCard("Shield 0");
       	assertFalse(game.endTurn());
       	
       	//Check that 4001 is shielded
       	assertTrue(game.getPlayer(4001).getShield());
       	
       	game.getPlayer(4002).getHand().clear();
       	game.getPlayer(4002).addToHand(new Card("Purple", 5));
       	game.getPlayer(4002).addToHand(new Card("Purple", 4));
       	game.playCard("Purple 5");
       	game.playCard("Purple 4");
       	assertFalse(game.endTurn());
       	
       	game.getPlayer(4003).getHand().clear();
       	game.getPlayer(4003).addToHand(new Card("Purple", 5));
       	game.getPlayer(4003).addToHand(new Card("Squire", 2));
       	game.playCard("Purple 5");
       	game.playCard("Squire 2");
       	
       	//Play Countercharge
       	game.getPlayer(4003).addToHand(new Card("Countercharge", 0));
       	assertEquals("Good", game.playCard("Countercharge 0"));
       	
       	//Check that 4001 is unaffected
       	assertEquals(2, game.getPlayer(4001).getDisplaySize());
       	assertEquals(0, game.getPlayer(4001).getHandSize());
       	assertEquals(8, game.getPlayer(4001).getScore());
       	
       	//Check that 4002 is unaffected
       	assertEquals(1, game.getPlayer(4002).getDisplaySize());
       	assertEquals(0, game.getPlayer(4002).getHandSize());
       	assertEquals(4, game.getPlayer(4002).getScore());
       	
       	//Check that 4003 is unaffected
       	assertEquals(1, game.getPlayer(4003).getDisplaySize());
       	assertEquals(0, game.getPlayer(4003).getHandSize());
       	assertEquals(2, game.getPlayer(4003).getScore());
       	assertTrue(game.getPlayer(4003).hasPlayedACard);
    }
    
    // Test the shield stopping the Disgrace action card
    @Test
   	public void playShieldDisgrace() {
    	System.out.println("@Test: playShieldDisgrace");
       	game.addPlayer(4001, "Jane");
       	game.addPlayer(4002, "Peter");
       	game.addPlayer(4003, "Pat");
       	game.beginGame();
       	game.setColour("P");
       	
       	game.getPlayer(4001).getHand().clear();
       	game.getPlayer(4001).addToHand(new Card("Squire", 3));
       	game.getPlayer(4001).addToHand(new Card("Purple", 5));
       	game.getPlayer(4001).addToHand(new Card("Shield", 0));
       	game.playCard("Squire 3");
       	game.playCard("Purple 5");
       	game.playCard("Shield 0");
       	assertFalse(game.endTurn());
       	
       	//Check that 4001 is shielded
       	assertTrue(game.getPlayer(4001).getShield());
       	
       	game.getPlayer(4002).getHand().clear();
       	game.getPlayer(4002).addToHand(new Card("Purple", 5));
       	game.getPlayer(4002).addToHand(new Card("Purple", 4));
       	game.playCard("Purple 5");
       	game.playCard("Purple 4");
       	assertFalse(game.endTurn());
       	
       	game.getPlayer(4003).getHand().clear();
       	game.getPlayer(4003).addToHand(new Card("Purple", 5));
       	game.getPlayer(4003).addToHand(new Card("Squire", 2));
       	game.playCard("Purple 5");
       	game.playCard("Squire 2");
       	
       	//Play Disgrace
       	game.getPlayer(4003).addToHand(new Card("Disgrace", 0));
       	assertEquals("Good", game.playCard("Disgrace 0"));
       	
       	//Check that 4001 is unaffected
       	assertEquals(2, game.getPlayer(4001).getDisplaySize());
       	assertEquals(0, game.getPlayer(4001).getHandSize());
       	assertEquals(8, game.getPlayer(4001).getScore());
       	
       	//Check that 4002 is unaffected
       	assertEquals(2, game.getPlayer(4002).getDisplaySize());
       	assertEquals(0, game.getPlayer(4002).getHandSize());
       	assertEquals(9, game.getPlayer(4002).getScore());
       	
       	//Check that 4003 is unaffected
       	assertEquals(1, game.getPlayer(4003).getDisplaySize());
       	assertEquals(1, game.getPlayer(4003).getHandSize());
       	assertEquals(5, game.getPlayer(4003).getScore());
       	assertTrue(game.getPlayer(4003).hasPlayedACard);
    }
    
 // Test the shield stopping the Adapt action card
    @Test
   	public void playShieldAdapt() {
    	System.out.println("@Test: playShieldAdapt");
       	game.addPlayer(4001, "Jane");
       	game.addPlayer(4002, "Peter");
       	game.addPlayer(4003, "Pat");
       	game.beginGame();
       	game.setColour("P");
       	
       	game.getPlayer(4001).getHand().clear();
       	game.getPlayer(4001).addToHand(new Card("Squire", 3));
       	game.getPlayer(4001).addToHand(new Card("Purple", 3));
       	game.getPlayer(4001).addToHand(new Card("Shield", 0));
       	game.playCard("Squire 3");
       	game.playCard("Purple 3");
       	game.playCard("Shield 0");
       	assertFalse(game.endTurn());
       	
       	//Check that 4001 is shielded
       	assertTrue(game.getPlayer(4001).getShield());
       	
       	game.getPlayer(4002).getHand().clear();
       	game.getPlayer(4002).addToHand(new Card("Purple", 3));
       	game.getPlayer(4002).addToHand(new Card("Squire", 3));
       	game.getPlayer(4002).addToHand(new Card("Purple", 4));
       	game.playCard("Purple 3");
       	game.playCard("Squire 3");
       	game.playCard("Purple 4");
       	assertFalse(game.endTurn());
       	
       	game.getPlayer(4003).getHand().clear();
       	game.getPlayer(4003).addToHand(new Card("Purple", 5));
       	game.getPlayer(4003).addToHand(new Card("Squire", 2));
       	game.playCard("Purple 5");
       	game.playCard("Squire 2");
       	
       	//Play Disgrace
       	game.getPlayer(4003).addToHand(new Card("Adapt", 0));
       	assertEquals("Adapt", game.playCard("Adapt 0"));
       	
       	//Logic handled outside of the game (ie. by the server)
       	
       	//Check that 4001 is protected by the shield
       	ArrayList<Integer> temp = game.findMultipleValueCards(4001);
       	assertTrue(temp.isEmpty());
       	
       	//Check that 4002 is handled correctly
       	temp = game.findMultipleValueCards(4002);
       	assertEquals(1, temp.size());
       	assertTrue(temp.contains(3));
       	
      //Check that 4003 is handled correctly
       	temp = game.findMultipleValueCards(4003);
       	assertTrue(temp.isEmpty());
    }
    
    // Test the shield stopping the Outwit action card
    @Test
	public void playShieldOutwit() {
    	System.out.println("@Test: playShieldOutwit");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.getPlayer(4001).addToHand(new Card("Purple", 4));
    	game.getPlayer(4001).addToHand(new Card("Shield", 0));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	game.playCard("Shield 0");
    	assertFalse(game.endTurn());
    	
    	//Check that 4001 is shielded
    	assertTrue(game.getPlayer(4001).getShield());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Squire", 2));
    	game.playCard("Squire 2");
    	
    	//Play Outwit against 4001
    	game.getPlayer(4002).addToHand(new Card("Outwit", 0));
    	assertEquals("targetDisplayOwnDisplay", game.playCard("Outwit 0"));
    	assertFalse(game.playAgainstTarget("Outwit 0", "4001", "0", "1"));
    	
    	//Check that 4001 is unaffected
    	assertEquals(2, game.getPlayer(4001).getDisplaySize());
    	assertEquals(0, game.getPlayer(4001).getHandSize());
    	assertEquals(7, game.getPlayer(4001).getScore());
    	
    	//Check that 4002 is unaffected
    	assertEquals(1, game.getPlayer(4002).getDisplaySize());
    	assertEquals(1, game.getPlayer(4002).getHandSize());
    	assertEquals(2, game.getPlayer(4002).getScore());
    	assertTrue(game.getPlayer(4002).hasPlayedACard);
    }
    
 // Test the shield stopping the Stunned action card
    @Test
	public void playShieldStunned() {
    	System.out.println("@Test: playShieldStunned");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.getPlayer(4001).addToHand(new Card("Purple", 4));
    	game.getPlayer(4001).addToHand(new Card("Shield", 0));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	game.playCard("Shield 0");
    	assertFalse(game.endTurn());
    	
    	//Check that 4001 is shielded
    	assertTrue(game.getPlayer(4001).getShield());
    	
    	//Play Stunned against 4001
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Stunned", 0));
    	assertEquals("targetHandCard", game.playCard("Stunned 0"));
    	assertTrue(game.playAgainstTarget("Stunned 0", "4001", ""));
    	
    	//Check that 4001 is affected
    	assertTrue(game.getPlayer(4001).getShield());
    	assertTrue(game.getPlayer(4001).getStunned());
    	
    	//Check that 4002 is affected
    	assertTrue(game.getPlayer(4002).hasPlayedACard);
    }
    
    /*
     * I don't see why or how we would need to check that shield stops then Ivanhoe card since the Ivanhoe card
     * is played against card not targets. 
     */
    
}