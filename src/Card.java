package ivanhoe;

public class Card {

	private String name;
	private Integer value;

	public Card(String name, int value) {
		this.name = name;
		this.value = value;

	}

	public String getName() {
		return name;
	}

	public int getValue() {
		return value;
	}
	
	public String toString() {
		return name + " " + value;
	}

}
