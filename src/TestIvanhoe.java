package ivanhoe;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import org.junit.Before;

//make tests for upto 5 players

public class TestIvanhoe {
	Game game;

	/** This will be processed before the Test Class is instantiated */
    @BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass: TestGame");
    }

    /** This is processed/initialized before each test is conducted */
    @Before
    public void setUp() {
		System.out.println("@Before(): TestGame");
		game = new Game(3);
	}
	
    /** This is processed/initialized after each test is conducted */
    @After
    public void tearDown () {
		System.out.println("@After(): TestGame");
		game = null;
	}
	
    /** This will be processed after the Test Class has been destroyed */
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass: TestGame");
    }
    
    /** Each unit test is annotated with the @Test Annotation */
    
    //Using the Ivanhoe card to block the playing of the Unhorse action card
    @Test
	public void playIvanhoeUnhorse() {
    	System.out.println("@Test: playIvanhoeUnhorse");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	//Play to stay in the game and give 4001 the Ivanhoe card
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Purple", 4));
    	game.getPlayer(4001).addToHand(new Card("Purple", 5));
    	game.getPlayer(4001).addToHand(new Card("Ivanhoe", 0));
    	game.playCard("Purple 4");
    	game.playCard("Purple 5");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Unhorse", 0));
    	assertEquals("colour", game.playCard("Unhorse 0"));
    	game.setColour("R");
    	//assertTrue(game.playAgainstTarget("DropWeapon 0", "4001", ""));
    	
    	//Check the colour
    	assertEquals("Red", game.getColour());
    	
    	//Check that break lance has affected 4001
    	assertEquals(2, game.getPlayer(4001).getDisplaySize());
    	assertEquals(1, game.getPlayer(4001).getHandSize());
    	assertEquals(9, game.getPlayer(4001).getScore());
    	
    	//Check that break lance has affected 4002
    	assertEquals(0, game.getPlayer(4002).getDisplaySize());
    	//THIS SHOULD BE FIXED!!!
    	//assertEquals(0, game.getPlayer(4002).getHandSize());
    	assertEquals(0, game.getPlayer(4002).getScore());
    	
    	//4001 decides to play Ivanhoe so the server calls
    	game.updateState("undo", 4001, "Unhorse 0");
    	
    	//Check the colour
    	assertEquals("Purple", game.getColour());
    	
    	//Check the effects have been undone for 4001
    	assertEquals(2, game.getPlayer(4001).getDisplaySize());
    	assertEquals(0, game.getPlayer(4001).getHandSize());
    	assertEquals(9, game.getPlayer(4001).getScore());
    	
    	//Check the effects have been undone for 4002
    	assertEquals(0, game.getPlayer(4002).getDisplaySize());
    	//THIS SHOULD BE FIXED!!!
    	//assertEquals(0, game.getPlayer(4002).getHandSize());
    	assertEquals(0, game.getPlayer(4002).getScore());
    }
    
    //Using the Ivanhoe card to block the playing of the DropWeapon action card
    @Test
	public void playIvanhoeDropWeapon() {
    	System.out.println("@Test: playIvanhoeDropWeapon");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("R");
    	
    	//Play to stay in the game and give 4001 the Ivanhoe card
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 2));
    	game.getPlayer(4001).addToHand(new Card("Red", 4));
    	game.getPlayer(4001).addToHand(new Card("Ivanhoe", 0));
    	game.playCard("Squire 2");
    	game.playCard("Red 4");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("DropWeapon", 0));
    	assertEquals("Good", game.playCard("DropWeapon 0"));
    	//assertTrue(game.playAgainstTarget("DropWeapon 0", "4001", ""));
    	
    	//Check the colour
    	assertEquals("Green", game.getColour());
    	
    	//Check that break lance has affected 4001
    	assertEquals(2, game.getPlayer(4001).getDisplaySize());
    	assertEquals(1, game.getPlayer(4001).getHandSize());
    	assertEquals(2, game.getPlayer(4001).getScore());
    	
    	//Check that break lance has affected 4002
    	assertEquals(0, game.getPlayer(4002).getDisplaySize());
    	assertEquals(0, game.getPlayer(4002).getHandSize());
    	assertEquals(0, game.getPlayer(4002).getScore());
    	
    	//4001 decides to play Ivanhoe so the server calls
    	game.updateState("undo", 4001, "DropWeapon 0");
    	
    	//Check the colour
    	assertEquals("Red", game.getColour());
    	
    	//Check the effects have been undone for 4001
    	assertEquals(2, game.getPlayer(4001).getDisplaySize());
    	assertEquals(0, game.getPlayer(4001).getHandSize());
    	assertEquals(6, game.getPlayer(4001).getScore());
    	
    	//Check the effects have been undone for 4002
    	assertEquals(0, game.getPlayer(4002).getDisplaySize());
    	assertEquals(0, game.getPlayer(4002).getHandSize());
    	assertEquals(0, game.getPlayer(4002).getScore());
    }
    
    //Using the Ivanhoe card to block the playing of the unhorse action card
    @Test
	public void playIvanhoeBreakLance() {
    	System.out.println("@Test: playIvanhoeBreakLance");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	//Play to stay in the game and give 4001 the Ivanhoe card
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.getPlayer(4001).addToHand(new Card("Purple", 4));
    	game.getPlayer(4001).addToHand(new Card("Ivanhoe", 0));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("BreakLance", 0));
    	assertEquals("target", game.playCard("BreakLance 0"));
    	assertTrue(game.playAgainstTarget("BreakLance 0", "4001", ""));
    	
    	//Check that break lance has affected 4001
    	assertEquals(1, game.getPlayer(4001).getDisplaySize());
    	assertEquals(1, game.getPlayer(4001).getHandSize());
    	assertEquals(3, game.getPlayer(4001).getScore());
    	
    	//Check that break lance has affected 4002
    	assertEquals(0, game.getPlayer(4002).getDisplaySize());
    	assertEquals(0, game.getPlayer(4002).getHandSize());
    	assertEquals(0, game.getPlayer(4002).getScore());
    	
    	//4001 decides to play Ivanhoe so the server calls
    	game.updateState("undo", 4001, "BreakLance 0");
    	
    	//Check the effects have been undone for 4001
    	assertEquals(2, game.getPlayer(4001).getDisplaySize());
    	assertEquals(0, game.getPlayer(4001).getHandSize());
    	assertEquals(7, game.getPlayer(4001).getScore());
    	
    	//Check the effects have been undone for 4002
    	assertEquals(0, game.getPlayer(4002).getDisplaySize());
    	assertEquals(0, game.getPlayer(4002).getHandSize());
    	assertEquals(0, game.getPlayer(4002).getScore());
    }
    
    //Using the Ivanhoe card to block the playing of the riposte action card
    @Test
	public void playIvanhoeRiposte() {
    	System.out.println("@Test: playIvanhoeRiposte");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	//Play to stay in the game and give 4001 the Ivanhoe card
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.getPlayer(4001).addToHand(new Card("Purple", 4));
    	game.getPlayer(4001).addToHand(new Card("Ivanhoe", 0));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Riposte", 0));
    	assertEquals("target", game.playCard("Riposte 0"));
    	assertTrue(game.playAgainstTarget("Riposte 0", "4001", ""));
    	
    	//Check that break lance has affected 4001
    	assertEquals(1, game.getPlayer(4001).getDisplaySize());
    	assertEquals(1, game.getPlayer(4001).getHandSize());
    	assertEquals(3, game.getPlayer(4001).getScore());
    	
    	//Check that break lance has affected 4002
    	assertEquals(1, game.getPlayer(4002).getDisplaySize());
    	assertEquals(0, game.getPlayer(4002).getHandSize());
    	assertEquals(4, game.getPlayer(4002).getScore());
    	
    	//4001 decides to play Ivanhoe so the server calls
    	game.updateState("undo", 4001, "Riposte 0");
    	
    	//Check the effects have been undone for 4001
    	assertEquals(2, game.getPlayer(4001).getDisplaySize());
    	assertEquals(0, game.getPlayer(4001).getHandSize());
    	assertEquals(7, game.getPlayer(4001).getScore());
    	
    	//Check the effects have been undone for 4002
    	assertEquals(0, game.getPlayer(4002).getDisplaySize());
    	assertEquals(0, game.getPlayer(4002).getHandSize());
    	assertEquals(0, game.getPlayer(4002).getScore());
    }
    
    //Using the Ivanhoe card to block the playing of the dodge action card
    @Test
	public void playIvanhoeDodge() {
    	System.out.println("@Test: playIvanhoeDodge");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	//Play to stay in the game and give 4001 the Ivanhoe card
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.getPlayer(4001).addToHand(new Card("Purple", 4));
    	game.getPlayer(4001).addToHand(new Card("Ivanhoe", 0));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Dodge", 0));
    	assertEquals("targetDisplayCard", game.playCard("Dodge 0"));
    	assertTrue(game.playAgainstTarget("Dodge 0", "4001", "0"));
    	
    	//Check that dodge has affected 4001
    	assertEquals(1, game.getPlayer(4001).getDisplaySize());
    	assertEquals(1, game.getPlayer(4001).getHandSize());
    	assertEquals(4, game.getPlayer(4001).getScore());
    	
    	//Check that dodge has affected 4002
    	assertEquals(0, game.getPlayer(4002).getDisplaySize());
    	assertEquals(0, game.getPlayer(4002).getHandSize());
    	assertEquals(0, game.getPlayer(4002).getScore());
    	
    	//4001 decides to play Ivanhoe so the server calls
    	game.updateState("undo", 4001, "Dodge 0");
    	
    	//Check the effects have been undone for 4001
    	assertEquals(2, game.getPlayer(4001).getDisplaySize());
    	assertEquals(0, game.getPlayer(4001).getHandSize());
    	assertEquals(7, game.getPlayer(4001).getScore());
    	
    	//Check the effects have been undone for 4002
    	assertEquals(0, game.getPlayer(4002).getDisplaySize());
    	assertEquals(0, game.getPlayer(4002).getHandSize());
    	assertEquals(0, game.getPlayer(4002).getScore());
    }
    
    //Using the Ivanhoe card to block the playing of the retreat action card
    @Test
	public void playIvanhoeRetreat() {
    	System.out.println("@Test: playIvanhoeRetreat");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Ivanhoe", 0));
    	
    	//Play to stay in the game and give 4001 the Ivanhoe card
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.getPlayer(4001).addToHand(new Card("Purple", 4));
    	game.getPlayer(4001).addToHand(new Card("Retreat", 0));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	
    	assertEquals("ownDisplayCard", game.playCard("Retreat 0"));
    	assertTrue(game.playAgainstTarget("Retreat 0", "4001", "0"));
    	//assertFalse(game.endTurn());
    	
    	//Check that retreat has affected 4001
    	assertEquals(1, game.getPlayer(4001).getDisplaySize());
    	assertEquals(1, game.getPlayer(4001).getHandSize());
    	assertEquals(4, game.getPlayer(4001).getScore());
    	
    	//Check that retreat has affected 4002
    	assertEquals(0, game.getPlayer(4002).getDisplaySize());
    	assertEquals(1, game.getPlayer(4002).getHandSize());
    	assertEquals(0, game.getPlayer(4002).getScore());
    	
    	//4002 decides to play Ivanhoe so the server calls
    	game.updateState("undo", 4002, "Retreat 0");
    	
    	//Check the effects have been undone for 4001
    	assertEquals(2, game.getPlayer(4001).getDisplaySize());
    	assertEquals(0, game.getPlayer(4001).getHandSize());
    	assertEquals(7, game.getPlayer(4001).getScore());
    	
    	//Check the effects have been undone for 4002
    	assertEquals(0, game.getPlayer(4002).getDisplaySize());
    	assertEquals(0, game.getPlayer(4002).getHandSize());
    	assertEquals(0, game.getPlayer(4002).getScore());
    }
    
    //Using the Ivanhoe card to block the playing of the dodge action card
    @Test
	public void playIvanhoeKnockDown() {
    	System.out.println("@Test: playIvanhoeKnockDown");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	//Play to stay in the game and give 4001 the Ivanhoe card
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.getPlayer(4001).addToHand(new Card("Purple", 4));
    	game.getPlayer(4001).addToHand(new Card("Purple", 5));
    	game.getPlayer(4001).addToHand(new Card("Ivanhoe", 0));
    	game.playCard("Purple 5");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("KnockDown", 0));
    	assertEquals("targetHandCard", game.playCard("KnockDown 0"));
    	assertTrue(game.playAgainstTarget("KnockDown 0", "4001", "1"));
    	
    	//Check that KnockDown has affected 4001
    	assertEquals(1, game.getPlayer(4001).getDisplaySize());
    	assertEquals(2, game.getPlayer(4001).getHandSize());
    	assertEquals(5, game.getPlayer(4001).getScore());
    	
    	//Check that KnockDown has affected 4002
    	assertEquals(0, game.getPlayer(4002).getDisplaySize());
    	assertEquals(1, game.getPlayer(4002).getHandSize());
    	assertEquals(0, game.getPlayer(4002).getScore());
    	
    	//4001 decides to play Ivanhoe so the server calls
    	game.updateState("undo", 4001, "KnockDown 0");
    	
    	//Check the effects have been undone for 4001
    	assertEquals(1, game.getPlayer(4001).getDisplaySize());
    	assertEquals(2, game.getPlayer(4001).getHandSize());
    	assertEquals(5, game.getPlayer(4001).getScore());
    	
    	//Check the effects have been undone for 4002
    	assertEquals(0, game.getPlayer(4002).getDisplaySize());
    	assertEquals(0, game.getPlayer(4002).getHandSize());
    	assertEquals(0, game.getPlayer(4002).getScore());
    }
    
    //Using the Ivanhoe card to block the playing of the outmaneuver action card
    @Test
	public void playIvanhoeOutmaneuver() {
    	System.out.println("@Test: playIvanhoeOutmaneuver");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Purple", 3));
    	game.getPlayer(4001).addToHand(new Card("Purple", 3));
    	game.getPlayer(4001).addToHand(new Card("Ivanhoe", 0));
    	game.playCard("Purple 3");
    	game.playCard("Purple 3");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Squire", 3));
    	game.getPlayer(4002).addToHand(new Card("Purple", 4));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4003).getHand().clear();
    	game.getPlayer(4003).addToHand(new Card("Outmaneuver", 0));
    	assertTrue(game.playCard("Outmaneuver 0").equalsIgnoreCase("Good"));
    	
    	//Check that KnockDown has affected 4001
    	assertEquals(1, game.getPlayer(4001).getDisplaySize());
    	assertEquals(2, game.getPlayer(4001).getHandSize());
    	assertEquals(3, game.getPlayer(4001).getScore());
    	
    	//Check that KnockDown has affected 4002
    	assertEquals(1, game.getPlayer(4002).getDisplaySize());
    	assertEquals(1, game.getPlayer(4002).getHandSize());
    	assertEquals(3, game.getPlayer(4002).getScore());
    	
    	//Check that KnockDown has affected 4003
    	assertEquals(0, game.getPlayer(4003).getDisplaySize());
    	assertEquals(0, game.getPlayer(4003).getHandSize());
    	assertEquals(0, game.getPlayer(4003).getScore());
    	
    	//4001 decides to play Ivanhoe so the server calls
    	game.updateState("undo", 4001, "Outmaneuver 0");
    	
    	//Check the effects have been undone for 4001
    	assertEquals(2, game.getPlayer(4001).getDisplaySize());
    	assertEquals(0, game.getPlayer(4001).getHandSize());
    	assertEquals(6, game.getPlayer(4001).getScore());
    	
    	//Check the effects have been undone for 4002
    	assertEquals(2, game.getPlayer(4002).getDisplaySize());
    	assertEquals(0, game.getPlayer(4002).getHandSize());
    	assertEquals(7, game.getPlayer(4002).getScore());
    	
    	//Check that KnockDown has affected 4003
    	assertEquals(0, game.getPlayer(4003).getDisplaySize());
    	assertEquals(0, game.getPlayer(4003).getHandSize());
    	assertEquals(0, game.getPlayer(4003).getScore());
    }
    
    //Using the Ivanhoe card to block the playing of the charge action card
    @Test
	public void playIvanhoeCharge() {
    	System.out.println("@Test: playIvanhoeCharge");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Purple", 3));
    	game.getPlayer(4001).addToHand(new Card("Purple", 3));
    	game.getPlayer(4001).addToHand(new Card("Ivanhoe", 0));
    	game.playCard("Purple 3");
    	game.playCard("Purple 3");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Squire", 3));
    	game.getPlayer(4002).addToHand(new Card("Purple", 4));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4003).getHand().clear();
    	game.getPlayer(4003).addToHand(new Card("Maiden", 6));
    	game.getPlayer(4003).addToHand(new Card("Purple", 4));
    	game.playCard("Maiden 6");
    	game.playCard("Purple 4");
    	game.getPlayer(4003).addToHand(new Card("Charge", 0));
    	assertEquals("Good", game.playCard("Charge 0"));
    	
    	//Check that KnockDown has affected 4001
    	assertEquals(1, game.getPlayer(4001).getDisplaySize());
    	assertEquals(1, game.getPlayer(4001).getHandSize());
    	assertEquals(3, game.getPlayer(4001).getScore());
    	
    	//Check that KnockDown has affected 4002
    	assertEquals(1, game.getPlayer(4002).getDisplaySize());
    	assertEquals(0, game.getPlayer(4002).getHandSize());
    	assertEquals(4, game.getPlayer(4002).getScore());
    	
    	//Check that KnockDown has affected 4003
    	assertEquals(2, game.getPlayer(4003).getDisplaySize());
    	assertEquals(0, game.getPlayer(4003).getHandSize());
    	assertEquals(10, game.getPlayer(4003).getScore());
    	
    	//4001 decides to play Ivanhoe so the server calls
    	game.updateState("undo", 4001, "Charge 0");
    	
    	//Check the effects have been undone for 4001
    	assertEquals(2, game.getPlayer(4001).getDisplaySize());
    	assertEquals(0, game.getPlayer(4001).getHandSize());
    	assertEquals(6, game.getPlayer(4001).getScore());
    	
    	//Check the effects have been undone for 4002
    	assertEquals(2, game.getPlayer(4002).getDisplaySize());
    	assertEquals(0, game.getPlayer(4002).getHandSize());
    	assertEquals(7, game.getPlayer(4002).getScore());
    	
    	//Check that KnockDown has affected 4003
    	assertEquals(2, game.getPlayer(4003).getDisplaySize());
    	assertEquals(0, game.getPlayer(4003).getHandSize());
    	assertEquals(10, game.getPlayer(4003).getScore());
    }
    
    //Using the Ivanhoe card to block the playing of the Countercharge action card
    @Test
	public void playIvanhoeCountercharge() {
    	System.out.println("@Test: playIvanhoeCountercharge");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Purple", 3));
    	game.getPlayer(4001).addToHand(new Card("Purple", 3));
    	game.getPlayer(4001).addToHand(new Card("Ivanhoe", 0));
    	game.playCard("Purple 3");
    	game.playCard("Purple 3");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Squire", 3));
    	game.getPlayer(4002).addToHand(new Card("Purple", 4));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4003).getHand().clear();
    	game.getPlayer(4003).addToHand(new Card("Squire", 2));
    	game.getPlayer(4003).addToHand(new Card("Purple", 4));
    	game.playCard("Squire 2");
    	game.playCard("Purple 4");
    	game.getPlayer(4003).addToHand(new Card("Countercharge", 0));
    	assertEquals("Good", game.playCard("Countercharge 0"));
    	
    	//Check that Countercharge has affected 4001
    	assertEquals(2, game.getPlayer(4001).getDisplaySize());
    	assertEquals(1, game.getPlayer(4001).getHandSize());
    	assertEquals(6, game.getPlayer(4001).getScore());
    	
    	//Check that Countercharge has affected 4002
    	assertEquals(1, game.getPlayer(4002).getDisplaySize());
    	assertEquals(0, game.getPlayer(4002).getHandSize());
    	assertEquals(3, game.getPlayer(4002).getScore());
    	
    	//Check that Countercharge has affected 4003
    	assertEquals(1, game.getPlayer(4003).getDisplaySize());
    	assertEquals(0, game.getPlayer(4003).getHandSize());
    	assertEquals(2, game.getPlayer(4003).getScore());
    	
    	//4001 decides to play Ivanhoe so the server calls
    	game.updateState("undo", 4001, "Countercharge 0");
    	
    	//Check the effects have been undone for 4001
    	assertEquals(2, game.getPlayer(4001).getDisplaySize());
    	assertEquals(0, game.getPlayer(4001).getHandSize());
    	assertEquals(6, game.getPlayer(4001).getScore());
    	
    	//Check the effects have been undone for 4002
    	assertEquals(2, game.getPlayer(4002).getDisplaySize());
    	assertEquals(0, game.getPlayer(4002).getHandSize());
    	assertEquals(7, game.getPlayer(4002).getScore());
    	
    	//Check that KnockDown has affected 4003
    	assertEquals(2, game.getPlayer(4003).getDisplaySize());
    	assertEquals(0, game.getPlayer(4003).getHandSize());
    	assertEquals(6, game.getPlayer(4003).getScore());
    }
    
    //Using the Ivanhoe card to block the playing of the Disgrace action card
    @Test
	public void playIvanhoeDisgrace() {
    	System.out.println("@Test: playIvanhoeDisgrace");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Purple", 3));
    	game.getPlayer(4001).addToHand(new Card("Squire", 2));
    	game.getPlayer(4001).addToHand(new Card("Ivanhoe", 0));
    	game.playCard("Purple 3");
    	game.playCard("Squire 2");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Squire", 3));
    	game.getPlayer(4002).addToHand(new Card("Purple", 4));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4003).getHand().clear();
    	game.getPlayer(4003).addToHand(new Card("Purple", 5));
    	game.getPlayer(4003).addToHand(new Card("Purple", 4));
    	game.playCard("Purple 5");
    	game.playCard("Purple 4");
    	game.getPlayer(4003).addToHand(new Card("Disgrace", 0));
    	assertEquals("Good", game.playCard("Disgrace 0"));
    	
    	//Check that Countercharge has affected 4001
    	assertEquals(1, game.getPlayer(4001).getDisplaySize());
    	assertEquals(2, game.getPlayer(4001).getHandSize());
    	assertEquals(3, game.getPlayer(4001).getScore());
    	
    	//Check that Countercharge has affected 4002
    	assertEquals(1, game.getPlayer(4002).getDisplaySize());
    	assertEquals(1, game.getPlayer(4002).getHandSize());
    	assertEquals(4, game.getPlayer(4002).getScore());
    	
    	//Check that Countercharge has affected 4003
    	assertEquals(2, game.getPlayer(4003).getDisplaySize());
    	assertEquals(0, game.getPlayer(4003).getHandSize());
    	assertEquals(9, game.getPlayer(4003).getScore());
    	
    	//4001 decides to play Ivanhoe so the server calls
    	game.updateState("undo", 4001, "Disgrace 0");
    	
    	//Check the effects have been undone for 4001
    	assertEquals(2, game.getPlayer(4001).getDisplaySize());
    	assertEquals(0, game.getPlayer(4001).getHandSize());
    	assertEquals(5, game.getPlayer(4001).getScore());
    	
    	//Check the effects have been undone for 4002
    	assertEquals(2, game.getPlayer(4002).getDisplaySize());
    	assertEquals(0, game.getPlayer(4002).getHandSize());
    	assertEquals(7, game.getPlayer(4002).getScore());
    	
    	//Check that KnockDown has affected 4003
    	assertEquals(2, game.getPlayer(4003).getDisplaySize());
    	assertEquals(0, game.getPlayer(4003).getHandSize());
    	assertEquals(9, game.getPlayer(4003).getScore());
    }
    
    //Using the Ivanhoe card to block the playing of the Outwit action card
    @Test
	public void playIvanhoeOutwit() {
    	System.out.println("@Test: playIvanhoeOutwit");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Maiden", 6));
    	game.getPlayer(4001).addToHand(new Card("Ivanhoe", 0));
    	game.playCard("Maiden 6");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Squire", 3));
    	game.getPlayer(4002).addToHand(new Card("Purple", 4));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4003).getHand().clear();
    	game.getPlayer(4003).addToHand(new Card("Purple", 4));
    	game.playCard("Purple 4");
    	game.getPlayer(4003).addToHand(new Card("Outwit", 0));
    	assertEquals("targetDisplayOwnDisplay", game.playCard("Outwit 0"));
    	assertTrue(game.playAgainstTarget("Outwit 0", "4001", "0", "0"));
    	
    	//Check that Countercharge has affected 4001
    	assertEquals(1, game.getPlayer(4001).getDisplaySize());
    	assertEquals(1, game.getPlayer(4001).getHandSize());
    	assertEquals(4, game.getPlayer(4001).getScore());
    	
    	//Check that Countercharge has affected 4002
    	assertEquals(2, game.getPlayer(4002).getDisplaySize());
    	assertEquals(0, game.getPlayer(4002).getHandSize());
    	assertEquals(7, game.getPlayer(4002).getScore());
    	
    	//Check that Countercharge has affected 4003
    	assertEquals(1, game.getPlayer(4003).getDisplaySize());
    	assertEquals(0, game.getPlayer(4003).getHandSize());
    	assertEquals(6, game.getPlayer(4003).getScore());
    	
    	//4001 decides to play Ivanhoe so the server calls
    	game.updateState("undo", 4001, "Outwit 0");
    	
    	//Check the effects have been undone for 4001
    	assertEquals(1, game.getPlayer(4001).getDisplaySize());
    	assertEquals(0, game.getPlayer(4001).getHandSize());
    	assertEquals(6, game.getPlayer(4001).getScore());
    	
    	//Check the effects have been undone for 4002
    	assertEquals(2, game.getPlayer(4002).getDisplaySize());
    	assertEquals(0, game.getPlayer(4002).getHandSize());
    	assertEquals(7, game.getPlayer(4002).getScore());
    	
    	//Check that KnockDown has affected 4003
    	assertEquals(1, game.getPlayer(4003).getDisplaySize());
    	assertEquals(0, game.getPlayer(4003).getHandSize());
    	assertEquals(4, game.getPlayer(4003).getScore());
    }
    
    //Using the Ivanhoe card to block the playing of the Shield action card
    @Test
	public void playIvanhoeShield() {
    	System.out.println("@Test: playIvanhoeShield");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Maiden", 6));
    	game.getPlayer(4001).addToHand(new Card("Ivanhoe", 0));
    	game.playCard("Maiden 6");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Shield", 0));
    	assertEquals("Good", game.playCard("Shield 0"));
    	
    	//Check that Shield has affected 4002
    	assertTrue(game.getPlayer(4002).getShield());
    	
    	//4001 decides to play Ivanhoe so the server calls
    	game.updateState("undo", 4001, "Shield 0");
    	
    	//Check the effects have been undone for 4001
    	assertEquals(1, game.getPlayer(4001).getDisplaySize());
    	assertEquals(0, game.getPlayer(4001).getHandSize());
    	assertEquals(6, game.getPlayer(4001).getScore());
    	
    	//Check the effects have been undone for 4002
    	assertEquals(0, game.getPlayer(4002).getDisplaySize());
    	assertEquals(0, game.getPlayer(4002).getHandSize());
    	assertEquals(0, game.getPlayer(4002).getScore());
    	assertFalse(game.getPlayer(4002).getShield());
    }
    
  //Using the Ivanhoe card to block the playing of the Stunned action card
    @Test
	public void playIvanhoeStunned() {
    	System.out.println("@Test: playIvanhoeStunned");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Maiden", 6));
    	game.getPlayer(4001).addToHand(new Card("Ivanhoe", 0));
    	game.playCard("Maiden 6");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Stunned", 0));
    	assertEquals("target", game.playCard("Stunned 0"));
    	assertTrue(game.playAgainstTarget("Stunned 0", "4001", ""));
    	
    	//Check that Shield has affected 4002
    	assertTrue(game.getPlayer(4001).getStunned());
    	
    	//4001 decides to play Ivanhoe so the server calls
    	game.updateState("undo", 4001, "Stunned 0");
    	
    	//Check the effects have been undone for 4001
    	assertEquals(1, game.getPlayer(4001).getDisplaySize());
    	assertEquals(0, game.getPlayer(4001).getHandSize());
    	assertEquals(6, game.getPlayer(4001).getScore());
    	assertFalse(game.getPlayer(4001).getStunned());
    	
    	//Check the effects have been undone for 4002
    	assertEquals(0, game.getPlayer(4002).getDisplaySize());
    	assertEquals(0, game.getPlayer(4002).getHandSize());
    	assertEquals(0, game.getPlayer(4002).getScore());
    }
   
}