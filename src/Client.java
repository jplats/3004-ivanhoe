package ivanhoe;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client implements Runnable {

	private int ID;
	private Thread thread;
	private Thread clientThread;
	private Socket socket;
	private BufferedReader userInput;
	private BufferedReader streamIn;
	private BufferedWriter streamOut;
	private String lastMessage;
	private boolean turn;
	private GUI gui;
	
	Client(){
		ID = 0;
		socket = null;
		thread = null;
		turn = false;
		gui = null;
	}
	Client(GUI g){
		ID = 0;
		socket = null;
		thread = null;
		turn = false;
		gui = g;
	}
	
	//attempt to connect to a server
	
	public boolean connect(String Ip, int portNum){
		try{
			this.socket = new Socket(Ip, portNum);
	 		this.ID = socket.getLocalPort();
	 		this.start();
		}catch(UnknownHostException uhe){
			return false;
		}catch(IOException ioe){
			return false;	
		}
		return true;
	}
	
	//once socket connection is achieved, initialize input/output buffers and start thread
	
	public void start(){
		try{
			userInput = new BufferedReader(new InputStreamReader(System.in));
			streamIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			streamOut = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			if(thread == null){
				clientThread = new ClientThread(this, socket);
				thread = new Thread(this);
				thread.start();
			}
		
		}catch(IOException ioe){
			
		}
	}

	public void handle(String message){
		System.out.println("1 " + message);
		if(message != null){
			lastMessage = message;
		}
		if(gui != null)gui.update();
	}
	
	public void send(String message){
		try{
			if (streamOut!= null){
				streamOut.write(message);
				streamOut.newLine();
				streamOut.flush();
			}
		}catch(IOException ioe){
			
		}
	}

	public void run(){
		while(thread != null){
			
			try{
				
				if (streamOut != null){
					streamOut.write(userInput.readLine());
					streamOut.newLine();
					streamOut.flush();
					
				}
				
			}catch(IOException e){
				
			}
			
		}
	}
	
	public String getLastMessage(){
		return lastMessage;
	}
	public int getID(){
		return ID;
	}
}
