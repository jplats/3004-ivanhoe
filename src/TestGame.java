package ivanhoe;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import org.junit.Before;

//make tests for upto 5 players

public class TestGame {
	Game game;

	/** This will be processed before the Test Class is instantiated */
    @BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass: TestGame");
    }

    /** This is processed/initialized before each test is conducted */
    @Before
    public void setUp() {
		System.out.println("@Before(): TestGame");
		game = new Game(2);
	}
	
    /** This is processed/initialized after each test is conducted */
    @After
    public void tearDown () {
		System.out.println("@After(): TestGame");
		game = null;
	}
	
    /** This will be processed after the Test Class has been destroyed */
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass: TestGame");
    }
    
    /** Each unit test is annotated with the @Test Annotation */
    
    @Test
	public void AddPlayer() {
    	System.out.println("@Test: AddPlayer");
    	game.addPlayer(4001, "Jane");
    	assertNotNull(game.getPlayer(4001));
	}
    
    @Test
	public void DrawCard() {
    	System.out.println("@Test: DrawCard");
    	game.addPlayer(4001, "Jane");
    	game.beginGame();
    	game.setColour("G");
    	Card c = game.drawCard();
    	assertNotNull(c);
	}
    
    @Test
	public void playCard() {
    	System.out.println("@Test: playCard");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("G");
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Green", 1));
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.getPlayer(4001).addToHand(new Card("Maiden", 6));
    	game.getPlayer(4001).addToHand(new Card("Red", 3));
    	assertTrue(game.playCard("Green 1").equalsIgnoreCase("Good"));
    	assertTrue(game.playCard("Squire 3").equalsIgnoreCase("Good"));
    	assertTrue(game.playCard("Maiden 6").equalsIgnoreCase("Good"));
    	assertTrue(game.playCard("Red 3").equalsIgnoreCase("Bad"));
    	
    	assertEquals(3, game.getPlayer(4001).getScore());
	}
    
    //test playing unhorse with the colour purple
    @Test
	public void playUnhorse1() {
    	System.out.println("@Test: playUnhorse1");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("P");
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Unhorse", 0));
    	assertTrue(game.playCard("Unhorse 0").equalsIgnoreCase("Colour"));
    	//can only choose red, blue, or yellow
    	game.setColour("R");
	}
    
  //test playing unhorse with the colour not purple
    @Test
	public void playUnhorse2() {
    	System.out.println("@Test: playUnhorse2");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("G");
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Unhorse", 0));
    	assertTrue(game.playCard("Unhorse 0").equalsIgnoreCase("Bad"));
    	//can only choose red, blue, or yellow
    	game.setColour("R");
	}
    
  //test change weapon with the colour red (same for blue, yellow).
    @Test
	public void playChangeWeapon1() {
    	System.out.println("@Test: playChangeWeapon1");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("R");
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("ChangeWeapon", 0));
    	assertTrue(game.playCard("ChangeWeapon 0").equalsIgnoreCase("Colour"));
    	//can only choose red, blue, or yellow
    	game.setColour("B");
	}

  //test change weapon with the colour green (same for purple).
    @Test
	public void playChangeWeapon2() {
    	System.out.println("@Test: playChangeWeapon2");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("G");
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("ChangeWeapon", 0));
    	assertTrue(game.playCard("ChangeWeapon 0").equalsIgnoreCase("Bad"));    	
    	//can only choose red, blue, or yellow
    	game.setColour("R");
	}

    
  //test change weapon with the colour red (same for blue, yellow).
    @Test
	public void playDropWeapon1() {
    	System.out.println("@Test: playChangeWeapon1");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("R");
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("DropWeapon", 0));
    	assertTrue(game.playCard("DropWeapon 0").equalsIgnoreCase("Good"));
    	//sets the game colour to green, getColour() doesn't exist, should we make it??
    	//assertTrue(game.getColour().equalsIgnoreCase("Green"));
	}
    
  //test change weapon with the colour purple (same for green).
    @Test
	public void playDropWeapon2() {
    	System.out.println("@Test: playChangeWeapon2");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("P");
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("DropWeapon", 0));
    	assertTrue(game.playCard("DropWeapon 0").equalsIgnoreCase("Bad"));
    	//sets the game colour to green, getColour() doesn't exist, should we make it??
    	//assertTrue(game.getColour().equalsIgnoreCase("Green"));
	}
    
    
    
    
    
  //playing the break lance card when the target has only one purple card in their display
    @Test
	public void playBreakLance1() {
    	System.out.println("@Test: playBreakLance1");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Purple", 3));
    	game.getPlayer(4001).addToHand(new Card("Purple", 4));
    	game.playCard("Purple 4");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("BreakLance", 0));
    	assertTrue(game.playCard("BreakLance 0").equalsIgnoreCase("Target"));
    	assertFalse(game.playAgainstTarget("BreakLance 0", "4001", ""));
    	assertTrue(game.getPlayer(4001).getScore() == 4);
    	assertFalse(game.getPlayer(4002).hasPlayedACard);
    	assertEquals(0, game.getPlayer(4002).numPlayedDisplay);
    	
    }
    
  //playing the break lance card when the target has only purple cards in their display
    //the first card played should be left
    @Test
	public void playBreakLance2() {
    	System.out.println("@Test: playBreakLance2");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Purple", 3));
    	game.getPlayer(4001).addToHand(new Card("Purple", 4));
    	game.playCard("Purple 4");
    	game.playCard("Purple 3");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("BreakLance", 0));
    	assertTrue(game.playCard("BreakLance 0").equalsIgnoreCase("Target"));
    	assertTrue(game.playAgainstTarget("BreakLance 0", "4001", ""));
    	assertTrue(game.getPlayer(4001).getScore() == 4);
    	assertTrue(game.getPlayer(4002).hasPlayedACard);    	
    }
    
    //playing the break lance card when the target has purple & other cards in their display
    @Test
	public void playBreakLance3() {
    	System.out.println("@Test: playBreakLance3");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Purple", 3));
    	game.getPlayer(4001).addToHand(new Card("Purple", 4));
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.playCard("Purple 4");
    	game.playCard("Squire 3");
    	game.playCard("Purple 3");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("BreakLance", 0));
    	assertTrue(game.playCard("BreakLance 0").equalsIgnoreCase("Target"));
    	assertTrue(game.playAgainstTarget("BreakLance 0", "4001", ""));
    	assertTrue(game.getPlayer(4001).getScore() == 3);
    	assertTrue(game.getPlayer(4002).hasPlayedACard);    	
    }
    
  //playing the break lance card when the target has only non-purple cards in their display
    @Test
	public void playBreakLance4() {
    	System.out.println("@Test: playBreakLance4");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.playCard("Squire 3");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("BreakLance", 0));
    	assertTrue(game.playCard("BreakLance 0").equalsIgnoreCase("Target"));
    	assertFalse(game.playAgainstTarget("BreakLance 0", "4001", ""));
    	assertTrue(game.getPlayer(4001).getScore() == 3);
    	assertFalse(game.getPlayer(4002).hasPlayedACard);
    	assertEquals(0, game.getPlayer(4002).numPlayedDisplay);
    	
    }
    
  //playing the repost card when the target has no cards in their display
    @Test
	public void playRiposte1() {
    	System.out.println("@Test: playRiposte1");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Riposte", 0));
    	assertTrue(game.playCard("Riposte 0").equalsIgnoreCase("Target"));
    	assertFalse(game.playAgainstTarget("Riposte 0", "4002", ""));
    	assertTrue(game.getPlayer(4001).getScore() == 0);
    	assertFalse(game.getPlayer(4001).hasPlayedACard);
    	assertEquals(0, game.getPlayer(4001).numPlayedDisplay);
    	
    }
    
    //playing the repost card when the target has 1 card in their display (can not be played)
    @Test
	public void playRiposte2() {
    	System.out.println("@Test: playRiposte2");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.playCard("Squire 3");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Riposte", 0));
    	assertTrue(game.playCard("Riposte 0").equalsIgnoreCase("Target"));
    	assertFalse(game.playAgainstTarget("Riposte 0", "4001", ""));
    	assertTrue(game.getPlayer(4001).getScore() == 3);
    	assertTrue(game.getPlayer(4002).getScore() == 0);
    	assertFalse(game.getPlayer(4002).hasPlayedACard);
    	assertEquals(0, game.getPlayer(4002).numPlayedDisplay);
    	
    }
    
    //playing the repost card when the target has multiple cards in their display (can be played)
    @Test
	public void playRiposte3() {
    	System.out.println("@Test: playRiposte3");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.getPlayer(4001).addToHand(new Card("Purple", 4));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Riposte", 0));
    	assertTrue(game.playCard("Riposte 0").equalsIgnoreCase("Target"));
    	assertTrue(game.playAgainstTarget("Riposte 0", "4001", ""));
    	assertTrue(game.getPlayer(4001).getScore() == 3);
    	assertTrue(game.getPlayer(4002).getScore() == 4);
    	assertTrue(game.getPlayer(4002).hasPlayedACard);
    	assertEquals(1, game.getPlayer(4002).numPlayedDisplay);
    	
    }
    
  //playing the dodge card when the target has no cards in their display (can not be played)
    @Test
	public void playDodge1() {
    	System.out.println("@Test: playDodge1");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Dodge", 0));
    	assertTrue(game.playCard("Dodge 0").equalsIgnoreCase("targetDisplayCard"));
    	assertFalse(game.playAgainstTarget("Dodge 0", "4002", "0"));
       	assertTrue(game.getPlayer(4001).getScore() == 0);
    	assertFalse(game.getPlayer(4001).hasPlayedACard);
    	assertEquals(0, game.getPlayer(4001).numPlayedDisplay);
    	
    }
    
  //playing the dodge card when the target has one card in their display (can not be played)
    @Test
	public void playDodge2() {
    	System.out.println("@Test: playDodge2");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.playCard("Squire 3");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Dodge", 0));
    	assertTrue(game.playCard("Dodge 0").equalsIgnoreCase("targetDisplayCard"));
    	assertFalse(game.playAgainstTarget("Dodge 0", "4001", "0"));
    	assertTrue(game.getPlayer(4002).getScore() == 0);
    	assertFalse(game.getPlayer(4002).hasPlayedACard);
    	assertEquals(0, game.getPlayer(4002).numPlayedDisplay);
    }
    
  //playing the dodge card when the target has multiple cards in their display (can be played)
    @Test
	public void playDodge3() {
    	System.out.println("@Test: playDodge3");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.getPlayer(4001).addToHand(new Card("Purple", 4));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Dodge", 0));
    	assertTrue(game.playCard("Dodge 0").equalsIgnoreCase("targetDisplayCard"));
    	assertTrue(game.playAgainstTarget("Dodge 0", "4001", "0"));
    	assertTrue(game.getPlayer(4001).getScore() == 4);
    	assertTrue(game.getPlayer(4002).hasPlayedACard);
    }
    
    //COULD HAVE ANOTHER TEST FOR USING DODGE TO REMOVE A MIDDLE CARD
    
  //playing the retreat card when the current player has no cards in their display (can not be played)
    @Test
	public void playRetreat1() {
    	System.out.println("@Test: playRetreat1");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Retreat", 0));
    	assertTrue(game.playCard("Retreat 0").equalsIgnoreCase("ownDisplayCard"));
    	assertFalse(game.playAgainstTarget("Retreat 0", "4001", "0"));
    	//assertFalse(game.playAgainstTarget("Retreat 0", "", "0"));
    	assertTrue(game.getPlayer(4001).getScore() == 0);
    }
    
  //playing the retreat card when the current player has one card in their display (can not be played)
    @Test
	public void playRetreat2() {
    	System.out.println("@Test: playRetreat2");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
       	game.playCard("Squire 3");
    	
    	game.getPlayer(4001).addToHand(new Card("Retreat", 0));
    	assertTrue(game.playCard("Retreat 0").equalsIgnoreCase("ownDisplayCard"));
    	assertFalse(game.playAgainstTarget("Retreat 0", "4001", "0"));
    	assertTrue(game.getPlayer(4001).getScore() == 3);
    }
    
  //playing the retreat card when the current player has multiple cards in their display (can be played)
    @Test
	public void playRetreat3() {
    	System.out.println("@Test: playRetreat3");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	Card c = new Card("Squire", 3);
    	game.getPlayer(4001).addToHand(c);
    	game.getPlayer(4001).addToHand(new Card("Purple", 4));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	
    	game.getPlayer(4001).addToHand(new Card("Retreat", 0));
    	assertTrue(game.playCard("Retreat 0").equalsIgnoreCase("ownDisplayCard"));
    	assertTrue(game.playAgainstTarget("Retreat 0", "4001", "0"));
    	//assertFalse(game.playAgainstTarget("Retreat 0", "", "0"));
    	assertTrue(game.getPlayer(4001).getScore() == 4);
    	assertTrue(game.getPlayer(4001).getHand().contains(c));
    }
    
    //playing the knock down card when the target is playing (can be played)
    //(it isn't possible form a player to not have any cards in their hand even if they've withdrawn)    
    @Test
	public void playKnockDown1() {
    	System.out.println("@Test: playKnockDown1");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	Card c = new Card("Squire", 3);
    	game.getPlayer(4001).addToHand(c);
    	Card c2 = new Card("Purple", 4);
    	game.getPlayer(4001).addToHand(c2);
    	game.playCard("Purple 4"); //to stay in the game
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("KnockDown", 0));
    	assertTrue(game.playCard("KnockDown 0").equalsIgnoreCase("targetHandCard"));
    	assertTrue(game.playAgainstTarget("KnockDown 0", "4001", "" + (game.getPlayer(4001).getHandSize() - 1)));
    	
    	//if 4001 already had a purple 4 card the card we get will be c2 otherwise it will be c
    	assertTrue(game.getPlayer(4002).getHand().contains(c) || game.getPlayer(4002).getHand().contains(c2));
    }
    
    //playing the Outwit card with an empty display & no shield & no stunned (can't play)
    @Test
	public void playOutwit1() {
    	System.out.println("@Test: playOutwit1");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Outwit", 0));
    	assertTrue(game.playCard("Outwit 0").equalsIgnoreCase("targetDisplayOwnDisplay"));
    	assertFalse(game.playAgainstTarget("Outwit 0", "4002", "0", "0"));
    }
    
    //playing the Outwit card against an empty display & no shield & no stunned (can't play)
    @Test
	public void playOutwit2() {
    	System.out.println("@Test: playOutwit2");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.playCard("Squire 3");
    	
    	game.getPlayer(4001).addToHand(new Card("Outwit", 0));
    	assertTrue(game.playCard("Outwit 0").equalsIgnoreCase("targetDisplayOwnDisplay"));
    	assertFalse(game.playAgainstTarget("Outwit 0", "4002", "0", "0"));
    }
    
    //playing the Outwit card to exchange the shield card for the stunned card (can play)
    @Test
	public void playOutwit3() {
    	System.out.println("@Test: playOutwit3");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.playCard("Squire 3");
    	assertFalse(game.endTurn());
    	
    	//stun 4001
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Stunned", 0));
    	assertTrue(game.playCard("Stunned 0").equalsIgnoreCase("targetHandCard"));
    	assertTrue(game.playAgainstTarget("Stunned 0", "4001", ""));
    	assertFalse(game.getPlayer(4001).getShield());
    	assertTrue(game.getPlayer(4001).getStunned());
    	
    	//shield 4002
    	game.getPlayer(4002).addToHand(new Card("Shield", 0));
    	assertTrue(game.playCard("Shield 0").equalsIgnoreCase("Good"));
    	assertFalse(game.getPlayer(4002).getStunned());
    	assertTrue(game.getPlayer(4002).getShield());
    	
    	game.getPlayer(4002).addToHand(new Card("Outwit", 0));
    	assertEquals("targetDisplayOwnDisplay", game.playCard("Outwit 0"));
    	assertTrue(game.playAgainstTarget("Outwit 0", "4001", "-1", "-2"));
    	
    	//Check 4001
    	assertFalse(game.getPlayer(4001).getStunned());
    	assertTrue(game.getPlayer(4001).getShield());
    	
    	//Check 4002
    	assertTrue(game.getPlayer(4002).getStunned());
    	assertFalse(game.getPlayer(4002).getShield());
    }
    
    //playing the Outwit card to exchange the stunned card for the shield card (can play)
    @Test
	public void playOutwit4() {
    	System.out.println("@Test: playOutwit4");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.playCard("Squire 3");
    	
    	//stun 4002
    	game.getPlayer(4001).addToHand(new Card("Stunned", 0));
    	assertTrue(game.playCard("Stunned 0").equalsIgnoreCase("targetHandCard"));
    	assertTrue(game.playAgainstTarget("Stunned 0", "4002", ""));
    	assertTrue(game.getPlayer(4002).getStunned());
    	
    	//shield 4001
    	game.getPlayer(4001).addToHand(new Card("Shield", 0));
    	assertTrue(game.playCard("Shield 0").equalsIgnoreCase("Good"));
    	assertTrue(game.getPlayer(4001).getShield());
    	
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Outwit", 0));
    	assertEquals("targetDisplayOwnDisplay", game.playCard("Outwit 0"));
    	assertTrue(game.playAgainstTarget("Outwit 0", "4001", "-2", "-1"));
    	
    	//Check 4001
    	assertTrue(game.getPlayer(4001).getStunned());
    	assertFalse(game.getPlayer(4001).getShield());
    	
    	//Check 4002
    	assertFalse(game.getPlayer(4002).getStunned());
    	assertTrue(game.getPlayer(4002).getShield());
    }
    
    //playing the Outwit card to exchange the shield card for a display card (can play)
    @Test
	public void playOutwit5() {
    	System.out.println("@Test: playOutwit5");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.getPlayer(4001).addToHand(new Card("Purple", 4));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	assertFalse(game.endTurn());
    	
    	//shield 4002
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Shield", 0));
    	assertTrue(game.playCard("Shield 0").equalsIgnoreCase("Good"));
    	assertTrue(game.getPlayer(4002).getShield());
    	
    	game.getPlayer(4002).addToHand(new Card("Outwit", 0));
    	assertEquals("targetDisplayOwnDisplay", game.playCard("Outwit 0"));
    	assertTrue(game.playAgainstTarget("Outwit 0", "4001", "-1", "0"));
    	
    	//Check 4001
    	assertTrue(game.getPlayer(4001).getShield());
    	assertEquals(1, game.getPlayer(4001).getDisplay().size());
    	assertEquals(4, game.getPlayer(4001).getScore());
    	
    	//Check 4002
    	assertFalse(game.getPlayer(4002).getShield());
    	assertEquals(1, game.getPlayer(4002).getDisplay().size());
    	assertEquals(3, game.getPlayer(4002).getScore());
    }
    
    //playing the Outwit card to exchange a display card for the shield card (can play)
    @Test
	public void playOutwit6() {
    	System.out.println("@Test: playOutwit6");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.playCard("Squire 3");
    	
    	//shield 4001
    	game.getPlayer(4001).addToHand(new Card("Shield", 0));
    	assertTrue(game.playCard("Shield 0").equalsIgnoreCase("Good"));
    	assertTrue(game.getPlayer(4001).getShield());
    	
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Purple", 4));
    	game.getPlayer(4002).addToHand(new Card("Purple", 2));
    	game.playCard("Purple 4");
    	game.playCard("Purple 2");
    	
    	game.getPlayer(4002).addToHand(new Card("Outwit", 0));
    	assertEquals("targetDisplayOwnDisplay", game.playCard("Outwit 0"));
    	assertTrue(game.playAgainstTarget("Outwit 0", "4001", "1", "-1"));
    	
    	//Check 4001
    	assertFalse(game.getPlayer(4001).getShield());
    	assertEquals(2, game.getPlayer(4001).getDisplay().size());
    	assertEquals(5, game.getPlayer(4001).getScore());
    	
    	//Check 4002
    	assertTrue(game.getPlayer(4002).getShield());
    	assertEquals(1, game.getPlayer(4002).getDisplay().size());
    	assertEquals(4, game.getPlayer(4002).getScore());
    }
    
    //playing the Outwit card to exchange the stunned card for a display card (can play)
    @Test
	public void playOutwit7() {
    	System.out.println("@Test: playOutwit7");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.getPlayer(4001).addToHand(new Card("Purple", 4));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	
    	//stun 4002
    	game.getPlayer(4001).addToHand(new Card("Stunned", 0));
    	assertTrue(game.playCard("Stunned 0").equalsIgnoreCase("targetHandCard"));
    	assertTrue(game.playAgainstTarget("Stunned 0", "4002", ""));
    	assertTrue(game.getPlayer(4002).getStunned());
    	
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Outwit", 0));
    	assertEquals("targetDisplayOwnDisplay", game.playCard("Outwit 0"));
    	assertTrue(game.playAgainstTarget("Outwit 0", "4001", "-2", "1"));
    	
    	//Check 4001
    	assertTrue(game.getPlayer(4001).getStunned());
    	assertEquals(1, game.getPlayer(4001).getDisplay().size());
    	assertEquals(3, game.getPlayer(4001).getScore());
    	
    	//Check 4002
    	assertFalse(game.getPlayer(4002).getStunned());
    	assertEquals(1, game.getPlayer(4002).getDisplay().size());
    	assertEquals(4, game.getPlayer(4002).getScore());
    }
    
    //playing the Outwit card to exchange a display card for the stunned card (can play)
    @Test
	public void playOutwit8() {
    	System.out.println("@Test: playOutwit8");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.getPlayer(4001).addToHand(new Card("Maiden", 6));
    	game.getPlayer(4001).addToHand(new Card("Purple", 4));
    	game.playCard("Squire 3");
    	game.playCard("Maiden 6");
    	game.playCard("Purple 4");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Purple", 2));
    	game.getPlayer(4002).addToHand(new Card("Purple", 4));
    	game.getPlayer(4002).addToHand(new Card("Squire", 2));
    	game.playCard("Purple 2");
    	game.playCard("Purple 4");
    	game.playCard("Squire 2");
    	
    	game.getPlayer(4002).addToHand(new Card("Outwit", 0));
    	assertEquals("targetDisplayOwnDisplay", game.playCard("Outwit 0"));
    	assertTrue(game.playAgainstTarget("Outwit 0", "4001", "2", "1"));
    	
    	//Check 4001
    	assertEquals(3, game.getPlayer(4001).getDisplay().size());
    	assertEquals(9, game.getPlayer(4001).getScore());
    	
    	//Check 4002
    	assertEquals(3, game.getPlayer(4002).getDisplay().size());
    	assertEquals(12, game.getPlayer(4002).getScore());
    }
    
    //playing the Outwit card to exchange a display card for a display card (can play)
    @Test
	public void playOutwit9() {
    	System.out.println("@Test: playOutwit9");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.playCard("Squire 3");
    	assertFalse(game.endTurn());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Purple", 2));
    	game.getPlayer(4002).addToHand(new Card("Purple", 4));
    	game.playCard("Purple 2");
    	game.playCard("Purple 4");
    	
    	//stun 4001
    	game.getPlayer(4002).addToHand(new Card("Stunned", 0));
    	assertTrue(game.playCard("Stunned 0").equalsIgnoreCase("targetHandCard"));
    	assertTrue(game.playAgainstTarget("Stunned 0", "4001", ""));
    	assertTrue(game.getPlayer(4001).getStunned());
    	
    	game.getPlayer(4002).addToHand(new Card("Outwit", 0));
    	assertEquals("targetDisplayOwnDisplay", game.playCard("Outwit 0"));
    	assertTrue(game.playAgainstTarget("Outwit 0", "4001", "0", "-2"));
    	
    	//Check 4001
    	assertFalse(game.getPlayer(4001).getStunned());
    	assertEquals(2, game.getPlayer(4001).getDisplay().size());
    	assertEquals(5, game.getPlayer(4001).getScore());
    	
    	//Check 4002
    	assertTrue(game.getPlayer(4002).getStunned());
    	assertEquals(1, game.getPlayer(4002).getDisplay().size());
    	assertEquals(4, game.getPlayer(4002).getScore());
    }
    
    // test playing the shield card and playing an action card (riposte) against the 
    // player with the shield
    @Test
	public void playAgainstShield1() {
    	System.out.println("@Test: playAgainstShield1");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.getPlayer(4001).addToHand(new Card("Purple", 4));
    	game.getPlayer(4001).addToHand(new Card("Shield", 0));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	game.playCard("Shield 0");
    	assertFalse(game.endTurn());
    	assertTrue(game.getPlayer(4001).getShield());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Riposte", 0));
    	assertTrue(game.playCard("Riposte 0").equalsIgnoreCase("Target"));
    	assertFalse(game.playAgainstTarget("Riposte 0", "4001", ""));
    	assertTrue(game.getPlayer(4001).getScore() == 7);
    	assertTrue(game.getPlayer(4002).getScore() == 0);
    	assertFalse(game.getPlayer(4002).hasPlayedACard);
    	assertEquals(0, game.getPlayer(4002).numPlayedDisplay);
    }
    
    // test playing the stunned card and the stunned player playing colour and supporter cards to display
    @Test
	public void playStunned1() {
    	System.out.println("@Test: playStunned1");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("P");
    	
    	//4001 plays and stuns 4002
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.getPlayer(4001).addToHand(new Card("Purple", 4));
    	game.getPlayer(4001).addToHand(new Card("Stunned", 0));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	assertEquals("targetHandCard", game.playCard("Stunned 0"));
    	assertTrue(game.playAgainstTarget("Stunned 0", "4002", "0"));
    	assertFalse(game.endTurn());
    	
    	//4002 is stunned so after the first card the others should fail
    	assertTrue(game.getPlayer(4002).getStunned());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Purple", 5));
    	game.getPlayer(4002).addToHand(new Card("Purple", 4));
    	game.getPlayer(4002).addToHand(new Card("Squire", 3));
    	game.getPlayer(4002).addToHand(new Card("Maiden", 6));
    	assertEquals("Good", game.playCard("Purple 5"));
    	assertEquals("Bad", game.playCard("Purple 4"));
    	assertEquals("Bad", game.playCard("Squire 3"));
    	assertEquals("Bad", game.playCard("Maiden 6"));
    	
    	//Check that only the first card was added to 4002's display
    	assertTrue(game.getPlayer(4002).getScore() == 5);
    	assertEquals(1, game.getPlayer(4002).numPlayedDisplay);
    }
    
    // test playing the stunned card and the stunned player playing a card to display and then trying to play dodge
    // (or any action card that doesn't add to the display)
    @Test
	public void playStunned2() {
    	System.out.println("@Test: playStunned2");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("P");
   	
    	//4001 plays and stuns 4002
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.getPlayer(4001).addToHand(new Card("Purple", 4));
    	game.getPlayer(4001).addToHand(new Card("Stunned", 0));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	assertEquals("targetHandCard", game.playCard("Stunned 0"));
    	assertTrue(game.playAgainstTarget("Stunned 0", "4002", "0"));
    	assertFalse(game.endTurn());
   	
    	//4002 is stunned so after the first card the others should fail
    	assertTrue(game.getPlayer(4002).getStunned());
   	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Purple", 5));
    	assertEquals("Good", game.playCard("Purple 5"));
    	
    	game.getPlayer(4002).addToHand(new Card("Dodge", 0));
    	assertTrue(game.playCard("Dodge 0").equalsIgnoreCase("targetDisplayCard"));
    	assertTrue(game.playAgainstTarget("Dodge 0", "4001", "1"));
   	
    	//Check that only the first card was added to 4002's display
    	assertTrue(game.getPlayer(4002).getScore() == 5);
    	
    	assertTrue(game.getPlayer(4001).getScore() == 3);
    }
    
    // test playing the stunned card and the stunned player trying to play dodge (or any action card that doesn't 
    //add to the display) and then playing a card to display (can play)
    @Test
	public void playStunned3() {
    	System.out.println("@Test: playStunned3");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("P");
   	
    	//4001 plays and stuns 4002
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.getPlayer(4001).addToHand(new Card("Purple", 4));
    	game.getPlayer(4001).addToHand(new Card("Stunned", 0));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	assertEquals("targetHandCard", game.playCard("Stunned 0"));
    	assertTrue(game.playAgainstTarget("Stunned 0", "4002", "0"));
    	assertFalse(game.endTurn());
   	
    	//4002 is stunned so after the first card the others should fail
    	assertTrue(game.getPlayer(4002).getStunned());
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Dodge", 0));
    	assertTrue(game.playCard("Dodge 0").equalsIgnoreCase("targetDisplayCard"));
    	assertTrue(game.playAgainstTarget("Dodge 0", "4001", "1"));
   	
    	game.getPlayer(4002).addToHand(new Card("Purple", 5));
    	assertEquals("Good", game.playCard("Purple 5"));
   	
    	//Check that only the first card was added to 4002's display
    	assertTrue(game.getPlayer(4002).getScore() == 5);
    	assertEquals(1, game.getPlayer(4002).numPlayedDisplay);
    	
    	assertTrue(game.getPlayer(4001).getScore() == 3);
    }
    
    // test playing the stunned card and the stunned player playing a card to display and then trying to play riposte
    @Test
	public void playStunned4() {
    	System.out.println("@Test: playStunned4");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("P");
   	
    	//4001 plays and stuns 4002
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.getPlayer(4001).addToHand(new Card("Purple", 4));
    	game.getPlayer(4001).addToHand(new Card("Stunned", 0));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	assertEquals("targetHandCard", game.playCard("Stunned 0"));
    	assertTrue(game.playAgainstTarget("Stunned 0", "4002", "0"));
    	assertFalse(game.endTurn());
   	
    	//4002 is stunned so after the first card the others should fail
    	assertTrue(game.getPlayer(4002).getStunned());
   	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Purple", 5));
    	assertEquals("Good", game.playCard("Purple 5"));
    	
    	game.getPlayer(4002).addToHand(new Card("Riposte", 0));
    	assertTrue(game.playCard("Riposte 0").equalsIgnoreCase("Bad"));
    	//assertFalse(game.playAgainstTarget("Riposte 0", "4001", ""));
   	
    	//Check that only the first card was added to 4002's display
    	assertTrue(game.getPlayer(4002).getScore() == 5);
    	assertEquals(1, game.getPlayer(4002).numPlayedDisplay);
    }
    
    // test withdrawing with no tokens, cards in display or maidens played
    @Test
	public void Withdraw1() {
    	System.out.println("@Test: Withdraw1");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("G");
    	
    	boolean result = game.withdraw();
    	Player p = game.getPlayer(4001);
    	assertFalse(result);
    	assertFalse(p.playing);
    	assertTrue(0 == p.getScore());
    	assertTrue(p.getDisplay().isEmpty());
	}
    
 // test withdrawing with maiden in display but no tokens
    @Test
	public void Withdraw2() {
    	System.out.println("@Test: Withdraw2");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("G");
    	
    	game.getPlayer(4001).getHand().clear();
       	game.getPlayer(4001).addToHand(new Card("Green", 1));
    	game.getPlayer(4001).addToHand(new Card("Maiden", 6));
    	game.playCard("Green 1");
    	game.playCard("Maiden 6");
    	
    	boolean result = game.withdraw();
    	Player p = game.getPlayer(4001);
    	assertFalse(result);
    	assertFalse(p.playing);
    	assertTrue(0 == p.getScore());
    	assertTrue(p.getDisplay().isEmpty());
	}
    
 // test withdrawing with maiden in display and a token
    @Test
	public void Withdraw3() {
    	System.out.println("@Test: Withdraw3");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("G");
    	
    	game.getPlayer(4001).getHand().clear();
       	game.getPlayer(4001).addToHand(new Card("Green", 1));
    	game.getPlayer(4001).addToHand(new Card("Maiden", 6));
    	game.playCard("Green 1");
    	game.playCard("Maiden 6");
    	game.getPlayer(4001).addToken('G');
    	
    	boolean result = game.withdraw();
    	Player p = game.getPlayer(4001);
    	assertTrue(result);
    	assertFalse(p.playing);
    	assertTrue(0 == p.getScore());
    	assertTrue(p.getDisplay().isEmpty());
	}
    
    //test ending turn without having played any type of card
    @Test
	public void EndTurn1() {
    	System.out.println("@Test: EndTurn1");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("G");
    	
    	assertTrue(game.endTurn());
	}
    
    //test ending turn having played at least 1 card, score is high enough
    @Test
	public void EndTurn2() {
    	System.out.println("@Test: EndTurn2");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("G");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Green", 1));
    	game.playCard("Green 1");
    	
    	//check that it moves to the next player
    	Player p = game.getPlayer(4001);
    	assertFalse(game.endTurn());
    	assertFalse(game.checkCurrPlayer(4001));
    	assertTrue(p.playing);
    	assertTrue(1 == p.getScore());
	}
    
    //test ending turn having played at least 1 card, score is not enough
    @Test
	public void EndTurn3() {
    	System.out.println("@Test: EndTurn3");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("G");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Green", 1));
    	game.playCard("Green 1");
    	
    	game.getPlayer(4002).setScore(5);
    	
    	//Player p = game.getPlayer(4001);
    	assertTrue(game.endTurn());
	}
    
    //test withdrawing that ends a tournament, check that the winner is given a token of the colour
    @Test
	public void EndTournament1() {
    	System.out.println("@Test: EndTournament1");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("G");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Green", 1));
    	game.playCard("Green 1");
    	
    	game.withdraw();
    	assertTrue(game.state == Game.WAITING);
    	boolean result = game.endTournament();
    	assertFalse(result);
    	Player p = game.getPlayer(4002);
    	assertTrue(p.getTokens().size() == 1);
	}
    
    //test multiple tournaments
    @Test
	public void MultipleTournaments() {
    	System.out.println("@Test: MultipleTournaments");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("G");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Green", 1));
    	game.playCard("Green 1");
    	game.withdraw();
    	
    	assertFalse(game.endTournament());
    	assertTrue(game.getPlayer(4002).getTokens().size() == 1);
    	assertTrue(game.getPlayer(4002).getTokens().contains('G'));
    	
    	game.beginTournament();
    	assertTrue(game.checkCurrPlayer(4002));
    	assertEquals(0, game.getPlayer(4001).getScore());
    	assertEquals(0, game.getPlayer(4002).getScore());
    	assertTrue(game.getPlayer(4001).playing);
    	assertTrue(game.getPlayer(4002).playing);
	}
    
  //test that a green tournament can be played
    @Test
	public void GreenTournament() {
    	System.out.println("@Test: GreenTournament");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("G");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Green", 1));
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.getPlayer(4001).addToHand(new Card("Green", 1));
    	game.playCard("Green 1");
    	game.playCard("Squire 3");
    	game.playCard("Green 1");
    	assertEquals(3, game.getPlayer(4001).getScore());
    	assertFalse(game.endTurn()); //stays in tournament
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Green", 1));
    	game.getPlayer(4002).addToHand(new Card("Red", 3));
    	game.playCard("Green 1");
    	game.playCard("Red 3");
    	assertEquals(1, game.getPlayer(4002).getScore());
    	assertTrue(game.endTurn()); //must withdraw
    	assertFalse(game.withdraw()); //can't choose token received
    	
    	assertFalse(game.endTournament());
    	assertTrue(game.getPlayer(4001).getTokens().size() == 1);
    	assertTrue(game.getPlayer(4001).getTokens().contains('G'));
	}
    
  //test that a yellow tournament can be played
    @Test
	public void YellowTournament() {
    	System.out.println("@Test: YellowTournament");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("Y");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Yellow", 2));
    	game.getPlayer(4001).addToHand(new Card("Squire", 3));
    	game.getPlayer(4001).addToHand(new Card("Green", 1));
    	game.playCard("Yellow 2");
    	game.playCard("Squire 3");
    	game.playCard("Green 1");
    	assertEquals(5, game.getPlayer(4001).getScore());
    	assertFalse(game.endTurn()); //stays in tournament
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Yellow", 2));
    	game.getPlayer(4002).addToHand(new Card("Yellow", 3));
    	game.playCard("Yellow 2");
    	game.playCard("Yellow 3");
    	assertEquals(5, game.getPlayer(4002).getScore());
    	assertTrue(game.endTurn()); //must withdraw
    	assertFalse(game.withdraw()); //can't choose token received
    	
    	assertFalse(game.endTournament());
    	assertTrue(game.getPlayer(4001).getTokens().size() == 1);
    	assertTrue(game.getPlayer(4001).getTokens().contains('Y'));
	}
    
  //test that a red tournament can be played
    @Test
	public void RedTournament() {
    	System.out.println("@Test: RedTournament");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("R");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Maiden", 6));
    	game.playCard("Maiden 6");
    	assertEquals(6, game.getPlayer(4001).getScore());
    	assertFalse(game.endTurn()); //stays in tournament
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Red", 5));
    	game.getPlayer(4002).addToHand(new Card("Yellow", 3));
    	game.playCard("Red 5");
    	game.playCard("Yellow 3");
    	System.out.println("player's score is: " + game.getPlayer(4002).getScore());
    	assertEquals(5, game.getPlayer(4002).getScore());
    	assertTrue(game.endTurn()); //must withdraw
    	assertFalse(game.withdraw()); //can't choose token received
    	
    	assertFalse(game.endTournament());
    	assertTrue(game.getPlayer(4001).getTokens().size() == 1);
    	assertTrue(game.getPlayer(4001).getTokens().contains('R'));
	}
    
  //test that a blue tournament can be played
    @Test
	public void BlueTournament() {
    	System.out.println("@Test: BlueTournament");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("B");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Blue", 4));
    	game.getPlayer(4001).addToHand(new Card("Purple", 5));
    	game.playCard("Blue 4");
    	game.playCard("Purple 5");
    	assertEquals(4, game.getPlayer(4001).getScore());
    	assertFalse(game.withdraw()); //can't choose token received
    	
    	assertEquals(game.state, Game.WAITING);
    	
    	assertFalse(game.endTournament());
    	assertTrue(game.getPlayer(4002).getTokens().size() == 1);
    	assertTrue(game.getPlayer(4002).getTokens().contains('B'));
	}
    
  //test that a purple tournament can be played
    @Test
	public void PurpleTournament() {
    	System.out.println("@Test: PurpleTournament");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.beginGame();
    	game.setColour("P");
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Purple", 3));
    	game.playCard("Purple 3");
    	assertEquals(3, game.getPlayer(4001).getScore());
    	assertFalse(game.endTurn()); //stays in tournament
    	
    	game.getPlayer(4002).getHand().clear();
    	game.getPlayer(4002).addToHand(new Card("Squire", 3));
    	game.getPlayer(4002).addToHand(new Card("Purple", 4));
    	game.getPlayer(4002).addToHand(new Card("Green", 1));
    	game.playCard("Squire 3");
    	game.playCard("Purple 4");
    	game.playCard("Green 1");
    	assertEquals(7, game.getPlayer(4002).getScore());
    	assertFalse(game.endTurn()); //stays in tournament
    	
    	game.getPlayer(4001).getHand().clear();
    	game.getPlayer(4001).addToHand(new Card("Purple", 3));
    	game.playCard("Purple 3");
    	assertEquals(6, game.getPlayer(4001).getScore());
    	assertTrue(game.endTurn()); //must withdraw
    	assertFalse(game.withdraw()); //doesn't loose a token
    	    	
    	assertTrue(game.endTournament()); //choose token to receive
    	game.getPlayer(4002).addToken('P');
    	assertTrue(game.getPlayer(4002).getTokens().size() == 1);
    	assertTrue(game.getPlayer(4002).getTokens().contains('P'));
	}
    
    //starting a new tournament with purple after a purple tournament
}