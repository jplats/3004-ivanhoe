package ivanhoe;

import static org.junit.Assert.*;
import java.util.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Before;

public class TestScenarios {
	Game game;

	/** This will be processed before the Test Class is instantiated */
    @BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass: TestGame");
    }

    /** This is processed/initialized before each test is conducted */
    @Before
    public void setUp() {
		System.out.println("@Before(): TestGame");
		game = new Game(3);
	}
	
    /** This is processed/initialized after each test is conducted */
    @After
    public void tearDown () {
		System.out.println("@After(): TestGame");
		game = null;
	}
	
    /** This will be processed after the Test Class has been destroyed */
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass: TestGame");
    }
    
    /** Each unit test is annotated with the @Test Annotation */
    
    //A: the player who should start cannot start a tournament
    //B: last tournament was purple, cannot be purple again
    @Test
	public void playScenarioA() {
        System.out.println("@Test: playScenarioA");
        game.addPlayer(4001, "Jane");
        game.addPlayer(4002, "Peter");
        game.addPlayer(4003, "Pat");
        game.beginGame();
        game.setColour("P");
        
        //The tournament does the draw for the first player then the nextPlayer function handles it.  
        game.getPlayer(4001).addToHand(new Card("Purple", 3));
        game.playCard("Purple 3");
        assertFalse(game.endTurn()); 
        
        //Draw is done automatically in the game, withdraw 4002
        assertFalse(game.withdraw());
       	
        //Check the tournament is still going
        assertEquals(Game.INTOURNAMENT, game.state);
       	
        //Draw is done automatically in the game, withdraw 4003
        assertFalse(game.withdraw());
        	
        //Check the tournament has been won and assign the token
        assertEquals(Game.WAITING, game.state);
        assertTrue(game.endTournament()); 
        game.awardToken('P'); //choose token
        assertEquals(1, game.getPlayer(4001).getTokens().size());
        assertTrue(game.getPlayer(4001).getTokens().contains('P'));
        	
        //Make a new hand for 4001 so that they can't play
        game.getPlayer(4001).getHand().clear();
        game.getPlayer(4001).addToHand(new Card("Unhorse", 0));
        game.getPlayer(4001).addToHand(new Card("BreakLance", 0));
                
        //Start a new tournament
        game.beginTournament();
        
        assertTrue(game.checkCurrPlayer(4002));
        assertFalse(game.setColour("P"));
        assertTrue(game.setColour("R"));
        
    }
    
    //E: coming to end of deck (The discard pile is shuffled and used as the new deck)
    @Test
	public void playScenarioE() {
    	System.out.println("@Test: playScenarioE");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("G");
    	
    	game.discardPile.add(new Card("Green", 1));
    	assertEquals(1, game.discardPile.size());
    	//110 - (3*8 cards for the initial hands)
    	assertEquals(86, game.drawPile.size());
    	for (int i = 0; i < 86; i++) {
    		game.drawCard();
    	}
    	assertTrue(game.drawPile.isEmpty());
    	Card c = game.drawCard();
    	assertEquals("Green", c.getName());
    	assertEquals(1, c.getValue());
    }
    
    //F: using CHARGE in a green tournament with every player with only green 1s: one card must remain
    @Test
	public void playScenarioF() {
    	System.out.println("@Test: playScenarioF");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("G");
    	
    	//should keep purple 3
    	game.getPlayer(4001).addToHand(new Card("Green", 1));
    	game.getPlayer(4001).addToHand(new Card("Green", 1));
    	game.playCard("Green 1");
    	game.playCard("Green 1");
    	assertFalse(game.endTurn());
    	
    	//should keep purple 4
    	game.getPlayer(4002).addToHand(new Card("Green", 1));
    	game.getPlayer(4002).addToHand(new Card("Green", 1));
    	game.getPlayer(4002).addToHand(new Card("Green", 1));
    	game.playCard("Green 1");
    	game.playCard("Green 1");
    	game.playCard("Green 1");
    	assertFalse(game.endTurn());
    	
    	//should keep both cards
    	game.getPlayer(4003).addToHand(new Card("Green", 1));
    	game.getPlayer(4003).addToHand(new Card("Green", 1));
    	game.getPlayer(4003).addToHand(new Card("Green", 1));
    	game.getPlayer(4003).addToHand(new Card("Green", 1));
    	game.playCard("Green 1");
    	game.playCard("Green 1");
    	game.playCard("Green 1");
    	game.playCard("Green 1");
    	
    	game.getPlayer(4003).addToHand(new Card("Charge", 0));
    	assertTrue(game.playCard("Charge 0").equalsIgnoreCase("Good"));
    	
    	//Check player 4001
    	assertEquals(1, game.getPlayer(4001).getScore());
    	assertEquals(1, game.getPlayer(4001).getDisplay().size());
    	
    	//Check player 4002
    	assertEquals(1, game.getPlayer(4002).getScore());
    	assertEquals(1, game.getPlayer(4002).getDisplay().size());
    	
    	//Check player 4003
    	assertEquals(1, game.getPlayer(4003).getScore());
    	assertEquals(1, game.getPlayer(4003).getDisplay().size());
    }
    
    //H: winning the game
    @Test
	public void playScenarioH() {
    	System.out.println("@Test: playScenarioH");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("R");
    	
    	//The tournament does the draw for the first player then the nextPlayer function handles it.    	
    	game.getPlayer(4001).addToHand(new Card("Red", 3));
    	game.playCard("Red 3");
    	assertFalse(game.endTurn()); 
    	
    	//Draw is done automatically in the game, withdraw 4002
    	assertFalse(game.withdraw());
    	
    	//Draw is done automatically in the game, withdraw 4003
    	assertFalse(game.withdraw());
    	
    	//Check the tournament has been won and assign the token
    	assertFalse(game.endTournament()); 
    	assertEquals(1, game.getPlayer(4001).getTokens().size());
    	assertTrue(game.getPlayer(4001).getTokens().contains('R'));
    	
    	//Start a new tournament
    	game.beginTournament();
    	game.setColour("B");
    	
    	//The tournament does the draw for the first player then the nextPlayer function handles it.    	
    	game.getPlayer(4001).addToHand(new Card("Blue", 3));
    	game.playCard("Blue 3");
    	assertFalse(game.endTurn()); 
    	
    	//Draw is done automatically in the game, withdraw 4002
    	assertFalse(game.withdraw());
    	
    	//Draw is done automatically in the game, withdraw 4003
    	assertFalse(game.withdraw());
    	
    	//Check the tournament has been won and assign the token
    	assertFalse(game.endTournament()); 
    	assertEquals(2, game.getPlayer(4001).getTokens().size());
    	assertTrue(game.getPlayer(4001).getTokens().contains('B'));
    	
    	//Start a new tournament
    	game.beginTournament();
    	game.setColour("Y");
    	
    	//The tournament does the draw for the first player then the nextPlayer function handles it.    	
    	game.getPlayer(4001).addToHand(new Card("Yellow", 3));
    	game.playCard("Yellow 3");
    	assertFalse(game.endTurn()); 
    	
    	//Draw is done automatically in the game, withdraw 4002
    	assertFalse(game.withdraw());
    	
    	//Draw is done automatically in the game, withdraw 4003
    	assertFalse(game.withdraw());
    	
    	//Check the tournament has been won and assign the token
    	assertFalse(game.endTournament()); 
    	assertEquals(3, game.getPlayer(4001).getTokens().size());
    	assertTrue(game.getPlayer(4001).getTokens().contains('Y'));
    	
    	//Start a new tournament
    	game.beginTournament();
    	game.setColour("G");
    	
    	//The tournament does the draw for the first player then the nextPlayer function handles it.    	
    	game.getPlayer(4001).addToHand(new Card("Green", 1));
    	game.playCard("Green 1");
    	assertFalse(game.endTurn()); 
    	
    	//Draw is done automatically in the game, withdraw 4002
    	assertFalse(game.withdraw());
    	
    	//Draw is done automatically in the game, withdraw 4003
    	assertFalse(game.withdraw());
    	
    	//Check the tournament has been won and assign the token
    	assertFalse(game.endTournament()); 
    	assertEquals(4, game.getPlayer(4001).getTokens().size());
    	assertTrue(game.getPlayer(4001).getTokens().contains('G'));
    	
    	//Check that the game has NOT been won by 4001
    	assertFalse(game.hasWon());
    	
    	//Start a new tournament
    	game.beginTournament();
    	game.setColour("P");
    	
    	//The tournament does the draw for the first player then the nextPlayer function handles it.    	
    	game.getPlayer(4001).addToHand(new Card("Purple", 3));
    	game.playCard("Purple 3");
    	assertFalse(game.endTurn()); 
    	
    	//Draw is done automatically in the game, withdraw 4002
    	assertFalse(game.withdraw());
    	
    	//Draw is done automatically in the game, withdraw 4003
    	assertFalse(game.withdraw());
    	
    	//Check the tournament has been won and assign the token
    	assertTrue(game.endTournament()); 
    	assertTrue(game.awardToken('P'));
    	assertEquals(5, game.getPlayer(4001).getTokens().size());
    	assertTrue(game.getPlayer(4001).getTokens().contains('P'));
    	
    	//Check that the game has been won by 4001
    	assertTrue(game.hasWon());
    }
    
    //do you enforce NOT choosing a token already received when winning a purple tournament
    @Test
	public void playScenarioX() {
    	System.out.println("@Test: playScenarioX");
    	game.addPlayer(4001, "Jane");
    	game.addPlayer(4002, "Peter");
    	game.addPlayer(4003, "Pat");
    	game.beginGame();
    	game.setColour("R");
    	
    	//The tournament does the draw for the first player then the nextPlayer function handles it.    	
    	game.getPlayer(4001).addToHand(new Card("Red", 3));
    	game.playCard("Red 3");
    	assertFalse(game.endTurn()); 
    	
    	//Draw is done automatically in the game, withdraw 4002
    	assertFalse(game.withdraw());
    	
    	//Check the tournament is still going
    	assertEquals(Game.INTOURNAMENT, game.state);
    	
    	//Draw is done automatically in the game, withdraw 4003
    	assertFalse(game.withdraw());
    	
    	//Check the tournament has been won and assign the token
    	assertEquals(Game.WAITING, game.state);
    	assertFalse(game.endTournament()); 
    	assertEquals(1, game.getPlayer(4001).getTokens().size());
    	assertTrue(game.getPlayer(4001).getTokens().contains('R'));
    	
    	//Start a new tournament
    	game.beginTournament();
    	game.setColour("P");
    	
    	//The tournament does the draw for the first player then the nextPlayer function handles it.    	
    	game.getPlayer(4001).addToHand(new Card("Purple", 3));
    	game.playCard("Purple 3");
    	assertFalse(game.endTurn()); 
    	
    	//Draw is done automatically in the game, withdraw 4002
    	assertFalse(game.withdraw());
    	
    	//Check the tournament is still going
    	assertEquals(Game.INTOURNAMENT, game.state);
    	
    	//Draw is done automatically in the game, withdraw 4003
    	assertFalse(game.withdraw());
    	
    	//Check the tournament has been won and assign the token
    	assertEquals(Game.WAITING, game.state);
    	assertTrue(game.endTournament()); 
    	assertFalse(game.awardToken('R')); 
    	assertTrue(game.awardToken('B')); 
    }
    
    
    
}