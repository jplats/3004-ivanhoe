package ivanhoe;
import java.net.*;
import java.io.*;

public class ClientThread extends Thread{
	private Socket socket;
	private Client client;
	private BufferedReader streamIn;
	private boolean done;
	
	public ClientThread(Client client, Socket socket){
		super();
		this.client = client;
		this.socket = socket;
		this.open();
		this.start();
		done = false;
	}
	public void open(){
		try{
			streamIn= new  BufferedReader (new InputStreamReader(socket.getInputStream()));

		}catch(IOException ioe){
		
		}
	}
	public void run(){
		while(!done){	
			try{
				client.handle(streamIn.readLine());
				
				
			}catch(IOException ioe){
				
			}
		}
	}
	public void close(){
		done = true;
	}
}
