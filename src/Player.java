package ivanhoe;
import java.util.*;

public class Player{

	private String name;
	private Integer portId;

	private ArrayList<Card> hand;
	private ArrayList<Card> display;

	private Integer score = 0;

	private Set<Character> tokens;
	private Boolean shield;
	private Boolean stunned;
	
	private Boolean hasIvanhoe;
	
	public boolean playing;
	public boolean hasPlayedACard; //track if the player has played at least one card in their turn.
	public int numPlayedDisplay; //track the number of cards added to display for the stunned card

	public Player(int portId, String name) {
		this.portId = portId;
		this.name = name;
		this.hand = new ArrayList<Card>();
		this.display = new ArrayList<Card>();
		this.tokens = new HashSet<Character>();
		this.shield = false;
		this.stunned = false;
		this.playing = true;
		this.hasIvanhoe = false;
		this.hasPlayedACard = false;
		this.numPlayedDisplay = 0;
	}
	
	public Player(Player old) {
		this.portId = old.portId;
		this.name = old.name;
		this.score = old.score;
		this.hand = new ArrayList<Card>(old.hand);
		this.display = new ArrayList<Card>(old.display);
		this.tokens = new HashSet<Character>(old.tokens);
		this.shield = old.shield;
		this.stunned = old.stunned;
		this.playing = old.playing;
		this.hasIvanhoe = old.hasIvanhoe;
		this.hasPlayedACard = old.hasPlayedACard;
		this.numPlayedDisplay = old.numPlayedDisplay;
	}

	public Integer getPortId() {
		return portId;
	}

	public void setPortId(Integer portId) {
		this.portId = portId;
	}

	public String getName() {
		return name;
	}

	public void addToHand(Card c) {
		hand.add(c);
	}

	/*
	  This function will be used to send the data to the player but we don't want to send the cards
	  so what should this function really be returning. (Temporary implementation).
	*/
	public ArrayList<Card> getHand() {
		return hand;
	}

	public Card removeFromHand(String name, int value) {
		for (int i = 0; i < hand.size(); i++) {
			if (hand.get(i).getName().equalsIgnoreCase(name) && hand.get(i).getValue() == value) {
				return hand.remove(i);
			}
		}
		return null;
	}

	public int getHandSize() {
		return hand.size();
	}
	
	public int getDisplaySize() {
		return display.size();
	}

	public void addToDisplay(Card c) {
		display.add(c);
	}

	/*
	  Needs a proper implementation. Will we know that card to remove or will be be using the
	  name and value like with the remove from hand function?
	*/
	//public Card removeFromDisplay(Card c) {
	//	return display.remove(c);
	//}

	/*
	  This function will be used to send the data to the player but we don't want to send the cards
	  so what should this function really be returning. (Temporary implementation).
	*/
	public ArrayList<Card> getDisplay() {
		return display;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public boolean addToken(char token) {
		return tokens.add(token);
	}

	public Set<Character> getTokens() {
		return tokens;
	}

	public boolean removeToken(char token) {

		return tokens.remove(token);
	}

	/*public boolean hasWon() {
		return tokens.contains("P") && tokens.contains("R") &&
			   tokens.contains("B") && tokens.contains("Y") &&
			   tokens.contains("G");
	}*/
	public int numTokens() {
		return tokens.size();
	}

	public boolean getShield() {
		return shield;
	}

	public void setShield(boolean shield) {
		this.shield = shield;
	}

	public boolean getStunned() {
		return stunned;
	}

	public void setStunned(boolean stunned) {
		this.stunned = stunned;
	}
	
	public boolean getHasIvanhoe() {
		return hasIvanhoe;
	}

	public void setHasIvanhoe(boolean ivanhoe) {
		this.hasIvanhoe = ivanhoe;
	}
}
