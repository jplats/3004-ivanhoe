package ivanhoe;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class AiTestMax {
	Server server;
	Client clients[];
	
	@Before
	public void setUp(){
		server = new Server();
		server.startUp(10000);
		clients = new Client[2];
		clients[0] = new Client();
		
		clients[0].connect("0.0.0.0", 10000);
		
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
			
		}
		clients[0].send("create 2");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[0].send("join Peter");

		clients[0].send("AI Jane max");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		clients[0].send("start");
		
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		

		server.game.getPlayer(clients[0].getID()).getHand().clear();
		server.game.getPlayer(1).getHand().clear();
		
	}
	@Test
	public void testMaxAI(){
		
		clients[0].send("set B");
		clients[0].send("end");

		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		server.game.getPlayer(1).getHand().clear();
		server.game.getPlayer(1).addToHand(new Card("Red", 3));
		server.game.getPlayer(1).addToHand(new Card("Red", 4));
		server.game.getPlayer(1).addToHand(new Card("Blue", 3));

		assertEquals(server.game.getColour(),"Red");
		
		try{
			Thread.sleep(3000);
		}catch(InterruptedException ie){
		}
		assertEquals(server.game.getPlayer(1).getScore(), 7);
	}
	
}
