package ivanhoe;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {

	private FileWriter serverLogger = null;
	Logger(){
		try{
			String userDir = System.getProperty("user.dir");
			String serverLogFile = String.format("%s//logs//Server.log", userDir);
			File logDir = new File(String.format("%s//logs", userDir));
			if (!logDir.exists()) logDir.mkdir();
			serverLogger = new FileWriter(serverLogFile);
		}catch(IOException ioe){
			
		}	
	}
	public void write(String s){
		try{
			serverLogger.write(format(s));
			serverLogger.flush();
		}catch(IOException ioe){
			
		}
	}
	private String format (String message) {
		return String.format ("[Time: %23s] : %s" + System.getProperty("line.separator"),getDateTime(), message) ;
	}
	
	private String getDateTime () {
		Format formatter = new SimpleDateFormat("yyyy-MMM-dd hh:mm:ss:SSS");
		return formatter.format((new Date()).getTime());
	}
	public void close() {
		try {
			serverLogger.close();
		} catch (IOException e) {
			System.out.printf("Error while closing log file: %s\n", e.getMessage());
		}
	}
}
