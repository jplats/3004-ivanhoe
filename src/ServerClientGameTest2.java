package ivanhoe;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ServerClientGameTest2 {
	Server server;
	Client clients[];
	
	@Before
	public void setUp(){
		server = new Server();
		server.startUp(10000);
		clients = new Client[2];
		clients[0] = new Client();
		clients[1] = new Client();
		
		clients[0].connect("0.0.0.0", 10000);
		clients[1].connect("0.0.0.0", 10000);
		
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
			
		}
		clients[0].send("create 2");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[0].send("join Peter");
		clients[1].send("join Jane");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		clients[0].send("start");
		
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		//clients[0].send("set R");
		
		/*try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}*/
		server.game.getPlayer(clients[0].getID()).getHand().clear();
		server.game.getPlayer(clients[1].getID()).getHand().clear();
		
	}
	
	@After
	public void tearDown(){
		server.close();
	}
	
	
	//Test playing colour and supported cards
	@Test
	public void testPlayCard() {
		System.out.println("@Test: testPlayCard");
		clients[0].send("set R");
		server.sendGameState();
		
		ArrayList<Card> cards = new ArrayList<Card>();
		cards.add(new Card("Red", 3));
		cards.add(new Card("Squire", 3));
		cards.add(new Card("Maiden", 6));
		cards.add(new Card("Green", 1));
		for (Card c : cards) {
			server.game.getPlayer(clients[0].getID()).addToHand(c);
		}
		server.game.getPlayer(clients[1].getID()).addToHand(cards.get(0));
		
		clients[0].send("play Red 3");
		clients[0].send("play Squire 3");
		clients[0].send("play Maiden 6");
		clients[0].send("play Green 1");
		clients[1].send("play Red 3");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		assertTrue(server.game.getPlayer(clients[0].getID()).getDisplay().contains(cards.get(0)));
		assertTrue(server.game.getPlayer(clients[0].getID()).getDisplay().contains(cards.get(1)));
		assertTrue(server.game.getPlayer(clients[0].getID()).getDisplay().contains(cards.get(2)));
		assertFalse(server.game.getPlayer(clients[0].getID()).getDisplay().contains(cards.get(3)));
		
		assertEquals(12,server.game.getPlayer(clients[0].getID()).getScore());
		assertEquals(0,server.game.getPlayer(clients[1].getID()).getScore());		

		clients[0].send("end turn");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		assertEquals(true, server.game.checkCurrPlayer(clients[1].getID()));
		
	}
	
	
	//Test playing Unhorse in a purple tournament (can play)
	@Test
	public void playUnhorse1() {
		System.out.println("@Test: playUnhorse1");
		clients[0].send("set P");
		server.sendGameState();
		
		Card c = new Card("Unhorse", 0);
		server.game.getPlayer(clients[0].getID()).addToHand(c);
		clients[0].send("play Unhorse 0");
		
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
	//	assertTrue(server.game.getPlayer(clients[0].getID()).hasPlayedACard);
		assertEquals("action colour", clients[0].getLastMessage());	
	}
	
	//Test playing Unhorse in a non-purple tournament (can't play)
	@Test
	public void playUnhorse2() {
		System.out.println("@Test: playUnhorse2");
		clients[0].send("set G");
		server.sendGameState();
			
		Card c = new Card("Unhorse", 0);
		server.game.getPlayer(clients[0].getID()).addToHand(c);
		clients[0].send("play Unhorse 0");
			
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
	//	assertFalse(server.game.getPlayer(clients[0].getID()).hasPlayedACard);
		assertNotEquals("action colour", clients[0].getLastMessage());	
	}
		
	//Test playing Change Weapon in a red, blue or yellow tournament (can play)
	@Test
	public void playChangeWeapon1() {
		System.out.println("@Test: playChangeWeapon1");	
		clients[0].send("set R");
		server.sendGameState();
					
		Card c = new Card("ChangeWeapon", 0);
		server.game.getPlayer(clients[0].getID()).addToHand(c);
		clients[0].send("play ChangeWeapon 0");
					
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
					
		assertTrue(server.game.getPlayer(clients[0].getID()).hasPlayedACard);
		assertEquals("action colour", clients[0].getLastMessage());	
	}
	
	//Test playing Change Weapon in a green or purple tournament (can't play)
	@Test
	public void playChangeWeapon2() {
		System.out.println("@Test: playChangeWeapon2");	
		clients[0].send("set G");
		server.sendGameState();
						
		Card c = new Card("ChangeWeapon", 0);
		server.game.getPlayer(clients[0].getID()).addToHand(c);
		clients[0].send("play ChangeWeapon 0");
						
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
						
		assertFalse(server.game.getPlayer(clients[0].getID()).hasPlayedACard);
		assertNotEquals("action colour", clients[0].getLastMessage());	
	}
	
	//Test playing Drop Weapon in a blue, red or yellow tournament (can play)
	@Test
	public void playDropWeapon1() {
		System.out.println("@Test: playDropWeapon1");	
		clients[0].send("set B");
		server.sendGameState();
							
		Card c = new Card("DropWeapon", 0);
		server.game.getPlayer(clients[0].getID()).addToHand(c);
		clients[0].send("play DropWeapon 0");
						
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
							
		assertTrue(server.game.getPlayer(clients[0].getID()).hasPlayedACard);
		assertNotEquals("action colour", clients[0].getLastMessage());	
		assertEquals("Green", server.game.getColour());
	}
	
	//Test playing Drop Weapon in a purple or green tournament (can't play)
	@Test
	public void playDropWeapon2() {
		System.out.println("@Test: playDropWeapon2");	
		clients[0].send("set P");
		server.sendGameState();
								
		Card c = new Card("DropWeapon", 0);
		server.game.getPlayer(clients[0].getID()).addToHand(c);
		clients[0].send("play DropWeapon 0");
							
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
								
		assertFalse(server.game.getPlayer(clients[0].getID()).hasPlayedACard);
		assertNotEquals("action colour", clients[0].getLastMessage());	
		assertEquals("Purple", server.game.getColour());
	}
	
	//Test playing Break Lance when the target has only one purple card in their display (can't play)
	@Test
	public void playBreakLance1() {
		System.out.println("@Test: playBreakLance1");	
		clients[0].send("set P");
		server.sendGameState();
		
		server.game.getPlayer(clients[0].getID()).addToHand(new Card("Purple", 4));
		clients[0].send("play Purple 4");
		clients[0].send("end turn");
		
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		server.game.getPlayer(clients[1].getID()).addToHand(new Card("BreakLance", 0));
		clients[1].send("play BreakLance 0");
							
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
								
		assertFalse(server.game.getPlayer(clients[1].getID()).hasPlayedACard);
		assertEquals("pickTargetDisplayCard", clients[1].getLastMessage());	
		
		clients[1].send("targetDisplayCard Peter 0");
		
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		assertFalse(server.game.getPlayer(clients[1].getID()).hasPlayedACard);
		assertEquals(4, server.game.getPlayer(clients[0].getID()).getScore());
	}
	
	//Test playing Break Lance when the target has only one purple card in their display (can play)
	//The first card should be left
	@Test
	public void playBreakLance2() {
		System.out.println("@Test: playBreakLance2");	
		clients[0].send("set P");
		server.sendGameState();
		
		server.game.getPlayer(clients[0].getID()).addToHand(new Card("Purple", 5));
		clients[0].send("play Purple 5");
		server.game.getPlayer(clients[0].getID()).addToHand(new Card("Purple", 4));
		clients[0].send("play Purple 4");
		clients[0].send("end turn");
		
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		server.game.getPlayer(clients[1].getID()).addToHand(new Card("BreakLance", 0));
		clients[1].send("play BreakLance 0");
							
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
								
		assertEquals(9, server.game.getPlayer(clients[0].getID()).getScore());
		assertFalse(server.game.getPlayer(clients[1].getID()).hasPlayedACard);
		assertEquals("pickTargetDisplayCard", clients[1].getLastMessage());	
		
		clients[1].send("targetDisplayCard Peter 0");
		
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		assertTrue(server.game.getPlayer(clients[1].getID()).hasPlayedACard);
		assertEquals(5, server.game.getPlayer(clients[0].getID()).getScore());
	}
	
	//Test playing Break Lance when the target has purple and non-purple cards in their display (can play)
	@Test
	public void playBreakLance3() {
		System.out.println("@Test: playBreakLance3");	
		clients[0].send("set P");
		server.sendGameState();
		
		server.game.getPlayer(clients[0].getID()).addToHand(new Card("Purple", 5));
		clients[0].send("play Purple 5");
		server.game.getPlayer(clients[0].getID()).addToHand(new Card("Squire", 3));
		clients[0].send("play Squire 3");
		clients[0].send("end turn");
		
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		server.game.getPlayer(clients[1].getID()).addToHand(new Card("BreakLance", 0));
		clients[1].send("play BreakLance 0");
							
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
								
		assertEquals(8, server.game.getPlayer(clients[0].getID()).getScore());
		assertFalse(server.game.getPlayer(clients[1].getID()).hasPlayedACard);
		assertEquals("pickTargetDisplayCard", clients[1].getLastMessage());	
		
		clients[1].send("targetDisplayCard Peter 0");
		
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		assertTrue(server.game.getPlayer(clients[1].getID()).hasPlayedACard);
		assertEquals(3, server.game.getPlayer(clients[0].getID()).getScore());
	}
	
	//Test playing Break Lance when the target has no purple cards in their display (can't play)
	@Test
	public void playBreakLance4() {
		System.out.println("@Test: playBreakLance4");	
		clients[0].send("set P");
		server.sendGameState();
		
		server.game.getPlayer(clients[0].getID()).addToHand(new Card("Maiden", 6));
		clients[0].send("play Maiden 6");
		server.game.getPlayer(clients[0].getID()).addToHand(new Card("Squire", 3));
		clients[0].send("play Squire 3");
		clients[0].send("end turn");
		
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		server.game.getPlayer(clients[1].getID()).addToHand(new Card("BreakLance", 0));
		clients[1].send("play BreakLance 0");
							
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
								
		assertEquals(9, server.game.getPlayer(clients[0].getID()).getScore());
		assertFalse(server.game.getPlayer(clients[1].getID()).hasPlayedACard);
		assertEquals("pickTargetDisplayCard", clients[1].getLastMessage());	
		
		clients[1].send("targetDisplayCard Peter 0");
		
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		assertFalse(server.game.getPlayer(clients[1].getID()).hasPlayedACard);
		assertEquals(9, server.game.getPlayer(clients[0].getID()).getScore());
	}
	
	//Test playing Riposte when the target has less then 2 cards in their display (can't play)
	@Test
	public void playRiposte1() {
		System.out.println("@Test: playRiposte1");	
		clients[0].send("set P");
		server.sendGameState();
		
		server.game.getPlayer(clients[0].getID()).addToHand(new Card("Squire", 3));
		clients[0].send("play Squire 3");
		clients[0].send("end turn");
		
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		server.game.getPlayer(clients[1].getID()).addToHand(new Card("Riposte", 0));
		clients[1].send("play Riposte 0");
							
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
								
		assertEquals(3, server.game.getPlayer(clients[0].getID()).getScore());
		assertFalse(server.game.getPlayer(clients[1].getID()).hasPlayedACard);
		assertEquals("pickTargetDisplayCard", clients[1].getLastMessage());	
		
		clients[1].send("targetDisplayCard Peter 0");
		
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		assertFalse(server.game.getPlayer(clients[1].getID()).hasPlayedACard);
		assertEquals(3, server.game.getPlayer(clients[0].getID()).getScore());
	}
	
	//Test playing Riposte when the target has 2 or more cards in their display (can play)
	@Test
	public void playRiposte2() {
		System.out.println("@Test: playRiposte2");	
		clients[0].send("set P");
		server.sendGameState();
		
		server.game.getPlayer(clients[0].getID()).addToHand(new Card("Purple", 4));
		clients[0].send("play Purple 4");
		server.game.getPlayer(clients[0].getID()).addToHand(new Card("Squire", 3));
		clients[0].send("play Squire 3");
		clients[0].send("end turn");
		
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		server.game.getPlayer(clients[1].getID()).addToHand(new Card("Riposte", 0));
		clients[1].send("play Riposte 0");
							
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
								
		assertEquals(7, server.game.getPlayer(clients[0].getID()).getScore());
		assertFalse(server.game.getPlayer(clients[1].getID()).hasPlayedACard);
		assertEquals("pickTargetDisplayCard", clients[1].getLastMessage());	
		
		clients[1].send("targetDisplayCard Peter 0");
		
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
			
		assertTrue(server.game.getPlayer(clients[1].getID()).hasPlayedACard);
		assertEquals(4, server.game.getPlayer(clients[0].getID()).getScore());
		assertEquals(3, server.game.getPlayer(clients[1].getID()).getScore());
	}
	
	//Test playing Dodge when the target has less then 2 cards in their display (can't play)
	@Test
	public void playDodge1() {
		System.out.println("@Test: playDodge1");	
		clients[0].send("set P");
		server.sendGameState();
		
		//server.game.getPlayer(clients[0].getID()).addToHand(new Card("Purple", 4));
		//clients[0].send("play Purple 4");
		server.game.getPlayer(clients[0].getID()).addToHand(new Card("Squire", 3));
		clients[0].send("play Squire 3");
		clients[0].send("end turn");
			
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
			
		server.game.getPlayer(clients[1].getID()).addToHand(new Card("Dodge", 0));
		clients[1].send("play Dodge 0");
								
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
									
		assertEquals(3, server.game.getPlayer(clients[0].getID()).getScore());
		assertFalse(server.game.getPlayer(clients[1].getID()).hasPlayedACard);
		assertEquals("pickTargetDisplayCard", clients[1].getLastMessage());	
			
		clients[1].send("targetDisplayCard Peter 0");
		
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
				
		assertFalse(server.game.getPlayer(clients[1].getID()).hasPlayedACard);
		assertEquals(3, server.game.getPlayer(clients[0].getID()).getScore());
	}
	
	//Test playing Dodge when the target has 2 or more cards in their display (can play)
	@Test
	public void playDodge2() {
		System.out.println("@Test: playDodge2");	
		clients[0].send("set P");
		server.sendGameState();
			
		server.game.getPlayer(clients[0].getID()).addToHand(new Card("Purple", 4));
		clients[0].send("play Purple 4");
		server.game.getPlayer(clients[0].getID()).addToHand(new Card("Squire", 3));
		clients[0].send("play Squire 3");
		clients[0].send("end turn");
				
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
				
		server.game.getPlayer(clients[1].getID()).addToHand(new Card("Dodge", 0));
		clients[1].send("play Dodge 0");
								
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
										
		assertEquals(7, server.game.getPlayer(clients[0].getID()).getScore());
		assertFalse(server.game.getPlayer(clients[1].getID()).hasPlayedACard);
		assertEquals("pickTargetDisplayCard", clients[1].getLastMessage());	
				
		clients[1].send("targetDisplayCard Peter 0");
			
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
					
		assertTrue(server.game.getPlayer(clients[1].getID()).hasPlayedACard);
		assertEquals(3, server.game.getPlayer(clients[0].getID()).getScore());
	}
	
	//Test playing Retreat when the target has less then 2 cards in their display (can't play)
	@Test
	public void playRetreat1() {
		System.out.println("@Test: playRetreat1");	
		clients[0].send("set P");
		server.sendGameState();
			
		server.game.getPlayer(clients[0].getID()).addToHand(new Card("Purple", 4));
		clients[0].send("play Purple 4");
		server.game.getPlayer(clients[0].getID()).addToHand(new Card("Retreat", 0));
		clients[0].send("play Retreat 0");
								
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
										
		assertEquals(4, server.game.getPlayer(clients[0].getID()).getScore());
		assertEquals("pickOwnDisplayCard", clients[0].getLastMessage());	
					
		clients[0].send("ownDisplayCard Peter 1");
				
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
						
		assertEquals(4, server.game.getPlayer(clients[0].getID()).getScore());
	}
	
	//Test playing Knock Down when the target's hand is empty (can't play)
	@Test
	public void playKnockDown1() {
		System.out.println("@Test: playKnockDown1");	
		clients[0].send("set P");
		server.sendGameState();
		
		server.game.getPlayer(clients[0].getID()).getHand().clear();
		server.game.getPlayer(clients[1].getID()).getHand().clear();
		
		server.game.getPlayer(clients[0].getID()).addToHand(new Card("KnockDown", 0));
		clients[0].send("play KnockDown 0");
									
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
										
		assertEquals("pickTargetHandCard", clients[0].getLastMessage());	
					
		clients[0].send("targetHandCard Jane 0");
				
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
						
		assertFalse(server.game.getPlayer(clients[0].getID()).hasPlayedACard);
		assertEquals(1, server.game.getPlayer(clients[0].getID()).getHandSize());
	}
	
	//Test playing Knock Down when the target's hand is not empty (can play)
	@Test
	public void playKnockDown2() {
		System.out.println("@Test: playKnockDown2");	
		clients[0].send("set P");
		server.sendGameState();
		
		server.game.getPlayer(clients[0].getID()).getHand().clear();
		server.game.getPlayer(clients[1].getID()).getHand().clear();
		
		server.game.getPlayer(clients[1].getID()).addToHand(new Card("Purple", 4));
		server.game.getPlayer(clients[1].getID()).addToHand(new Card("Squire", 3));
		server.game.getPlayer(clients[0].getID()).addToHand(new Card("KnockDown", 0));
		clients[0].send("play KnockDown 0");
									
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
											
		assertEquals("pickTargetHandCard", clients[0].getLastMessage());	
					
		clients[0].send("targetHandCard Jane 0");
				
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
						
		assertTrue(server.game.getPlayer(clients[0].getID()).hasPlayedACard);
		assertEquals(1, server.game.getPlayer(clients[0].getID()).getHandSize());
	}
	
	//Test withdrawing with a maiden but no tokens (don't return token)
	
	@Test
	public void Withdraw1() {
		System.out.println("@Test: Withdraw1");	
		clients[0].send("set P");
	//	server.sendGameState();
		
		server.game.getPlayer(clients[0].getID()).getHand().clear();
		server.game.getPlayer(clients[1].getID()).getHand().clear();
		
		server.game.getPlayer(clients[1].getID()).addToHand(new Card("Purple", 4));
		
		server.game.getPlayer(clients[0].getID()).addToHand(new Card("Purple", 4));
		clients[0].send("play Purple 4");
		server.game.getPlayer(clients[0].getID()).addToHand(new Card("Maiden", 6));
		clients[0].send("play Maiden 6");
		
		clients[0].send("withdraw");
									
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		// NOT WORKING, CAN'T FIGURE OUT WHY.
		// was bug in manual withdraw, fixed
		assertFalse(server.game.getPlayer(clients[0].getID()).playing);
		assertTrue(server.game.getPlayer(clients[0].getID()).getDisplay().isEmpty());
	
	}
	
	@Test
	public void purpleTournament(){
		clients[0].send("set P");
		server.sendGameState();
		server.game.getPlayer(clients[0].getID()).getHand().clear();
		server.game.getPlayer(clients[1].getID()).getHand().clear();
		
		server.game.getPlayer(clients[1].getID()).addToHand(new Card("Purple", 4));
		server.game.getPlayer(clients[1].getID()).addToHand(new Card("Purple", 5));
		
		server.game.getPlayer(clients[0].getID()).addToHand(new Card("Purple", 4));
		server.game.getPlayer(clients[0].getID()).addToHand(new Card("Maiden", 6));
	
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[0].send("play purple 4");
		clients[0].send("end");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[1].send("play purple 5");
		clients[1].send("end");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[0].send("play maiden 6");
		clients[0].send("end");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[1].send("withdraw");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[0].send("choose g");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		assertFalse(server.game.getPlayer(clients[0].getID()).getTokens().isEmpty());
		assertEquals(server.game.getPlayer(clients[0].getID()).getTokens().toString(), "[g]");
		
	}
	
	@Test
	public void redTournament(){
		clients[0].send("set R");
		server.sendGameState();
		server.game.getPlayer(clients[0].getID()).getHand().clear();
		server.game.getPlayer(clients[1].getID()).getHand().clear();
		
		server.game.getPlayer(clients[1].getID()).addToHand(new Card("Red", 4));
		server.game.getPlayer(clients[1].getID()).addToHand(new Card("Red", 5));
		
		server.game.getPlayer(clients[0].getID()).addToHand(new Card("Red", 4));
		server.game.getPlayer(clients[0].getID()).addToHand(new Card("Maiden", 6));
	
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[0].send("play Red 4");
		clients[0].send("end");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[1].send("play Red 5");
		clients[1].send("end");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[0].send("play maiden 6");
		clients[0].send("end");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[1].send("withdraw");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}

		assertFalse(server.game.getPlayer(clients[0].getID()).getTokens().isEmpty());
		assertEquals(server.game.getPlayer(clients[0].getID()).getTokens().toString(), "[R]");
		
	}
	
	@Test
	public void YellowTournament(){
		clients[0].send("set Y");
		server.sendGameState();
		server.game.getPlayer(clients[0].getID()).getHand().clear();
		server.game.getPlayer(clients[1].getID()).getHand().clear();
		
		server.game.getPlayer(clients[1].getID()).addToHand(new Card("Yellow", 4));
		server.game.getPlayer(clients[1].getID()).addToHand(new Card("Yellow", 5));
		
		server.game.getPlayer(clients[0].getID()).addToHand(new Card("Yellow", 4));
		server.game.getPlayer(clients[0].getID()).addToHand(new Card("Maiden", 6));
	
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[0].send("play Yellow 4");
		clients[0].send("end");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[1].send("play Yellow 5");
		clients[1].send("end");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[0].send("play maiden 6");
		clients[0].send("end");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[1].send("withdraw");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}

		assertFalse(server.game.getPlayer(clients[0].getID()).getTokens().isEmpty());
		assertEquals(server.game.getPlayer(clients[0].getID()).getTokens().toString(), "[Y]");
		
	}
	@Test
	public void BlueTournament(){
		clients[0].send("set B");
		server.sendGameState();
		server.game.getPlayer(clients[0].getID()).getHand().clear();
		server.game.getPlayer(clients[1].getID()).getHand().clear();
		
		server.game.getPlayer(clients[1].getID()).addToHand(new Card("Blue", 4));
		server.game.getPlayer(clients[1].getID()).addToHand(new Card("Blue", 5));
		
		server.game.getPlayer(clients[0].getID()).addToHand(new Card("Blue", 4));
		server.game.getPlayer(clients[0].getID()).addToHand(new Card("Maiden", 6));
	
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[0].send("play Blue 4");
		clients[0].send("end");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[1].send("play Blue 5");
		clients[1].send("end");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[0].send("play maiden 6");
		clients[0].send("end");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[1].send("withdraw");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}

		assertFalse(server.game.getPlayer(clients[0].getID()).getTokens().isEmpty());
		assertEquals(server.game.getPlayer(clients[0].getID()).getTokens().toString(), "[B]");
		
	}
	
	@Test
	public void GreenTournament(){
		clients[0].send("set G");
		server.sendGameState();
		server.game.getPlayer(clients[0].getID()).getHand().clear();
		server.game.getPlayer(clients[1].getID()).getHand().clear();
		
		server.game.getPlayer(clients[1].getID()).addToHand(new Card("Green", 1));
		server.game.getPlayer(clients[1].getID()).addToHand(new Card("Green", 1));
		
		server.game.getPlayer(clients[0].getID()).addToHand(new Card("Green", 1));
		server.game.getPlayer(clients[0].getID()).addToHand(new Card("Maiden", 6));
	
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[0].send("play Green 1");

		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[0].send("end");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[1].send("play Green 1");
		clients[1].send("end");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}

		clients[0].send("play Maiden 6");
		
		
		clients[0].send("end");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		

		clients[1].send("withdraw");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}

		assertFalse(server.game.getPlayer(clients[0].getID()).getTokens().isEmpty());
		assertEquals(server.game.getPlayer(clients[0].getID()).getTokens().toString(), "[G]");
		
	}
	
}
