package ivanhoe;

import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.*;

import java.io.*;

public class Server implements Runnable {
	
	private int maxClients;
	public Game game;
	private Thread thread;
	private ServerSocket server;
	private HashMap<Integer, ServerThread> clients;
	private String lastMessage;
	private String result;
	private String lastCard;
	private String outwitIndex;
	public Logger logger;
	private int numAi;
	
	public Server(){
		maxClients = 5;
		clients = null;
		game = null;
		lastCard = "x x";
		logger = new Logger();
		numAi = 0;
	}
	
	//initialize the server
	
	public void startUp(int portNum){
		try{
			server = new ServerSocket(portNum);
			//server.
			server.setReuseAddress(true);
			clients = new HashMap<Integer, ServerThread>();
//			System.out.println(server.getInetAddress().getHostAddress());
		} catch (IOException ioe){
			
		}
		start();
	}
	
	//initialize main server thread if non-existent
	public void start(){
		if(thread == null){
			thread = new Thread(this);
			thread.start();
		}
	}
	
	//listen for socket connections 
	public void run(){
		while(thread != null){
			try{
				addThread(server.accept());		
			}catch(IOException ioe){
				
			}
		}
	}
	
	//accepts a client connection and spawns a serverthread to listen for messages from the client
	
	public void addThread(Socket newClient) {
		if(clients.size() < maxClients){
			ServerThread serverThread = new ServerThread(this, newClient);
			serverThread.open();
			serverThread.start();
			clients.put(serverThread.getID(), serverThread);
			serverThread.send("Connection Success ");
			if(game == null){
				serverThread.send(" f \n");
			}else{
				serverThread.send(" t ");
				for(ServerThread to: clients.values()){
					if(game.getPlayer(to.getID()) != null){
						serverThread.send(game.getPlayer(to.getID()).getName()+ " ");
					}
				}
				serverThread.send("\n");			}
		}
		
	}
	//handle messages from client, interface with games class
	
	public synchronized void handle(int ID, String message){
		logger.write("  From Client: " + ID + " " + message);
		lastMessage = message;
		System.out.println(ID + lastMessage);
		String parts[] = message.split(" ");

		
		//create game: create (a number)
		if(parts[0].equals("create")){

			if(game == null){
				game = new Game(Integer.valueOf(parts[1]));
				update(ID, "game created");
			}else{
				update(ID, "game in progress");
			}
		}else if(parts[0].equals("join")){
			//join game: join (a name)
			if(game.state == 1){
				if(game.addPlayer(ID, parts[1])){
					update(ID, "joined");
				}else{
					update(ID, "max players");
				}
			}else{
				update(ID, "game in progress");
			}
				
		}else if(parts[0].equals("AI")){
			//join game: join (a name)
			if(game.state == 1){
				numAi++;
				if(game.addPlayer(numAi, parts[1])){
					System.out.println("@");
					if(parts[2].equals("max")){
						System.out.println("adding max AI");
						clients.put(numAi, new ServerThread(numAi,new MaxAi(game.getPlayer(numAi),game),this));
						
					}else if(parts[2].equals("min")){
						System.out.println("adding min AI");
						clients.put(numAi, new ServerThread(numAi,new MinAi(game.getPlayer(numAi),game),this));
						
					}
					update(numAi, "AI");
				}else{
					update(ID, "max players");
				}
			}else{
				//update(ID, "game in progress");
			}
				
		}else if(parts[0].equals("start")){
			//start a tournament: start
			if(game.state == 1){
				System.out.println("got here");
				game.beginGame();
				update(ID,"start");
				update(ID, "set");
			}else{
				//do something - ignore command or some shit
			}
		}else if(parts[0].equals("set")){
			//set the tournament color: set (color string)
			if(game.checkCurrPlayer(ID)){
				game.getPlayer(ID).addToHand(game.drawCard());
				game.setColour(parts[1]);
				update(ID, "begin turn");
			}else{
				
			}
		}else if(parts[0].equals("play")){
			//play card: play (cardname) (card valueas stirng)
			if(game.getIvanhoePlayer() == ID && parts[1].equals("Ivanhoe")){
				String parts2[] = lastCard.split(" ");
				if(parts2[0] != "Red" && parts2[0] != "Green" && parts2[0] != "Blue" && parts2[0] != "Yellow" 
						&& parts2[0] != "Purple" && parts2[0] != "Squire" && parts2[0] != "Maiden"){
					game.updateState("undo", ID, lastCard);
					sendGameState();
				}
			}else if(game.checkCurrPlayer(ID)){
				result = "";
				result = game.playCard(parts[1]+" "+parts[2]);
				if(result.equalsIgnoreCase("good")){
					update(ID,"card played");
					lastCard = parts[1]+" "+parts[2];
				}else if(result.equalsIgnoreCase("colour")){
					update(ID,"action colour");
					lastCard = parts[1]+" "+parts[2];
				}else if(result.equalsIgnoreCase("target")){
					update(ID,"target");
					lastCard = parts[1]+" "+parts[2];
				}else if(result.equalsIgnoreCase("targetDisplayCard")){
					update(ID,"pickTargetDisplayCard");
					lastCard = parts[1]+" "+parts[2];
				}else if(result.equalsIgnoreCase("ownDisplayCard")){
					update(ID,"pickOwnDisplayCard");
					lastCard = parts[1]+" "+parts[2];
				}else if(result.equalsIgnoreCase("targetHandCard")){
					update(ID,"pickTargetHandCard");
					lastCard = parts[1]+" "+parts[2];
				}else if(result.equalsIgnoreCase("targetDisplayOwnDisplay")){
					update(ID,"pickOwnDisplayCard");
					lastCard = parts[1]+" "+parts[2];
				}else if(result.equalsIgnoreCase("Adapt")){
					update(ID,"Adapt");
					lastCard = parts[1]+" "+parts[2];
				}else if(result.equalsIgnoreCase("Bad")){
					update(ID,"invalid");
				}
			}else{
				update(ID,"not your turn");
			}
		}else if(parts[0].equals("withdraw")){
			//withdraw from tournament: withdraw
			if(game.checkCurrPlayer(ID)){
				
		//		System.out.println(game.checkCurrPlayer(ID));
				if(game.withdraw()){
					game.endTurn();
					update(ID,"lose a token");
				}else{
					game.endTurn();
					if(game.state == 2){
						System.out.println("3");
						update(ID,"begin turn");
					}else if (game.state == 3){
						if(game.endTournament()){
							update(ID,"choose");
						}else{
							game.beginTournament();
							update(ID,"set");
						}
					}
				}
			}
		}else if(parts[0].equals("end")){
			//end a turn: end
			if(game.endTurn()){
				update(ID,"withdraw");
				if(game.withdraw()){
					update(ID,"lose a token");
				}else{
					if(game.state == 2){
						update(ID,"begin turn");
					}else if (game.state == 3){
						
						if(game.endTournament()){
							if(game.hasWon()){
								update(ID,"win");
							}
							update(ID,"choose");
						}else{
							if(game.hasWon()){
								update(ID,"win");
							}
							game.beginTournament();
							update(ID,"set");
						}
					}
				}
			}else{
				update(ID,"begin turn");
			}
		}else if(parts[0].equals("lose")){
			//lose a token: lose (token color as char)
			if(game.removeToken(ID, parts[1].charAt(0))){
				if(game.state == 2){
					update(ID,"begin turn");
				}else if (game.state == 3){
					for(ServerThread to: clients.values()){
						if(game.hasWon()){
							update(to.getID(),"win");
						}
					}
					if(game.endTournament()){
						update(ID,"choose");
					}else{
						game.beginTournament();
						update(ID,"set");
					}
				}
			}else{
				update(ID,"lose a token");
			}
			
		}else if(parts[0].equals("choose")){
			//gain a token of choice (token color as char)
			game.getPlayer(ID).addToken(parts[1].charAt(0));
			if(game.hasWon()){
				update(ID,"win");
			}
			game.beginTournament();
			update(ID,"set");
		}else if(parts[0].equals("add")){
			//manually add a card of choice to hand: add (card name) (card value as string)
			game.getPlayer(ID).addToHand(new Card(parts[1],Integer.parseInt(parts[2])));
			sendGameState();
		}else if(parts[0].equals("ownDisplayCard") && lastCard.equals("Outwit 0")){
			//called on retreat: ownDisplayCard (player name) (index # of card)
			//game.playAgainstTarget(lastCard, Integer.toString(ID), parts[2]);
			outwitIndex = parts[2];	
			update(ID,"pickTargetDisplayCard");
			sendGameState();
		}else if(parts[0].equals("ownDisplayCard") && lastCard.equals("Adapt 0")){
			//called on retreat: ownDisplayCard (player name) (index # of card)
			//game.playAgainstTarget(lastCard, Integer.toString(ID), parts[2]);
			Card c = game.getPlayer(ID).getDisplay().get(Integer.parseInt(parts[2]));
			game.removeExcept(ID, c.getName()+ " " + c.getValue());	
			update(ID,"Adapt2");
			sendGameState();
		}else if(parts[0].equals("targetDisplayCard") && lastCard.equals("Outwit 0")){
			//called on dodge/riposte/breaklance: targetDisplayCard (player name) (index # of card (only relevant for dodge but must have placeholder))
			for(ServerThread player: clients.values()){
				if(game.getPlayer(player.getID()).getName().equals(parts[1])){
					game.playAgainstTarget(lastCard, Integer.toString(player.getID()), outwitIndex, parts[2]);
				}
			}
			sendGameState();
		}else if(parts[0].equals("targetDisplayCard")){
			//called on dodge/riposte/breaklance: targetDisplayCard (player name) (index # of card (only relevant for dodge but must have placeholder))
			for(ServerThread player: clients.values()){
				if(game.getPlayer(player.getID()).getName().equals(parts[1])){
					game.playAgainstTarget(lastCard, Integer.toString(player.getID()), parts[2]);
				}
			}
			sendGameState();
		}else if(parts[0].equals("ownDisplayCard")){
			//called on retreat: ownDisplayCard (player name) (index # of card)
			game.playAgainstTarget(lastCard, Integer.toString(ID), parts[2]);
				
			sendGameState();
		}else if(parts[0].equals("targetHandCard")){
			//called on knockdown: targetHandCard (player name) (index # of card)
			for(ServerThread player: clients.values()){
				if(game.getPlayer(player.getID()).getName().equals(parts[1])){
					game.playAgainstTarget(lastCard, Integer.toString(player.getID()), parts[2]);
				}
			}	
			sendGameState();
		}
		else if(parts[0].equals("disconnect")){
				//clients.get(ID).interrupt();
				game.removePlayer(ID);

				clients.remove(ID);
				
				for(ServerThread to: clients.values()){
					System.out.println(ID);
					if(game.getTotalPlayers() == 1){
						update(to.getID(),"win");
					}else{
						to.send("redraw\n");
					}
				}
				
				if(game.getTotalPlayers() != 1){
					sendGameState();
				}
				
		}
	}
		
	public void update(int portID, String message){
		String parts[] = lastMessage.split(" "); 
		if(message == "game created"){
			for(ServerThread to: clients.values()){
				if(portID == to.getID()){
					to.send("game created\n");
				}else{
					//to.send("A game has been created.  Type join (name) to join\n");
				}
			}
		}else if(message == "game in progress"){
			clients.get(portID).send("A game is already in progress. Type join (name) to join\n");
		}else if (message == "joined"){
			for(ServerThread to: clients.values()){
				if(portID == to.getID()){
					to.send("You\n");
				}else{
					to.send(parts[1]+ "\n");
				}
			}
		}else if (message == "AI"){
			for(ServerThread to: clients.values()){
				to.send(parts[1]+"\n");
			}
		}else if (message == "set"){
			sendGameState();
			for(ServerThread to: clients.values()){
				if(game.checkCurrPlayer(to.getID()) && (game.state == 3 || game.state == 1)){
					to.send("set colour\n");
				}else{
					//to.send("Please wait, "+game.getPlayer(portID).getName()+" is picking the colour\n");
				}
			}
			//prompt player who's turn it is to choose a colour
			//message all other players: wait while player X chooses tournament color
		}else if(message=="start"){
			for(ServerThread to: clients.values()){
				to.send("start\n");
			}
			sendGameState();
		}else if (message == "begin turn"){
			for(ServerThread to: clients.values()){
				if(game.checkCurrPlayer(to.getID())){
					if(to.getID() < 5){
						clients.get(to.getID()).send("play");
					}
				}
			}
			sendGameState();
		}else if (message == "card played"){
			sendGameState();
			for(ServerThread to: clients.values()){
				if(game.checkCurrPlayer(to.getID())){	
				//	to.send("It is still your turn.  Play a card, withdraw from tournament or end game\n");
				}else{
			//		to.send("Please wait it is "+game.getPlayer(portID).getName()+"'s turn\n");
				}
			}
			//message all players initial hand/display info
			//message player who'se turn it is: play a card, withdraw or end turn
			//message all other players :waiting for other player
		}else if (message == "not your turn"){
			//clients.get(portID).send("It is not your turn");
			//message only player attempting to take turn: not your turn
		}else if(message == "withdraw"){
			
		}else if (message == "lose a token"){
			clients.get(portID).send("lose colour\n");
		}else if (message == "choose"){
			
			for(ServerThread to: clients.values()){
				if(game.checkCurrPlayer(to.getID())){
					to.send("choose colour\n");
				}
			}
		}else if (message == "action colour"){
			
			for(ServerThread to: clients.values()){
				if(game.checkCurrPlayer(to.getID())){
					to.send("action colour\n");
				}
			}
		}else if (message == "invalid"){
		//	clients.get(portID).send("bad");
		}else if (message == "target"){
				clients.get(portID).send("pickTargetDisplayCard\n");
		}else if (message == "pickTargetDisplayCard"){
			clients.get(portID).send("pickTargetDisplayCard\n");
		}else if (message == "pickOwnDisplayCard"){
			clients.get(portID).send("pickOwnDisplayCard\n");
		}else if (message == "target"){
			clients.get(portID).send("pickTargetDisplayCard\n");
		}else if (message == "pickTargetHandCard"){
			clients.get(portID).send("pickTargetHandCard\n");
		}else if (message == "Adapt"){
			boolean done = true;
			for(ServerThread to: clients.values()){
				ArrayList<Integer> result = new ArrayList<Integer>();
				result = game.findMultipleValueCards(to.getID());
				if(!result.isEmpty()){
					to.send("Adapt ");
					for(int i: result){
						to.send(i + " ");
					}
					to.send("\n");
					done = false;
				}
			}
			if(done == true){
			}
		}else if (message == "Adapt2"){
			boolean done = true;
			ArrayList<Integer> result = new ArrayList<Integer>();
			result = game.findMultipleValueCards(portID);
				if(!result.isEmpty()){
					clients.get(portID).send("Adapt ");
					for(int i: result){
						clients.get(portID).send(i + " ");
					}
					clients.get(portID).send("\n");
					done = false;
				}
			
			if(done == true){
			}
		}else if (message == "win"){
			for(ServerThread to: clients.values()){
				if(game.checkCurrPlayer(to.getID())){
					to.send("win you\n");
				}else{
					to.send("win " + clients.get(to.getID()).getName() + "\n");
				}
			}
		}
		
	}
	public void sendGameState(){
		/*
		 * preserving old code
		for(ServerThread to: clients.values()){
			for(ServerThread others: clients.values()){
				if(to.getID() != others.getID() && game.getPlayer(others.getID()).playing){
					to.send(game.getPlayer(others.getID()).getName() + "'s hand : " + game.getPlayer(others.getID()).getHandSize() + " cards\n");
					to.send(game.getPlayer(others.getID()).getName() + "'s display : ");
					for(Card display: game.getPlayer(others.getID()).getDisplay()){
						to.send(" " + display.getName() + " " + display.getValue() + ",");
					}
						to.send("\n" + game.getPlayer(others.getID()).getName() + "'s score: " + game.getPlayer(others.getID()).getScore() + "\n");
				}else if(game.getPlayer(to.getID()).playing){
					to.send("Your hand:");
					for(Card hand: game.getPlayer(to.getID()).getHand()){
						to.send(" " + hand.getName() + " " + hand.getValue() + ",");
					}
					to.send("\n");
				}
			}
		}
		*/
		
		for(ServerThread to: clients.values()){
			if(to.getID() > 5){
				to.send(game.getTotalPlayers() + ":");
				to.send(game.getColour() + ":");
				for(ServerThread turn: clients.values()){
					if(game.checkCurrPlayer(turn.getID())){
						if(turn.getID() == to.getID()){
							to.send("you:");
						}else{
							to.send(game.getPlayer(turn.getID()).getName()+":");
						}
					}
					
				}
				for(ServerThread others: clients.values()){
					if(to.getID() != others.getID()){
						to.send(game.getPlayer(others.getID()).getName() + "," + game.getPlayer(others.getID()).getHandSize() + ",");
						to.send(".");
						for(Card display: game.getPlayer(others.getID()).getDisplay()){
							to.send(display.getName() + display.getValue() + ".");
						}
						to.send("," + game.getPlayer(others.getID()).getScore());
					}else{
						to.send("you,");
						to.send(".");
						for(Card hand: game.getPlayer(to.getID()).getHand()){
							to.send(hand.getName() + hand.getValue() + ".");
						}
						to.send(",");
						to.send(".");
						for(Card display: game.getPlayer(to.getID()).getDisplay()){
							to.send(display.getName() + display.getValue() + ".");
						}
						to.send("," + game.getPlayer(to.getID()).getScore());
					}
					to.send(",.");
					for(Character c : game.getPlayer(others.getID()).getTokens()){
						to.send(c + ".");
					}
					to.send(",");
					if(game.getPlayer(others.getID()).playing){
						to.send("t");
					}else{
						to.send("f");
					}
					to.send(",");
					if(game.getPlayer(others.getID()).getShield()){
						to.send("t");
					}else{
						to.send("f");
					}
					to.send(".");
					if(game.getPlayer(others.getID()).getStunned()){
						to.send("t");
					}else{
						to.send("f");
					}
					to.send(":");
				}
				to.send("\n");
			}
		}
	}
	public String getLastMessage(){
		return lastMessage;
	}
	
	public void close(){
		try{
			server.close();
		}catch(IOException ioe){
			
		}
	}
	
}
