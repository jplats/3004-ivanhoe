package ivanhoe;

import java.awt.*;
import javax.swing.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.Point;

public class Display {

    private final JPanel gui = new JPanel(new BorderLayout(3, 3));
 //   private JButton[][] boardSquares = new JButton[20][20];
    private JLayeredPane board;
    private GroupLayout hand;

    Display() {
        initializeDisplay();
    }

    public final void initializeDisplay() {
        // set up the main GUI
        gui.setBorder(new EmptyBorder(5, 5, 5, 5));
        JToolBar tools = new JToolBar();
        tools.setFloatable(false);
        gui.add(tools, BorderLayout.PAGE_START);
        JButton b1 = new JButton("Pool");
        b1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
               
            }
        });  
        tools.add(b1); 
 		tools.addSeparator();
        board = new JLayeredPane();
        board.setPreferredSize(new Dimension(610,150));
        board.setBorder(BorderFactory.createTitledBorder("Your Hand"));
        gui.add(board);

      Icon icon = new ImageIcon("src/ivanhoe/images/card2.jpeg");
      JButton card = new JButton(icon);
      card.setBounds(15,15,icon.getIconWidth(),icon.getIconHeight()); 
      board.add(card,2);
      icon = new ImageIcon("src/ivanhoe/images/card3.jpeg");
      card = new JButton(icon);
      card.setBounds(35,15,icon.getIconWidth(),icon.getIconHeight());
      board.add(card,1);

    }

    public final JComponent getBoard() {
        return board;
    }

    public final JComponent getGui() {
        return gui;
    }
    public void draw(){


        JFrame f = new JFrame("Ivanhoe");
        f.add(this.getGui());
        f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        f.setLocationByPlatform(true);

        // ensures the frame is the minimum size it needs to be
        // in order display the components within it
        f.pack();
        // ensures the minimum size is enforced.
        f.setMinimumSize(f.getSize());
        f.setVisible(true);
    }
    public static void main(String[] args) {
       
            Display cb = new Display();
        	cb.draw();
  

    }
}