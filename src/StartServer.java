package ivanhoe;

import java.io.Console;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.Scanner;

public class StartServer {
	private static Boolean done = Boolean.FALSE;
	private static Boolean started = Boolean.FALSE;

	private static Scanner sc = new Scanner(System.in);
	private static Server appServer = null;
	
	public static void main(String[] argv) throws UnknownHostException {
	
		Console c = System.console();
		if (c == null) {
			System.err.println("No System Console....");
			System.err.println("Use IDE Console....");
		}
		
		do {
			String input = sc.nextLine();
			
			if (input.equalsIgnoreCase("START") && !started)
			{
				System.out.println("Starting server on address ...");

				System.out.println(Inet4Address.getLocalHost().getHostAddress());
				
				appServer = new Server();
				
				appServer.startUp(40000);
				
				started = Boolean.TRUE;
			}
			
			if (input.equalsIgnoreCase("SHUTDOWN") && started)
			{
				System.out.println("Shutting server down ...");

			
				started = Boolean.FALSE;
				done = Boolean.TRUE;
			}			
		} while (!done);

		System.exit(1);
	}
}