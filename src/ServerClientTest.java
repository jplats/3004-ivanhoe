package ivanhoe;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ServerClientTest {

	Client clients[];
	Server server;
	
	@Before
	public void setUp(){
		server = new Server();
		server.startUp(10000);
	}
	
	@After
	public void tearDown(){
		server.close();
	}
	/*
	@Test
	public void testConnect() {
		clients = new Client[1];
		clients[0] = new Client();
		assertEquals(true, clients[0].connect("0.0.0.0", 10000));
	}

	
	@Test
	public void testMultipleConnects() {
		clients = new Client[2];
		clients[0] = new Client();
		clients[1] = new Client();
		assertEquals(true, clients[0].connect("0.0.0.0", 10000));
		assertEquals(true, clients[1].connect("0.0.0.0", 10000));	
	}

	@Test
	public void testTwoConnectsAndServerResponse() {
		clients = new Client[2];
		clients[0] = new Client();
		clients[1] = new Client();
		assertEquals(true, clients[0].connect("0.0.0.0", 10000));
		
		try{
			Thread.sleep(2000);
		}catch(InterruptedException ie){
			
		}
		assertEquals(true, clients[1].connect("0.0.0.0", 10000));	
		
		try{
			Thread.sleep(2000);
		}catch(InterruptedException ie){
			
		}
		
		assertEquals("Connection Success", clients[0].getLastMessage());
		assertEquals("Connection Success", clients[1].getLastMessage());
	}
	
	@Test
	public void testThreeConnectsAndServerResponse() {
		clients = new Client[3];
		clients[0] = new Client();
		clients[1] = new Client();
		clients[2] = new Client();
		assertEquals(true, clients[0].connect("0.0.0.0", 10000));
		
		try{
			Thread.sleep(2000);
		}catch(InterruptedException ie){
			
		}
		assertEquals(true, clients[1].connect("0.0.0.0", 10000));	
		
		try{
			Thread.sleep(2000);
		}catch(InterruptedException ie){
			
		}
		assertEquals(true, clients[2].connect("0.0.0.0", 10000));	
		
		try{
			Thread.sleep(2000);
		}catch(InterruptedException ie){
			
		}
		assertEquals("Connection Success", clients[0].getLastMessage());
		assertEquals("Connection Success", clients[1].getLastMessage());
		assertEquals("Connection Success", clients[2].getLastMessage());
	}
	
	@Test
	public void testTooManyClients() {
		clients = new Client[6];
		clients[0] = new Client();
		clients[1] = new Client();
		clients[2] = new Client();
		clients[3] = new Client();
		clients[4] = new Client();
		clients[5] = new Client();
		
		assertEquals(true, clients[0].connect("0.0.0.0", 10000));
		
		try{
			Thread.sleep(2000);
		}catch(InterruptedException ie){
			
		}
		assertEquals(true, clients[1].connect("0.0.0.0", 10000));	
		
		try{
			Thread.sleep(2000);
		}catch(InterruptedException ie){
			
		}
		assertEquals(true, clients[2].connect("0.0.0.0", 10000));	
		
		try{
			Thread.sleep(2000);
		}catch(InterruptedException ie){
			
		}
		assertEquals(true, clients[3].connect("0.0.0.0", 10000));	
		
		try{
			Thread.sleep(2000);
		}catch(InterruptedException ie){
			
		}
		assertEquals(true, clients[4].connect("0.0.0.0", 10000));	
		
		try{
			Thread.sleep(2000);
		}catch(InterruptedException ie){
			
		}
		assertEquals(true, clients[5].connect("0.0.0.0", 10000));	
		
		try{
			Thread.sleep(2000);
		}catch(InterruptedException ie){
			
		}
		
		assertEquals("Connection Success", clients[0].getLastMessage());
		assertEquals("Connection Success", clients[1].getLastMessage());
		assertEquals("Connection Success", clients[2].getLastMessage());
		assertEquals("Connection Success", clients[3].getLastMessage());
		assertEquals("Connection Success", clients[4].getLastMessage());
		assertEquals(null, clients[5].getLastMessage());
	}
	*/
	/*
	@Test
	
	public void testServerMessageReceive(){
		clients = new Client[2];
		clients[0] = new Client();
		clients[1] = new Client();
		
		clients[0].connect("0.0.0.0", 10000);
		clients[1].connect("0.0.0.0", 10000);
		
		

		clients[0].send("create 2 scott");

		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
			
		}

		
		assertEquals("create 2 scott",server.getLastMessage());

		clients[1].send("join jon");
		
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
			
		}
		assertEquals("join jon",server.getLastMessage());
		
	}
	*/
	/*
	@Test
	public void testCreateJoinGame2Player(){
		clients = new Client[2];
		clients[0] = new Client();
		clients[1] = new Client();
		
		clients[0].connect("0.0.0.0", 10000);
		clients[1].connect("0.0.0.0", 10000);
		
		
		try{
			Thread.sleep(2000);
		}catch(InterruptedException ie){
			
		}
		clients[0].send("create 2 scott");
		try{
			Thread.sleep(2000);
		}catch(InterruptedException ie){
		}
		
		assertEquals("scott", server.game.getPlayer(clients[0].getID()).getName());
		assertEquals(2, server.game.getTotalPlayers());

		clients[1].send("create 2 jon");
		
		try{
			Thread.sleep(2000);
		}catch(InterruptedException ie){
		}
		
		assertEquals("A game is already in progress. Type join (name) to join", clients[1].getLastMessage());
		
		clients[1].send("join jon");
		
		try{
			Thread.sleep(2000);
		}catch(InterruptedException ie){
		}
		System.out.println(clients[1].getLastMessage());
		assertEquals("You have joined the game", clients[1].getLastMessage());
		
		assertEquals("jon", server.game.getPlayer(clients[1].getID()).getName());
		
		assertEquals(false, server.game.addPlayer(10000,"paul"));
		
	}
	*/
	/*
	@Test
	public void testGameBeginning(){
		clients = new Client[2];
		clients[0] = new Client();
		clients[1] = new Client();
		
		clients[0].connect("0.0.0.0", 10000);
		clients[1].connect("0.0.0.0", 10000);
		
		
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
			
		}
		clients[0].send("create 2 scott");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		clients[1].send("join jon");
		
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		clients[0].send("start");
		
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		assertEquals(8, server.game.getPlayer(clients[0].getID()).getHandSize());
		assertEquals(8, server.game.getPlayer(clients[1].getID()).getHandSize());
		assertEquals(0, server.game.getPlayer(clients[0].getID()).getScore());
		assertEquals(0, server.game.getPlayer(clients[1].getID()).getScore());
		assertEquals("Please wait, scott is picking the colour", clients[1].getLastMessage());
		assertEquals("Pick a colour for the tournament (set colour)", clients[0].getLastMessage());
		
		clients[0].send("set green");
		
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		assertEquals("green",server.game.getColour());
		
	}
	*/
}
