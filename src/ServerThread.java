package ivanhoe;

import java.net.*;
import java.io.*;

public class ServerThread extends Thread{
	private int ID = -1;
	private Socket socket;
	public Server server;
	private BufferedReader streamIn;
	private BufferedWriter streamOut;
	private String clientAddress;
	private AI aiPlayer;
	private boolean turnPlayed;
	private boolean done;
	
	public ServerThread (Server server, Socket socket){
		super();
		done = false;
		this.server = server;
		this.socket = socket;
		this.ID = socket.getPort();
		this.clientAddress = socket.getInetAddress().getHostAddress();
	}
	public ServerThread(int numAi, AI a, Server server){
		done = false;
		this.ID = numAi;
		aiPlayer = a;
		aiPlayer.thread = this;
		turnPlayed = false;
		this.server = server;
	}
	
	public void open(){
		try{
			if(ID > 5){
				streamIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				streamOut = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			}else{
				
			}
		}catch(IOException ioe){
			
		}
	}
	//listen for messages from clients and pass on to server
	public void run(){
		while(!done){
			try{
				if(ID > 5){
					server.handle(ID, streamIn.readLine());
				}
			}catch(IOException ioe){
				done = true;
				server.handle(ID, "disconnect");
			}
		}
	}
	//send message from server to client
	public void send(String message){
		try{
			//System.out.println("2 - " + ID + " " +message);
			if(ID > 5){
				server.logger.write(" To Client: " + ID + " " +message);
				streamOut.write(message);
				streamOut.flush();
			}else{
				aiPlayer.handle(message);
			}
		}catch(IOException ioe){
			
		}
	}
	
	public int getID(){
		return ID;
	}

	public String getSocketAddress(){
		return clientAddress;
	}
}
