package ivanhoe;

import java.awt.*;
import javax.swing.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.Point;

public class GUI {

    private JPanel game = new JPanel(new GridBagLayout());
 //   private JButton[][] myHandSquares = new JButton[20][20];
    private JLayeredPane[] hand;
    private JLayeredPane[] display;
    private JLabel[] score;
    private JLabel[] tokens;
    private JList listbox;
    private DefaultListModel listModel;
    private boolean gameExists;
    private boolean firstState;
    private int numPlayers;
    private String[] players;
    private Client client;
    private boolean joined;
    private JFrame frame;
    private JLabel turn;
    private JLabel colour;
    private JLabel lastMessage;
    private JLabel test;
    private String pickCard;
  

    
    GUI() {
        initializeDisplay();
        gameExists = false;
        joined = false;
        firstState = true;
        pickCard = "blank";
    }

    public final void initializeDisplay() {
        // set up the main GUI
        
    
    }
/*
    public final JComponent getmyHand() {
        return myHand;
    }
*/
    public final JComponent getGui() {
        return game;
    }
    public void drawLobby(){
    	
    	String[] parts = client.getLastMessage().split(" ");

    	frame = new JFrame("GameLobby");
        if(parts[3].equals("f")){
        	//no game in progress, pop display window;
        	String input = JOptionPane.showInputDialog(frame, "No game exists, enter a max number of players to create a game");
        	client.send("create " + input);
        	pause();
        }
        	

            
            frame.setTitle( "Game Lobby" );
    		frame.setSize( 300, 300 );
    		frame.setBackground( Color.gray );

    		// Create a panel to hold all other components
    		JPanel topPanel = new JPanel();
    		topPanel.setLayout( new BorderLayout() );
    		frame.getContentPane().add( topPanel );

    	

    		// Create a new listbox control
    		JList listbox = new JList();
    		listModel = new DefaultListModel();
    		listbox.setModel(listModel);
    		if(parts[3].equals("t")){
    			for(int i = 4; i < parts.length ; i++){
    				listModel.addElement(parts[i]);
    			}
    		}
    		topPanel.add( listbox, BorderLayout.CENTER );
    		JToolBar tools = new JToolBar();
            tools.setFloatable(false);
            JButton b1 = new JButton("Join Game");
            b1.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                   joinGame();
                }
            });  
            tools.add(b1);
            b1 = new JButton("Add Computer Player");
            b1.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                   addAi();
                }
            });  
            tools.add(b1);
            b1 = new JButton("Start Game");
            b1.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent arg0) {
                   startGame();
                }
            });  
            tools.add(b1); 
            topPanel.add(tools, BorderLayout.NORTH);
    		
    		frame.setVisible(true);

        
    
		
    }
    public void joinGame(){
    	if(joined == false){
    		JFrame frame = new JFrame();
    		String name = JOptionPane.showInputDialog(frame, "Enter Your Name");
    		client.send("join " +name);
    		pause();
    	}else{
    		//do something, cannot join if already in
    	}
    }
    public void addAi(){
    		JFrame frame = new JFrame();
    		Object[] options = {"Aggressive","Conservative"};
    		String name = JOptionPane.showInputDialog(frame, "Enter The Name of the Computer"); //room here for specification of strategy type, or could randomize
    		int response = 0;
    		response = JOptionPane.showOptionDialog(null,"Choose the A.I.'s play style", "Title",JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,null,options,options[0]);
    		if(response == 0){
    			client.send("AI " +name + " max");
    		}else{
    			client.send("AI " + name + " min");
    		}
    		pause();
    }
    public void startGame(){
    	client.send("start");
    	gameExists = true;
    }
    public void drawGame(){
    	
    	String parts[] = client.getLastMessage().split(":");
    	String parts2[] = parts[3].split(",");
    	String parts3[] = parts[4].split(",");
    	numPlayers = Integer.parseInt(parts[0]);
    	players = new String[numPlayers];
    	//current turn: parts[1]
    	
    	//setting up player 1
    	
    	game.setBorder(new EmptyBorder(5, 5, 5, 5));
    	
    	JToolBar tools = new JToolBar();
        tools.setFloatable(false);
        JButton b1 = new JButton("Withdraw");
        b1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
               withdraw();
            }
        });  
        tools.add(b1);
        
        b1 = new JButton("End Turn");
        b1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
               endTurn();
            }
        });  
        tools.add(b1);
        b1 = new JButton("Add Card");
        b1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
               addCard();
            }
        });  
        tools.add(b1);
        
        game.add(tools);
        
    	GridBagConstraints gbc = new GridBagConstraints();
    	display = new JLayeredPane[numPlayers];
    	hand = new JLayeredPane[numPlayers];
    	score = new JLabel[numPlayers];
    	tokens = new JLabel[numPlayers];
    	int x=0;
    	int y=0;
    	int j = 0;
    	for(int i = 0; i < numPlayers; i++){
    		
    		String playerInfo[] = parts[i+3].split(",");
    	
    		if(playerInfo[0].equals("you")){
    			
    			if(numPlayers == 5){
    				y = 9;
    			}else{
    				y=5;
    			}
    			players[i] = playerInfo[0];
		        
    			display[i] = new JLayeredPane();
    	    	display[i].setPreferredSize(new Dimension(900,150));
    	    	display[i].setBorder(BorderFactory.createTitledBorder("Your Display"));
    	    	gbc.gridx = 0;
    	    	gbc.gridy = y;
   	    		//gbc.gridwidth = GridBagConstraints.REMAINDER;
    	    	game.add(display[i],gbc);
    	        y++;
    	        hand[i] = new JLayeredPane();
    	        hand[i].setPreferredSize(new Dimension(900,150));
    	        hand[i].setBorder(BorderFactory.createTitledBorder("Your Hand"));
    	        gbc.gridx = 0;
    	        gbc.gridy = y;
	        	//gbc.gridwidth = GridBagConstraints.REMAINDER;
    	        game.add(hand[i],gbc);
    	        y++;
    	        score[i] = new JLabel("Your score: " + playerInfo[3]);
    	        gbc.gridx = 0;
    	        gbc.gridy = y;
    	        y++;
    	        game.add(score[i],gbc);
    	        
    	        tokens[i] = new JLabel("Tokens: ");
    	        gbc.gridx = 0;
    	        gbc.gridy = y;
    	        game.add(tokens[i],gbc);
    	        y++;
    		}else{
    			if(j == 0){
    				x=0;
    				y=1;
    			}else if(j ==1){
    				x=1;
    				y=1;
    			}else if(j==2){
    				x=1;
    				y=5;
    			}else if(j==3){
    				x=0;
    				y=5;
    			}
    			players[i] = playerInfo[0];
		        
	    		hand[i] = new JLayeredPane();	
	    		hand[i].setPreferredSize(new Dimension(900,150));
	    		hand[i].setBorder(BorderFactory.createTitledBorder(players[i] +"'s Hand"));
	        
	    		gbc.gridx = x;
	    		gbc.gridy = y;
	    //		gbc.gridwidth = GridBagConstraints.REMAINDER;
		        game.add(hand[i], gbc);
		        y++;
		        display[i] = new JLayeredPane();
		        display[i].setPreferredSize(new Dimension(900,150));
		        display[i].setBorder(BorderFactory.createTitledBorder(players[i] +"'s Display"));
		        gbc.gridx = x;
		        gbc.gridy = y;
	        //	gbc.gridwidth = GridBagConstraints.REMAINDER;
		        game.add(display[i], gbc);
		        y++;
		        score[i] = new JLabel(players[i] +"'s score: " + playerInfo[3]);
    	        gbc.gridx = x;
    	        gbc.gridy = y;
    	        y++;
    	        game.add(score[i],gbc);
    	        tokens[i] = new JLabel(players[i] +"'s tokens: ");
    	        gbc.gridx = x;
    	        gbc.gridy = y;
    	        game.add(tokens[i],gbc);
    	        y++;
    	        j++;
    		}
    	}
    	
        //-----------------------------------
        if(numPlayers==5){
        	y=13;
        }else{
        	y=9;
        }
        turn = new JLabel("Turn");
        gbc.gridx = 0;
        gbc.gridy = y;
        game.add(turn, gbc);
        y++;
        colour = new JLabel("Colour");
        gbc.gridx = 0;
        gbc.gridy = y;
        game.add(colour, gbc);
        y++;
        lastMessage = new JLabel("XXX");
        gbc.gridx = 0;
        gbc.gridy = y;
        y++;
       // game.add(lastMessage, gbc);

   /*
    	GridBagConstraints gbc = new GridBagConstraints();
    	
    	game.setBorder(new EmptyBorder(5, 5, 5, 5));
        JToolBar tools = new JToolBar();
        tools.setFloatable(false);
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        game.add(tools,gbc);
        JButton b1 = new JButton("Test");
        b1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
               
            }
        });  
        tools.add(b1); 
 		tools.addSeparator();
 		myDisplay = new JLayeredPane();
 		myDisplay.setPreferredSize(new Dimension(610,150));
        myDisplay.setBorder(BorderFactory.createTitledBorder("Your Display"));
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        game.add(myDisplay,gbc);
        
        myHand = new JLayeredPane();
        myHand.setPreferredSize(new Dimension(610,150));
        myHand.setBorder(BorderFactory.createTitledBorder("Your Hand"));
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        game.add(myHand,gbc);
        
      	Icon icon = new ImageIcon("src/ivanhoe/images/squire2.jpeg");
      	JButton card = new JButton(icon);
      	card.setBounds(15,15,icon.getIconWidth(),icon.getIconHeight()); 
      	myHand.add(card,8);
      	icon = new ImageIcon("src/ivanhoe/images/squire3.jpeg");
      	card = new JButton(icon);
      	card.setBounds(85,15,icon.getIconWidth(),icon.getIconHeight());
      	myHand.add(card,7);
      	icon = new ImageIcon("src/ivanhoe/images/red3.jpeg");
      	card = new JButton(icon);
      	card.setBounds(155,15,icon.getIconWidth(),icon.getIconHeight());
      	myHand.add(card,6);
      	icon = new ImageIcon("src/ivanhoe/images/red4.jpeg");
      	card = new JButton(icon);
      	card.setBounds(225,15,icon.getIconWidth(),icon.getIconHeight());
      	myHand.add(card,5);
      	icon = new ImageIcon("src/ivanhoe/images/squire2.jpeg");
      	card = new JButton(icon);
      	card.setBounds(295,15,icon.getIconWidth(),icon.getIconHeight());
      	myHand.add(card,3);
      	icon = new ImageIcon("src/ivanhoe/images/blue3.jpeg");
      	card = new JButton(icon);
      	card.setBounds(365,15,icon.getIconWidth(),icon.getIconHeight());
      	myHand.add(card,3);
      	icon = new ImageIcon("src/ivanhoe/images/squire2.jpeg");
      	card = new JButton(icon);
      	card.setBounds(435,15,icon.getIconWidth(),icon.getIconHeight());
      	myHand.add(card,2);
      	icon = new ImageIcon("src/ivanhoe/images/squire3.jpeg");
      	card = new JButton(icon);
      	card.setBounds(505,15,icon.getIconWidth(),icon.getIconHeight());
      	myHand.add(card,1);
        hand2 = new JLayeredPane();
        hand2.setPreferredSize(new Dimension(610,150));
        hand2.setBorder(BorderFactory.createTitledBorder("Player 2's Hand"));
        
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        game.add(hand2, gbc);
        
        display2 = new JLayeredPane();
        display2.setPreferredSize(new Dimension(610,150));
        display2.setBorder(BorderFactory.createTitledBorder("Player 2's Display"));
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        game.add(display2, gbc);
        
        for(int i = 1; i<9; i++){
        	icon = new ImageIcon("src/ivanhoe/images/card.jpg");
        	card = new JButton(icon);
        	card.setBounds(15 + (70*(i-1)),15,icon.getIconWidth(),icon.getIconHeight());
        	card.setBounds(15 + (70*(i-1)),15,icon.getIconWidth(),icon.getIconHeight());
        	hand2.add(card,1);
        }
        */
        frame.dispose();
    	frame = new JFrame("Ivanhoe");
        frame.add(this.getGui());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationByPlatform(true);

        // ensures the frame is the minimum size it needs to be
        // in order display the components within it
        frame.pack();
        // ensures the minimum size is enforced.
        frame.setMinimumSize(frame.getSize());
        
        frame.setVisible(true);
        
        updateGame();
		
    }
    public void updateLobby(){
    	if(listModel != null){
    		if(!client.getLastMessage().equals("max")){
    			if(client.getLastMessage().equals("you")) joined = true;
    			listModel.addElement(client.getLastMessage());
    		}else{
    			//max # reached, join no good;
    		}
    	}
    }
    public void update(){
    	System.out.print("XXX : " + client.getLastMessage());
    	String parts[] = client.getLastMessage().split(" ");
    	if(client.getLastMessage().equals("start")){
    		gameExists = true;
    	}else if(gameExists == false){
    		updateLobby();
    	}else if(client.getLastMessage().equals("redraw")){
    		frame.dispose();
    		game = new JPanel(new GridBagLayout());
    		firstState = true;
    	}else if(client.getLastMessage().equals("set colour")){
    		pickColour("set");
    	}else if(client.getLastMessage().equals("choose colour")){
    		pickColour("choose");
    	}else if(client.getLastMessage().equals("lose colour")){
    		pickColour("lose");
    	}else if(client.getLastMessage().equals("action colour")){
    		pickColour("action");
    	}else if(client.getLastMessage().equals("pickTargetDisplayCard")){
    		pickCard = "targetDisplay";
    	}else if(client.getLastMessage().equals("pickOwnDisplayCard")){
    		pickCard = "ownDisplay";
    	}else if(client.getLastMessage().equals("pickTargetHandCard")){
    		pickCard = "targetHand";
    	}else if(parts[0].equals("Adapt")){
    		pickCard = "ownDisplay";
    		adapt();
    	}else if(parts[0].equals("win")){
    		gameOver(parts[1]);
    	}else if(gameExists == true && firstState == true){
    		firstState = false;
    		drawGame();
    	}else if(gameExists == true && !client.getLastMessage().equals("play")){
    		updateGame();
    	}
    }
    private void endTurn(){
    	if(turn.getText().equals("Players turn: you")){
    		client.send("end");
    	}
    }
    private void gameOver(String s){
    	JOptionPane.showMessageDialog(null, s + " won the game");
    }
    public void pickColour(String s){
    
    	Object[] options = {"red", "yellow","blue","green","purple"};
    	Object[] options2 = {"red", "yellow","blue"};
    	int response=0;
    	if(s.equals("set")){
    			response = JOptionPane.showOptionDialog(null,"Set tournament colour", "Title",JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,null,options,options[0]);
    	}else if(s.equals("choose")){
				response = JOptionPane.showOptionDialog(null,"Pick a colour token to gain", "Title",JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,null,options,options[0]);
    	}else if(s.equals("action")){
				response = JOptionPane.showOptionDialog(null,"Change tournament colour to:", "Title",JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,null,options2,options2[0]);
				s = "set";
    	}else if(s.equals("lose")){
    		/*
    			options = null;
    			for(int i = 0; i < players.length; i++){
    				if(players[i].equals("you")){
    					String text = tokens[i].getText();
    					String[] parts = text.split(" ");
    					if(parts.length == 2){
    						Object[] optionsLose = {parts[1]};
    						response = JOptionPane.showOptionDialog(null,"Pick a colour token to lose", "Title",JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,null,optionsLose,optionsLose[0]);
    					}else if(parts.length == 3){
    						Object[] optionsLose = {parts[1], parts[2]};
    						response = JOptionPane.showOptionDialog(null,"Pick a colour token to lose", "Title",JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,null,optionsLose,optionsLose[0]);
    					}else if(parts.length == 4){
    						Object[] optionsLose = {parts[1], parts[2], parts[3]};
    						response = JOptionPane.showOptionDialog(null,"Pick a colour token to lose", "Title",JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,null,optionsLose,optionsLose[0]);

    					}else if(parts.length == 5){
    						Object[] optionsLose = {parts[1], parts[2], parts[3],parts[4]};
    						response = JOptionPane.showOptionDialog(null,"Pick a colour token to lose", "Title",JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,null,optionsLose,optionsLose[0]);

    					}else{
    						Object[] optionsLose ={};
    					}
    				}else{
    					Object[] optionsLose ={};
    				}
    				*/
				response = JOptionPane.showOptionDialog(null,"Pick a colour token to lose", "Title",JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,null,options,options[0]);

    		

    	}
    	
    	if(response == 0){
    		client.send(s+" R");
    	}else if(response == 1){
    		client.send(s+" Y");
    	}else if(response == 2){
    		client.send(s+" B");
    	}else if(response == 3){
    		client.send(s+" G");
    	}else if(response == 4){
    		client.send(s+" P");
    	}
    }
    public void updateGame(){
    	
    	lastMessage.setText(client.getLastMessage());
    	String parts[] = client.getLastMessage().split(":");
  //  	System.out.println(client.getLastMessage());
    	turn.setText("Players turn: " + parts[2]);
    	colour.setText("Tournament Colour: " + parts[1]);
    	int numCards = 0;
    	ImageIcon icon;
    	JButton card;
    	
    	for(int i = 0; i < players.length; i++){
    		hand[i].removeAll();
    		display[i].removeAll();
    		hand[i].repaint();
    		display[i].repaint();
    	}
    	
    	for(int i = 3; i <(numPlayers+3); i++){
 
    		
    		//System.out.println(i);
    		//System.out.println(" @: "+parts[i]);
    		String playerInfo[] = parts[i].split(",");
    		
    		if(playerInfo[0].equals("you")){
    		//	System.out.println("My hand: " + parts2[1]);
    		//	System.out.println("My display: " + parts2[2]);
    			
    			String aHand[] = playerInfo[1].split("\\.");
    			String aDisplay[] = playerInfo[2].split("\\.");

    			for(int j = 0; j < aHand.length; j++){
    			//	System.out.println(aHand[j]);
    				icon = new ImageIcon("images/"+aHand[j]+".jpeg");
					card = new JButton(icon);
					card.setBounds(15 + (72*(j)),15,icon.getIconWidth(),icon.getIconHeight());
					card.setBounds(15 + (72*(j)),15,icon.getIconWidth(),icon.getIconHeight());
					card.addActionListener(new ActionListener() {
			            public void actionPerformed(ActionEvent arg0) {
			            	cardPushedHand((JButton) arg0.getSource());
			            }
			        });  
					hand[i-3].add(card,1);
    			}
    			if(playerInfo[5].equals("t")){
    				
    			
    				for(int j = 0; j < aDisplay.length; j++){
    					icon = new ImageIcon("images/"+aDisplay[j]+".jpeg");
    					card = new JButton(icon);
						card.setName(playerInfo[0] + " " +(j-1));
    					card.setBounds(15 + (70*(j)),15,icon.getIconWidth(),icon.getIconHeight());
    					card.setBounds(15 + (70*(j)),15,icon.getIconWidth(),icon.getIconHeight());
    					card.addActionListener(new ActionListener() {
			            public void actionPerformed(ActionEvent arg0) {
			            	cardPushedOwnDisplay((JButton) arg0.getSource());
			            }
			        });  
    					display[i-3].add(card,1);
    				}
    				score[i-3].setText("Your score: " + playerInfo[3]);
    			}else if(playerInfo[5].equals("f")){
    				score[i-3].setText("Withdrawn");
    			}
    			String currTokens[] = playerInfo[4].split("\\.");
    			String text = "";
    			for(String t: currTokens){
    				text+=t + " ";
    			}
    			tokens[i-3].setText("Tokens: " + text);
    			
    			String sheildStunned[] = playerInfo[6].split("\\.");
    			
    			if(sheildStunned[0].equals("t") && sheildStunned[1].equals("t")){
    				icon = new ImageIcon("images/Shield0.jpeg");
    				card = new JButton(icon);
    				card.setName(playerInfo[0] + " -1");
    				card.setBounds(830,15,icon.getIconWidth(),icon.getIconHeight());
					card.setBounds(830,15,icon.getIconWidth(),icon.getIconHeight());
					card.addActionListener(new ActionListener() {
			            public void actionPerformed(ActionEvent arg0) {
			            	cardPushedOwnDisplay((JButton) arg0.getSource());
			            }
			        });
					
					display[i-3].add(card,1);
					icon = new ImageIcon("images/Stunned0.jpeg");
    				card = new JButton(icon);
    				card.setName(playerInfo[0] + " -2");
    				card.setBounds(760,15,icon.getIconWidth(),icon.getIconHeight());
					card.setBounds(760,15,icon.getIconWidth(),icon.getIconHeight());
					card.addActionListener(new ActionListener() {
			            public void actionPerformed(ActionEvent arg0) {
			            	cardPushedOwnDisplay((JButton) arg0.getSource());
			            }
			        });
					display[i-3].add(card,1);
					
    			}else if(sheildStunned[0].equals("t")){
    				icon = new ImageIcon("images/Shield0.jpeg");
    				card = new JButton(icon);
    				card.setName(playerInfo[0] + " -1");
    				card.setBounds(830,15,icon.getIconWidth(),icon.getIconHeight());
					card.setBounds(830,15,icon.getIconWidth(),icon.getIconHeight());
					card.addActionListener(new ActionListener() {
			            public void actionPerformed(ActionEvent arg0) {
			            	cardPushedOwnDisplay((JButton) arg0.getSource());
			            }
			        });
					display[i-3].add(card,1);
					
    			}else if(sheildStunned[1].equals("t")){
    				icon = new ImageIcon("images/Stunned0.jpeg");
    				card = new JButton(icon);
    				card.setName(playerInfo[0] + " -2");
    				card.setBounds(830,15,icon.getIconWidth(),icon.getIconHeight());
					card.setBounds(830,15,icon.getIconWidth(),icon.getIconHeight());
					card.addActionListener(new ActionListener() {
			            public void actionPerformed(ActionEvent arg0) {
			            	cardPushedOwnDisplay((JButton) arg0.getSource());
			            }
			        });
					display[i-3].add(card,1);
    			}
    			
    		}else{
    			//test.setText(parts[i]);
    			String aDisplay[] = playerInfo[2].split("\\.");
  
    			numCards = Integer.parseInt(playerInfo[1]);

    			for(int j = 0; j<players.length; j++){
    				if(players[j].equals(playerInfo[0])){
    					for(int k = 0; k<numCards; k++){
    						icon = new ImageIcon("images/card.jpg");
    						card = new JButton(icon);
    						card.setName(playerInfo[0] + " " +(k));
    						card.setBounds(15 + (70*(k)),15,icon.getIconWidth(),icon.getIconHeight());
    						card.setBounds(15 + (70*(k)),15,icon.getIconWidth(),icon.getIconHeight());
    						card.addActionListener(new ActionListener() {
    							public void actionPerformed(ActionEvent arg0) {
    								cardPushedEnemyHand((JButton) arg0.getSource());
    							}
    						}); 
    						hand[j].add(card,1);
    					}
    				
    					for(int k = 0; k < aDisplay.length; k++){
    						
    						icon = new ImageIcon("images/"+aDisplay[k]+".jpeg");
    						card = new JButton(icon);
    						card.setName(playerInfo[0] + " " +(k-1));
    						card.setBounds(15 + (70*(k)),15,icon.getIconWidth(),icon.getIconHeight());
    						card.setBounds(15 + (70*(k)),15,icon.getIconWidth(),icon.getIconHeight());
    						card.addActionListener(new ActionListener() {
    							public void actionPerformed(ActionEvent arg0) {
    								cardPushedEnemyDisplay((JButton) arg0.getSource());
    							}
    						});  
    						display[j].add(card,1);
    					}
    					if(playerInfo[5].equals("f")){
    						score[j].setText("Withdrawn");
    					}else{
    						score[j].setText(players[j] + "'s score: " + playerInfo[3]);
    					}
    					String currTokens[] = playerInfo[4].split("\\.");
    	    			String text = "";
    	    			for(String t: currTokens){
    	    				text+=t;
    	    			}
    	    			tokens[i-3].setText(players[j] +"'s Tokens: " + text);
    	    			
    	    			String sheildStunned[] = playerInfo[6].split("\\.");
    	    			
    	    			if(sheildStunned[0].equals("t") && sheildStunned[1].equals("t")){
    	    				icon = new ImageIcon("images/Shield0.jpeg");
    	    				card = new JButton(icon);
    	    				card.setName(playerInfo[0] + " -1");
    	    				card.setBounds(830,15,icon.getIconWidth(),icon.getIconHeight());
    						card.setBounds(830,15,icon.getIconWidth(),icon.getIconHeight());
    						card.addActionListener(new ActionListener() {
    				            public void actionPerformed(ActionEvent arg0) {
    				            	cardPushedEnemyDisplay((JButton) arg0.getSource());
    				            }
    				        });
    						
    						display[i-3].add(card,1);
    						icon = new ImageIcon("images/Stunned0.jpeg");
    	    				card = new JButton(icon);
    	    				card.setName(playerInfo[0] + " -2");
    	    				card.setBounds(760,15,icon.getIconWidth(),icon.getIconHeight());
    						card.setBounds(760,15,icon.getIconWidth(),icon.getIconHeight());
    						card.addActionListener(new ActionListener() {
    				            public void actionPerformed(ActionEvent arg0) {
    				            	cardPushedEnemyDisplay((JButton) arg0.getSource());
    				            }
    				        });
    						display[i-3].add(card,1);
    						
    	    			}else if(sheildStunned[0].equals("t")){
    	    				icon = new ImageIcon("images/Shield0.jpeg");
    	    				card = new JButton(icon);
    	    				card.setName(playerInfo[0] + " -1");
    	    				card.setBounds(830,15,icon.getIconWidth(),icon.getIconHeight());
    						card.setBounds(830,15,icon.getIconWidth(),icon.getIconHeight());
    						card.addActionListener(new ActionListener() {
    				            public void actionPerformed(ActionEvent arg0) {
    				            	cardPushedEnemyDisplay((JButton) arg0.getSource());
    				            }
    				        });
    						display[i-3].add(card,1);
    						
    	    			}else if(sheildStunned[1].equals("t")){
    	    				icon = new ImageIcon("images/Stunned0.jpeg");
    	    				card = new JButton(icon);
    	    				card.setName(playerInfo[0] + " -2");
    	    				card.setBounds(830,15,icon.getIconWidth(),icon.getIconHeight());
    						card.setBounds(830,15,icon.getIconWidth(),icon.getIconHeight());
    						card.addActionListener(new ActionListener() {
    				            public void actionPerformed(ActionEvent arg0) {
    				            	cardPushedEnemyDisplay((JButton) arg0.getSource());
    				            }
    				        });
    						display[i-3].add(card,1);
    	    			}
    				}
    			}
    		}
    	}
    }
    
    public void adapt(){
    	String message = Arrays.toString(client.getLastMessage().split(" ", 2));
    	JOptionPane.showMessageDialog(frame, "Adapt card has been played, Duplicate values: " + message);
    }
    public void cardPushedHand(JButton j){
    	String parts[] = j.getIcon().toString().split("/");
		String parts2[] = parts[1].split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");
		if(parts2[0].equals("Ivanhoe")){
			client.send("play " + parts2[0] + " " + parts2[1].charAt(0));
		}else if(turn.getText().equals("Players turn: you")){
    		client.send("play " + parts2[0] + " " + parts2[1].charAt(0));
    	}
    	
    }
    public void cardPushedEnemyHand(JButton j){
    	if(pickCard.equals("targetHand")){
    		String parts[] = j.getName().toString().split(" ");
    		client.send("targetHandCard " + parts[0] + " " + parts[1]);
    		pickCard = "";
    	}
    }
    public void cardPushedEnemyDisplay(JButton j){
    	if(pickCard.equals("targetDisplay")){
    		String parts[] = j.getName().toString().split(" ");
    		client.send("targetDisplayCard " + parts[0] + " " + parts[1]);
    		pickCard = "";
    	}
    }
    public void cardPushedOwnDisplay(JButton j){
    	if(pickCard.equals("ownDisplay")){
    		String parts[] = j.getName().toString().split(" ");
    		client.send("ownDisplayCard " + parts[0] + " " + parts[1]);
    		pickCard = "";
    	}
    }
    public void addCard(){
    	String card = JOptionPane.showInputDialog(frame, "Enter the card (Card) + (value) to add to your hand");
    	client.send("add "+ card);
    }
    public void withdraw(){
    	if(turn.getText().equals("Players turn: you")){
        	client.send("withdraw");
    	}
    }
    public void pause(){
    	try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
    }
    public static void main(String[] args) {
    	
    		JFrame frame = new JFrame("Connect to Server");
    		GUI gui = new GUI();
    		gui.client = new Client(gui);
    		while(!gui.client.connect(JOptionPane.showInputDialog(frame, "Enter the IP Address of the Server"), 40000)){
    			
    		}
    		try{
    			Thread.sleep(2000);
    		}catch(InterruptedException ie){
    		}
       		
       		gui.drawLobby();


    }
}