package ivanhoe;

import static org.junit.Assert.*;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ServerClientGameTest {
	Server server;
	Client clients[];
	
	@Before
	public void setUp(){
		server = new Server();
		server.startUp(10000);
		clients = new Client[3];
		clients[0] = new Client();
		clients[1] = new Client();
		clients[2] = new Client();
		
		clients[0].connect("0.0.0.0", 10000);
		clients[1].connect("0.0.0.0", 10000);
		clients[2].connect("0.0.0.0", 10000);
		
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
			
		}
		clients[0].send("create 3 scott");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		clients[1].send("join jon");
		
		clients[2].send("join kyle");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		clients[0].send("start");
		
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		clients[0].send("set purple");
		
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		for(int i = 0; i < 3; i++){
			for(int j = 2; j < 9; j++){
				Card c = server.game.getPlayer(clients[i].getID()).getHand().get(0);
				server.game.getPlayer(clients[i].getID()).removeFromHand(c.getName(), c.getValue());
				server.game.getPlayer(clients[i].getID()).addToHand(new Card("purple", j));
			}
			server.game.getPlayer(clients[i].getID()).addToHand(new Card("maiden", 5));
			
		}
		
	}
	
	@After
	public void tearDown(){
		server.close();
	}
	/*
	@Test
	public void testPlayCard() {
		
		server.sendGameState();
		clients[0].send("play purple 3");
		clients[1].send("play purple 3");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		assertEquals(3,server.game.getPlayer(clients[0].getID()).getScore());
		assertEquals(0,server.game.getPlayer(clients[1].getID()).getScore());
		assertEquals("Please wait it is scott's turn", clients[1].getLastMessage());
		

		clients[0].send("end turn");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		assertEquals(true, server.game.checkCurrPlayer(clients[1].getID()));
		
	}
	
	@Test
	public void testEndTurn(){
		server.sendGameState();
		clients[0].send("play purple 3");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		assertEquals(3,server.game.getPlayer(clients[0].getID()).getScore());
		assertEquals(0,server.game.getPlayer(clients[1].getID()).getScore());
		assertEquals("Please wait it is scott's turn", clients[1].getLastMessage());
		

		clients[0].send("end turn");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		assertEquals(true, server.game.checkCurrPlayer(clients[1].getID()));
				
	}
	*/
	/*
	@Test
	public void testAutoWithdrawNoCard(){
		server.sendGameState();
		server.game.getPlayer(clients[0].getID()).addToken('b');
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		//assertEquals("Please wait it is scott's turn", clients[1].getLastMessage());
		
		clients[0].send("play purple 2");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[0].send("end turn");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[1].send("play purple 3");
		try{
			Thread.sleep(2000);
		}catch(InterruptedException ie){
		}
		clients[1].send("end turn");
		try{
			Thread.sleep(2000);
		}catch(InterruptedException ie){
		}
		clients[2].send("end turn");
		try{
			Thread.sleep(2000);
		}catch(InterruptedException ie){
		}
		assertEquals(true, server.game.checkCurrPlayer(clients[0].getID()));
		assertEquals(false, server.game.getPlayer(clients[2].getID()).playing);		
	}
	
	@Test
	public void testAutoWithdrawLowScore(){
		server.sendGameState();
		server.game.getPlayer(clients[0].getID()).addToken('b');
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		//assertEquals("Please wait it is scott's turn", clients[1].getLastMessage());
		
		clients[0].send("play purple 3");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[0].send("end turn");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[1].send("play purple 4");
		try{
			Thread.sleep(2000);
		}catch(InterruptedException ie){
		}
		clients[1].send("end turn");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[2].send("play purple 2");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[2].send("end turn");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		assertEquals(true, server.game.checkCurrPlayer(clients[0].getID()));
		assertEquals(false, server.game.getPlayer(clients[2].getID()).playing);		
	}
	*/
	/*
	@Test
	public void testAutoWithdrawLoseToken(){
		server.sendGameState();
		server.game.getPlayer(clients[2].getID()).addToken('B');
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		//assertEquals("Please wait it is scott's turn", clients[1].getLastMessage());
		
		clients[0].send("play purple 7");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[0].send("end turn");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[1].send("play purple 8");
		try{
			Thread.sleep(2000);
		}catch(InterruptedException ie){
		}
		clients[1].send("end turn");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[2].send("play maiden 5");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[2].send("end turn");
		try{
			Thread.sleep(3000);
		}catch(InterruptedException ie){
		}
		clients[2].send("remove Blue");
		try{
			Thread.sleep(3000);
		}catch(InterruptedException ie){
		}
		
		assertEquals(true, server.game.checkCurrPlayer(clients[0].getID()));
		assertEquals(false, server.game.getPlayer(clients[2].getID()).playing);		
		assertEquals(true, server.game.getPlayer(clients[2].getID()).getTokens().isEmpty());
	}
	*/
	@Test
	public void testEndTournament(){
		server.sendGameState();
		server.game.getPlayer(clients[2].getID()).addToken('B');
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		//assertEquals("Please wait it is scott's turn", clients[1].getLastMessage());
		
		clients[0].send("play purple 7");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[0].send("end turn");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[1].send("play purple 8");
		try{
			Thread.sleep(2000);
		}catch(InterruptedException ie){
		}
		clients[1].send("end turn");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[2].send("play maiden 5");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[2].send("end turn");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[2].send("remove Blue");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		clients[0].send("play purple 5");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[0].send("end turn");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		clients[1].send("end turn");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		clients[0].send("choose Green");
		try{
			Thread.sleep(1000);
		}catch(InterruptedException ie){
		}
		
		assertEquals(true, server.game.getPlayer(clients[0].getID()).getTokens().contains('G'));
	}
}
